package com.yuzhantao.collection.sensorflow.request.handle;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.yuzhantao.collection.sensorflow.message.RootMessage;
import com.yuzhantao.collection.sensorflow.request.message.LightCommandEntity;
import com.yuzhantao.collection.sensorflow.request.message.LightCommandEntity.DevAddr;
import com.yuzhantao.collection.sensorflow.request.message.LightCommandEntity.LightCommand;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class LedCommandHandleTest {
	private final static int LED_COLOR = 5; // 代表蓝色LED
	@Autowired
	LedCommandHandle ledCommandHandle;
	
	RootMessage msg;
	@Before
	public void createMessage() {
		this.msg = new RootMessage();
		msg.setActioncode("reader032");
		LightCommandEntity lightCommandEntity = new LightCommandEntity();
		List<DevAddr> devAddrList = new ArrayList<>();
		
		DevAddr dev = new DevAddr();
		dev.setDevAddrCode("1234567890");
		List<LightCommand> lightCommandList = new ArrayList<>();

		for(int i=1;i<6;i++) {
			LightCommand lc = new LightCommand();
			lc.setColor(LED_COLOR);
			lc.setU(i);
			lightCommandList.add(lc);
		}
		dev.setLightCommandList(lightCommandList);
		devAddrList.add(dev);
		lightCommandEntity.setDevAddrList(devAddrList);
		
		this.msg.setAwsPostdata(lightCommandEntity);
	}
	@Test
	public void isHandle() {
		Assert.assertTrue(ledCommandHandle.isHandle(this.msg));
	}
	@Test
	public void handle() {
		ledCommandHandle.handle(this.msg);
	}
}
