package com.yuzhantao.collection.sensorflow.request.handle;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.yuzhantao.collection.core.message.IMessageHandle;
import com.yuzhantao.collection.mq.ActiveMqClient;
import com.yuzhantao.collection.sensorflow.message.RootMessage;
import com.yuzhantao.collection.sensorflow.message.SensorFlowCommandMessage;
import com.yuzhantao.collection.sensorflow.message.SensorFlowCommandMessage.RGB;
import com.yuzhantao.collection.sensorflow.request.message.LightCommandEntity;
import com.yuzhantao.collection.sensorflow.request.message.LightCommandEntity.DevAddr;
import com.yuzhantao.collection.sensorflow.request.message.LightCommandEntity.LightCommand;

@Service(value = "ledCommandHandle")
public class LedCommandHandle implements IMessageHandle<RootMessage, Object> {
	protected final static Logger logger = Logger.getLogger(LedCommandHandle.class);
	private final static String ACTION_CODE = "reader032"; // MQ中判断消息类型的标识符
	private final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	@Autowired
	protected ActiveMqClient activeMqClient;

	@Override
	public boolean isHandle(RootMessage t) {
		if (t.getActioncode().equals(ACTION_CODE)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 点亮指定u位led
	 */
	@Override
	public Object handle(RootMessage t) {
		LightCommandEntity lightCommand = (LightCommandEntity) t.getAwsPostdata();
		List<DevAddr> devAddrList = lightCommand.getDevAddrList();
		for (int i = 0; i < devAddrList.size(); i++) {
			DevAddr dev = devAddrList.get(i);
			List<LightCommand> lightCmdList = dev.getLightCommandList();
			for (int j = 0; j < lightCmdList.size(); j++) {
				LightCommand lc = lightCmdList.get(j);
				SensorFlowCommandMessage lcu = this.mqLedToSensorFlowLedEntity(dev.getDevAddrCode(), lc);
				String topic = this.getTopic(dev.getDevAddrCode(), lc.getU());
				this.sendLedCommandToMq(topic, dev.getDevAddrCode(), lcu);
			}
		}
		return null;
	}

	/**
	 * 点亮u位的led
	 * @param topic
	 * @param deviceCode
	 * @param lcu
	 */
	private void sendLedCommandToMq(String topic, String deviceCode, SensorFlowCommandMessage lcu) {
		String json = JSON.toJSONString(lcu);
		activeMqClient.send(topic, json);
		logger.info("发送LED控制命令到SensorFlow设备[设备编号:" + deviceCode + "]:" + json);
	}

	private String getTopic(String deviceCode, int uPosition) {
		return "command." + deviceCode + ".u-" + uPosition + ".led";
	}

	/**
	 * mq传过来的led命令转SensorFlow的led指令实体
	 * 
	 * @param deviceCode
	 * @param lc
	 * @return
	 */
	private SensorFlowCommandMessage mqLedToSensorFlowLedEntity(String deviceCode, LightCommand lc) {
		SensorFlowCommandMessage lcu = new SensorFlowCommandMessage();
		lcu.setMonitoringUnit(deviceCode);
		lcu.setSampleUnit("u-" + lc.getU());
		lcu.setChannel("led");
		lcu.setPhase("executing");
		lcu.setTimeout(10000);
		lcu.setPriority(0);
		lcu.setOperator("SensorFlowCollectionService");
		lcu.setStartTime(dateFormat.format(new Date()));

		RGB color = null;
		switch (lc.getColor()) {
		case 1: // 红色
			color = lcu.new RGB(1, 0, 0);
			break;
		case 5: // 蓝色
			color = lcu.new RGB(0, 0, 1);
			break;
		default:
			color = lcu.new RGB(0, 0, 0);
			break;
		}
		lcu.setParameters(color);
		return lcu;
	}
}
