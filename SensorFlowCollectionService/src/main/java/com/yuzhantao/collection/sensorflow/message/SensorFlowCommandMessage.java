package com.yuzhantao.collection.sensorflow.message;

/**
 * SenserFlowU位控制LED命令格式
 * 
 * @author yuzhantao
 *
 */
public class SensorFlowCommandMessage {
	/**
	 * 􏰘􏰙􏰚􏰛 控制单元ID
	 */
	private String monitoringUnit;
	/**
	 * 采集单元ID
	 */
	private String sampleUnit;
	/**
	 * 控制通道ID
	 */
	private String channel;
	/**
	 * 参数
	 */
	private Object parameters;
	/**
	 * 控制阶段
	 */
	private String phase;
	/**
	 * 超时时间（毫秒）
	 */
	private long timeout;
	/**
	 * 控制优先级
	 */
	private int priority;
	/**
	 * 控制执行者
	 */
	private String operator;
	/**
	 * 控制命令执行时间
	 */
	private String startTime;

	public String getMonitoringUnit() {
		return monitoringUnit;
	}

	public void setMonitoringUnit(String monitoringUnit) {
		this.monitoringUnit = monitoringUnit;
	}

	public String getSampleUnit() {
		return sampleUnit;
	}

	public void setSampleUnit(String sampleUnit) {
		this.sampleUnit = sampleUnit;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public Object getParameters() {
		return parameters;
	}

	public void setParameters(Object parameters) {
		this.parameters = parameters;
	}

	public String getPhase() {
		return phase;
	}

	public void setPhase(String phase) {
		this.phase = phase;
	}

	public long getTimeout() {
		return timeout;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * 颜色对象，0表示关闭，1表示常亮，>1表示闪烁频率
	 * 
	 * @author yuzhantao
	 *
	 */
	public class RGB {
		private int r;
		private int b;
		private int g;

		public RGB() {
		}

		public RGB(int r, int g, int b) {
			this.r = r;
			this.g = g;
			this.b = b;
		}

		public int getR() {
			return r;
		}

		public void setR(int r) {
			this.r = r;
		}

		public int getB() {
			return b;
		}

		public void setB(int b) {
			this.b = b;
		}

		public int getG() {
			return g;
		}

		public void setG(int g) {
			this.g = g;
		}
	}
}
