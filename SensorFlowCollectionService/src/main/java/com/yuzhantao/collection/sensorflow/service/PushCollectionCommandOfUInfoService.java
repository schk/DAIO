package com.yuzhantao.collection.sensorflow.service;

import java.util.concurrent.TimeUnit;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.google.common.util.concurrent.AbstractScheduledService;
import com.yuzhantao.collection.mq.ActiveMqClient;
import com.yuzhantao.collection.sensorflow.message.SensorFlowCommandMessage;

/**
 * 推送采集U位命令的服务
 * @author yuzhantao
 *
 */
@Service(value = "pushCollectionCommandOfUInfoService")
//@ConfigurationProperties(prefix = "uinfo.report.service")
public class PushCollectionCommandOfUInfoService extends AbstractScheduledService {
	protected final static Logger logger = Logger.getLogger(UInfoRegularlyReportService.class);
	protected final static String PUSH_U_INFO_TOPIC = "daioReader"; // 推送u位信息的topic
	@Autowired
	protected ActiveMqClient activeMqClient;
	/**
	 * 初始化时的延时时间，我们
	 */
	protected long initialdelay=30;
	/**
	 * 延时间隔时间
	 */
	protected long delay=30;
	
	@Override
	protected void runOneIteration() throws Exception {
		SensorFlowCommandMessage sf = new SensorFlowCommandMessage();
		sf.setMonitoringUnit("1234567890");
		sf.setSampleUnit("_");
		sf.setChannel("sync");
		sf.setPhase("executing");
		sf.setTimeout(1000*60*60*24);
		sf.setPriority(0);
		sf.setParameters(new Object());
		sf.setOperator("SensorFlowService");
		sf.setStartTime("2018-09-26 11:20:00");
//		String json = sensorFlowCoder.encode(sf);
		String json = JSON.toJSONString(sf);
		activeMqClient.send("command.1234567890._.sync", json);
//		logger.info("已发送:"+json);
	}

	@Override
	protected Scheduler scheduler() {
		return Scheduler.newFixedRateSchedule(this.initialdelay, this.delay, TimeUnit.SECONDS);
	}
}
