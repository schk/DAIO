package com.yuzhantao.collection.sensorflow.response.message;

import java.util.List;

import com.yuzhantao.collection.sensorflow.message.SensorParamesEntity;

public class SensorOfAwsPostdataEntity {
	private List<SensorParamesEntity> sensorList;
	private String rackConverCode;
	public List<SensorParamesEntity> getSensorList() {
		return sensorList;
	}
	public void setSensorList(List<SensorParamesEntity> sensorList) {
		this.sensorList = sensorList;
	}
	public String getRackConverCode() {
		return rackConverCode;
	}
	public void setRackConverCode(String rackConverCode) {
		this.rackConverCode = rackConverCode;
	}
}
