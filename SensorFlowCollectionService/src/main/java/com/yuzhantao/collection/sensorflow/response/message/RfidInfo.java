package com.yuzhantao.collection.sensorflow.response.message;

public class RfidInfo {
	private String rfid;
	private Integer u;
	public String getRfid() {
		return rfid;
	}
	public void setRfid(String rfid) {
		this.rfid = rfid;
	}
	public Integer getU() {
		return u;
	}
	public void setU(Integer u) {
		this.u = u;
	}
	
}
