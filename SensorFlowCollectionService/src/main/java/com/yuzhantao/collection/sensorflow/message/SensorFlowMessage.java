package com.yuzhantao.collection.sensorflow.message;
/**
 * SensorFlow消息
 * @author yuzhantao
 *
 */
public class SensorFlowMessage {
	/**
	 * 􏰘􏰙􏰚􏰛监控单元ID,全局唯一。
	 */
	private String monitoringUnitId;
	/**
	 * 采集单元ID,控制单元ID下唯一。
	 */
	private String sampleUnitId;
	/**
	 * 采集通道ID,采集单元唯一。
	 */
	private String channelId;
	/**
	 * 采集值
	 */
	private Object value;
	/**
	 * 采集时间,UTC时间。
	 */
	private String timestamp;
	/**
	 * 变化数据,COV标志。
	 */
	private boolean cov;
	/**
	 * 采集状态,0为正常,-1为异常,大于零为告警等级.
	 */
	private int state;
	public String getMonitoringUnitId() {
		return monitoringUnitId;
	}
	public void setMonitoringUnitId(String monitoringUnitId) {
		this.monitoringUnitId = monitoringUnitId;
	}
	public String getSampleUnitId() {
		return sampleUnitId;
	}
	public void setSampleUnitId(String sampleUnitId) {
		this.sampleUnitId = sampleUnitId;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public boolean isCov() {
		return cov;
	}
	public void setCov(boolean cov) {
		this.cov = cov;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
}
