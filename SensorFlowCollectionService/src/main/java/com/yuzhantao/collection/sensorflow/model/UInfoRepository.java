package com.yuzhantao.collection.sensorflow.model;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UInfoRepository extends JpaRepository<UInfo, String> {
	/**
	 * 根据设备编号和u位查找U位信息
	 * @param deviceCode
	 * @param position
	 * @return
	 */
	UInfo findOneByDeviceCodeAndPosition(String deviceCode,int position);
	
	/**
	 * 根据标签查找机柜
	 * @param tag
	 * @return
	 */
	UInfo findOneByTag(String tag);
	
	/**
	 * 根据设备编号查找U位信息列表
	 * @param deviceCode
	 * @return
	 */
	List<UInfo> findByDeviceCode(String deviceCode);
	
	/**
	 * 查找所有设备编号
	 * @return
	 */
    @Query("select deviceCode from UInfo group by deviceCode")
    List<String> findDeviceCodes();
}
