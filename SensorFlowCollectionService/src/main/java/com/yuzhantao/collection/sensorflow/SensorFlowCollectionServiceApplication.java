package com.yuzhantao.collection.sensorflow;

import org.jboss.logging.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jms.annotation.EnableJms;

@EnableJms
@SpringBootApplication
@ComponentScan(basePackages = { "com.yuzhantao.collection.sensorflow", "com.yuzhantao.collection.mq" })
public class SensorFlowCollectionServiceApplication {
	protected final static Logger logger = Logger.getLogger(SensorFlowCollectionServiceApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(SensorFlowCollectionServiceApplication.class, args);
	}
}
