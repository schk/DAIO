package com.yuzhantao.collection.sensorflow.response;

import javax.jms.ConnectionFactory;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.yuzhantao.collection.core.message.MessageHandleContext;
import com.yuzhantao.collection.sensorflow.message.SensorFlowMessage;
import com.yuzhantao.collection.sensorflow.model.UInfoRepository;
import com.yuzhantao.collection.sensorflow.response.coder.SensorFlowCoder;
import com.yuzhantao.collection.sensorflow.response.handle.BuiltInTemperatureHandle;
import com.yuzhantao.collection.sensorflow.response.handle.InsertTagHandle;
import com.yuzhantao.collection.sensorflow.response.handle.TagLinkStateHandle;

/**
 * SensorFlow设备消息监听
 * 
 * @author yuzhantao
 *
 */
@Service
public class SensorFlowSampleListener {
	protected static final Logger logger = Logger.getLogger(SensorFlowSampleListener.class);
	@Autowired
	protected SensorFlowCoder sensorFlowCoder; // 解码器
	protected MessageHandleContext<SensorFlowMessage, Object> messageHandle = new MessageHandleContext<>(); // 消息处理类

	protected UInfoRepository uInfoRepository; // u位信息dao

	@Autowired
	public SensorFlowSampleListener(UInfoRepository uInfoRepository, BuiltInTemperatureHandle builtInTemperatureHandle) {
		this.messageHandle.addHandleClass(new InsertTagHandle(uInfoRepository));
		this.messageHandle.addHandleClass(builtInTemperatureHandle);
		this.messageHandle.addHandleClass(new TagLinkStateHandle(uInfoRepository));
	}

	@JmsListener(destination = "sample-values.>", containerFactory = "jmsListenerSensorFlowTopic")
	public void receiveMessage(String message) {
		try {
			SensorFlowMessage sf = sensorFlowCoder.decode(message);
			logger.info("接收到连线设备消息:" + JSON.toJSONString(sf));
			messageHandle.handle(sf);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	@Bean
	public JmsListenerContainerFactory<?> jmsListenerSensorFlowTopic(ConnectionFactory activeMQConnectionFactory) {
		DefaultJmsListenerContainerFactory bean = new DefaultJmsListenerContainerFactory();
		bean.setPubSubDomain(true);
		bean.setConnectionFactory(activeMQConnectionFactory);
		return bean;
	}
}
