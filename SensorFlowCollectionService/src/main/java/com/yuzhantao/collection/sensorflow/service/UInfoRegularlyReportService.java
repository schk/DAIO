package com.yuzhantao.collection.sensorflow.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.google.common.util.concurrent.AbstractScheduledService;
import com.yuzhantao.collection.mq.ActiveMqClient;
import com.yuzhantao.collection.sensorflow.message.RootMessage;
import com.yuzhantao.collection.sensorflow.model.UInfo;
import com.yuzhantao.collection.sensorflow.model.UInfoRepository;
import com.yuzhantao.collection.sensorflow.response.message.UInfoEntity;
import com.yuzhantao.collection.sensorflow.response.message.UInfoEntity.UdevInfo;

/**
 * U位信息定期上报服务
 * 
 * @author yuzhantao
 *
 */
@Service(value = "uInfoRegularlyReportService")
@ConfigurationProperties(prefix = "uinfo.report.service")
public class UInfoRegularlyReportService extends AbstractScheduledService {
	protected final static Logger logger = Logger.getLogger(UInfoRegularlyReportService.class);
	protected final static String PUSH_U_INFO_TOPIC = "daioReader"; // 推送u位信息的topic
	@Autowired
	private UInfoRepository uInfoRepository;
	@Autowired
	protected ActiveMqClient activeMqClient;
	/**
	 * 初始化时的延时时间
	 */
	protected long initialdelay;
	/**
	 * 延时间隔时间
	 */
	protected long delay;

	@Override
	protected void runOneIteration() throws Exception {
		try {
			List<String> deviceCodes = uInfoRepository.findDeviceCodes(); // 从数据库获取所有设备编码
			// 遍历数据库中的所有设备，并将他们的u位信息上报到mq
			for (int i = 0; i < deviceCodes.size(); i++) {
				String deviceCode = deviceCodes.get(i); // 获取当前设备编号
				List<UInfo> uInfoList = this.uInfoRepository.findByDeviceCode(deviceCode); // 根据设备编号查找U位信息列表
				this.pushToMq(uInfoList, deviceCode); // 向mq推送设备中的u位信息
				uInfoList.clear(); // 清除用过的数组
			}
		} catch (Exception e) {
			logger.error(e.getCause());
		}
	}

	@Override
	protected Scheduler scheduler() {
		return Scheduler.newFixedRateSchedule(this.initialdelay, this.delay, TimeUnit.SECONDS);
	}

	/**
	 * 推送u位信息到MQ
	 * 
	 * @param list
	 * @param deviceCode
	 */
	private void pushToMq(List<UInfo> list, String deviceCode) {
		UInfoEntity devs = new UInfoEntity();
		List<UInfoEntity.DevAddr> devList = new ArrayList<>();
		UInfoEntity.DevAddr dev = devs.new DevAddr();
		dev.setDevAddrCode(deviceCode);

		List<UdevInfo> uDevInfoList = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			UInfo srcU = list.get(i);
			UdevInfo u = devs.new UdevInfo();
			u.setRfid(srcU.getTag());
			u.setU(srcU.getPosition());
			uDevInfoList.add(u);
		}
		dev.setUdevInfo(uDevInfoList);
		devList.add(dev);
		devs.setDevAddrList(devList);

		RootMessage mes = new RootMessage();
		mes.setActioncode("reader003");
		// TODO 目前web端写死只有smarrack设备的reader003才会处理
		mes.setRfidtype("smarrack");
		mes.setAwsPostdata(devs);
		logger.info("机柜U位信息定时上传:" + JSON.toJSONString(mes));
		activeMqClient.send(PUSH_U_INFO_TOPIC, JSON.toJSONString(mes)); // 发送mq到daioReader主题

		devList.clear();
		devList = null;
		mes = null;
		devs = null;
		dev = null;
		mes = null;
	}

	public void setInitialdelay(long initialdelay) {
		this.initialdelay = initialdelay;
	}

	public void setDelay(long delay) {
		this.delay = delay;
	}
}
