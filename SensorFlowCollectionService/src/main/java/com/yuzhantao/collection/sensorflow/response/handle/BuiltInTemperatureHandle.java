package com.yuzhantao.collection.sensorflow.response.handle;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuzhantao.collection.core.message.IMessageHandle;
import com.yuzhantao.collection.mq.ActiveMqClient;
import com.yuzhantao.collection.sensorflow.message.SensorFlowMessage;
import com.yuzhantao.collection.sensorflow.message.SensorManage;
import com.yuzhantao.collection.sensorflow.message.SensorParamesEntity;

/**
 * 内置温度的处理
 * 将收到的温度数据发送到mq服务器
 * @author yuzhantao
 *
 */
@Service(value = "builtInTemperatureHandle")
public class BuiltInTemperatureHandle implements IMessageHandle<SensorFlowMessage, Object> {
	protected static final Logger logger = Logger.getLogger(BuiltInTemperatureHandle.class);
	protected final static String SAMPLE_UNIT_ID_PATTERN = "^(tag\\-)[0-9]";
	protected final static String CHANNEL_ID = "temperature";
	
	@Autowired
	protected ActiveMqClient activeMqClient;
	
	@Override
	public boolean isHandle(SensorFlowMessage t) {
		if (isVail(t.getSampleUnitId()) && CHANNEL_ID.equals(t.getChannelId())) {
			return true;
		} else {
			return false;
		}
	}
	
	private boolean isVail(String sampleUnitId) {
		return sampleUnitId.indexOf("tag") == 0;
	}

	@Override
	public Object handle(SensorFlowMessage t) {
		int uPosition = Integer.valueOf(t.getSampleUnitId().replaceAll("tag-", "")); // 获取u位
		uPosition = 42 - uPosition + 1;
		SensorParamesEntity spe = new SensorParamesEntity();
		spe.setSensorPosition(uPosition);
		spe.setSensorType(1); // 温度类型默认1
		spe.setSensorDatas(Double.valueOf(String.valueOf(t.getValue())).intValue());
		SensorManage.getInstance().put(t.getMonitoringUnitId(), spe);
		return null;
	}

}
