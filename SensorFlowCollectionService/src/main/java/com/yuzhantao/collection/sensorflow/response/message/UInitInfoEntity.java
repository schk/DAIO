package com.yuzhantao.collection.sensorflow.response.message;

import java.util.List;

public class UInitInfoEntity {
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private int httpCode;
	private String msg;
	private long timestamp;
	private List<RackInfoVo> data;
	public int getiTotalRecords() {
		return iTotalRecords;
	}
	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}
	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}
	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	public int getHttpCode() {
		return httpCode;
	}
	public void setHttpCode(int httpCode) {
		this.httpCode = httpCode;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public List<RackInfoVo> getData() {
		return data;
	}
	public void setData(List<RackInfoVo> data) {
		this.data = data;
	}
	
}
