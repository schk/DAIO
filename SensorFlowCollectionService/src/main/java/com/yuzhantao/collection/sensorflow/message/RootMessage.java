package com.yuzhantao.collection.sensorflow.message;

/**
 * 最外层根目录的消息结构
 * @author yuzhantao
 *
 */
public class RootMessage {
	private String actioncode;
	private String rfidtype;
	private Object awsPostdata;
	private String timeStamp;
	private boolean isTiemout=false;
	public String getActioncode() {
		return actioncode;
	}
	public void setActioncode(String actioncode) {
		this.actioncode = actioncode;
	}
	public String getRfidtype() {
		return rfidtype;
	}
	public void setRfidtype(String rfidtype) {
		this.rfidtype = rfidtype;
	}
	public Object getAwsPostdata() {
		return awsPostdata;
	}
	public void setAwsPostdata(Object awsPostdata) {
		this.awsPostdata = awsPostdata;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public boolean isTiemout() {
		return isTiemout;
	}
	public void setTiemout(boolean isTiemout) {
		this.isTiemout = isTiemout;
	}
}
