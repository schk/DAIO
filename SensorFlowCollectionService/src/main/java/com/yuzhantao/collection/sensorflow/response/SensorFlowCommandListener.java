package com.yuzhantao.collection.sensorflow.response;

import javax.jms.ConnectionFactory;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.stereotype.Service;

import com.yuzhantao.collection.core.message.MessageHandleContext;
import com.yuzhantao.collection.sensorflow.message.SensorFlowMessage;
import com.yuzhantao.collection.sensorflow.model.UInfoRepository;
import com.yuzhantao.collection.sensorflow.response.coder.SensorFlowCoder;
import com.yuzhantao.collection.sensorflow.response.handle.InsertTagHandle;

@Service
public class SensorFlowCommandListener {
	protected static final Logger logger = Logger.getLogger(SensorFlowCommandListener.class);
	@Autowired
	protected SensorFlowCoder sensorFlowCoder; // 解码器
	protected MessageHandleContext<SensorFlowMessage, Object> messageHandle = new MessageHandleContext<>(); // 消息处理类

	protected UInfoRepository uInfoRepository;

	@Autowired
	public SensorFlowCommandListener(UInfoRepository uInfoRepository) {
		this.messageHandle.addHandleClass(new InsertTagHandle(uInfoRepository));
	}

	@JmsListener(destination = "command.>", containerFactory = "jmsListenerSensorFlowCommandTopic")
	public void receiveMessage(String message) {
//		try {
//			SensorFlowMessage sf = sensorFlowCoder.decode(message);
//			String json = JSON.toJSONString(sf);
//			logger.info("接收到command消息:" + json);
//		} catch (Exception e) {
//			e.printStackTrace();
//			logger.error(e.getMessage());
//		}
	}

	@Bean
	public JmsListenerContainerFactory<?> jmsListenerSensorFlowCommandTopic(
			ConnectionFactory activeMQConnectionFactory) {
		DefaultJmsListenerContainerFactory bean = new DefaultJmsListenerContainerFactory();
		bean.setPubSubDomain(true);
		bean.setConnectionFactory(activeMQConnectionFactory);
		return bean;
	}
}
