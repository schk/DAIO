package com.yuzhantao.collection.sensorflow.response.message;

import java.util.List;

public class RackInfoVo {
	private String rackConverCode;
	private List<RfidInfo> rfidInfoList;
	public String getRackConverCode() {
		return rackConverCode;
	}
	public void setRackConverCode(String rackConverCode) {
		this.rackConverCode = rackConverCode;
	}
	public List<RfidInfo> getRfidInfoList() {
		return rfidInfoList;
	}
	public void setRfidInfoList(List<RfidInfo> rfidInfoList) {
		this.rfidInfoList = rfidInfoList;
	}
	
}
