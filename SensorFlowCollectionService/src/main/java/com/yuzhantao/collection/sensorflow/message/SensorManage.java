package com.yuzhantao.collection.sensorflow.message;

import java.util.HashMap;
import java.util.Map;

/**
 * 传感器管理类
 * @author yuzhantao
 *
 */
public class SensorManage {
	private static SensorManage instance;
	
	private Map<String,Map<Integer, SensorParamesEntity>> pushMap = new HashMap<>();
	
	public static SensorManage getInstance() {
		if(instance==null) {
			instance = new SensorManage();
		}
		return instance;
	}
	
	public Map<Integer,SensorParamesEntity> get(String deviceCode) {
		if(this.pushMap.containsKey(deviceCode)) {
			return this.pushMap.get(deviceCode);
		}else {
			return null;
		}
	}
	
	public void put(String deviceCode, SensorParamesEntity sensorParames) {
		if(this.pushMap.containsKey(deviceCode)==false) {
			this.pushMap.put(deviceCode, new HashMap<Integer, SensorParamesEntity>());
		}
		
		this.pushMap.get(deviceCode).put(sensorParames.getSensorPosition(), sensorParames);
	}
	
	public Map<String,Map<Integer, SensorParamesEntity>> getMap(){
		return this.pushMap;
	}
}
