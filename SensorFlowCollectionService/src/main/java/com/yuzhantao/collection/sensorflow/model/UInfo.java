package com.yuzhantao.collection.sensorflow.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class UInfo {
	@Id
	@GeneratedValue
	private int id;
	private String tag;
	private String deviceCode;
	private int position;
	private int redLed;
	private int greenLed;
	private int blueLed;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDeviceCode() {
		return deviceCode;
	}
	public void setDeviceCode(String deviceCode) {
		this.deviceCode = deviceCode;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public int getRedLed() {
		return redLed;
	}
	public void setRedLed(int redLed) {
		this.redLed = redLed;
	}
	public int getGreenLed() {
		return greenLed;
	}
	public void setGreenLed(int greenLed) {
		this.greenLed = greenLed;
	}
	public int getBlueLed() {
		return blueLed;
	}
	public void setBlueLed(int blueLed) {
		this.blueLed = blueLed;
	}
	
	
}
