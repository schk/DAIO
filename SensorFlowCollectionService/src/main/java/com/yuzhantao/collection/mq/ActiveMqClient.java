package com.yuzhantao.collection.mq;

import javax.jms.Destination;

import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Service;

@Service(value="activeMqClient")
public class ActiveMqClient {
	@Autowired
    private JmsMessagingTemplate jmsTemplate;
	
	public void send(String topic,String msg){
		Destination destination = new ActiveMQTopic(topic);
        jmsTemplate.convertAndSend(destination, msg);
    }
}
