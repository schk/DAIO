package com.strongbj.iot.devices.guis.respnose.handle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.dam.common.AbstractTaskDelayedRunnable;
import com.strongbj.iot.devices.dam.common.DAMManager;
import com.strongbj.iot.devices.dam.common.ITaskManager;
import com.strongbj.iot.devices.dam.sub.dacai.message.DaCaiMessage;
import com.strongbj.iot.devices.dam.sub.dacai.message.DaCaiMessageType;
import com.strongbj.iot.devices.dam.sub.dacai.response.entity.ScreenReturnTextControlEntity;
import com.strongbj.iot.devices.dam.sub.dacai.response.entity.ScreenReturnTextControlFactory;
import com.strongbj.iot.devices.guis.message.GUISOSMessageFactory;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;

public class SetLocalNetParamsHandle implements IMessageHandle<DaCaiMessage, Object> {
	private static Logger logger = LogManager.getLogger(SetLocalNetParamsHandle.class.getName());
	protected final static String COMMAND_CODE = "B111"; // 打开页面命令的编号
//	protected final static String NET_PAGE_INDEX = "0004";  	// 网络设置页面的索引
	protected final static byte[] DEFAULT_DEVICE_CODE = { 00, 00, 00 }; // 设备默认编号
	protected final static byte[] EMPTY_DATAS = {}; // 空参数
	protected final static long MAX_SET_AND_WAIT_MILLISECOND = 2000; // 最大等待并设置参数的毫秒数
	private final static byte SET_NET_COMMAND = (byte)0x06;	// 设置网络的命令
	
	private GUISOSMessageFactory gUISOSMessageFactory = new GUISOSMessageFactory();

	private String ip, mask, gateway,serverIp,serverPort; // 从屏幕获取的数据
	private long prevGetTime; // 上一次获取数据的时间
	private ScreenReturnTextControlEntity screenReturnTextControlEntity;

	@Override
	public boolean isHandle(DaCaiMessage t) {
		logger.info("设置网络参数 set net");
		if (t.getMessageType() == DaCaiMessageType.System && t.getCommand().equals(COMMAND_CODE)) {
			ScreenReturnTextControlEntity sr = ScreenReturnTextControlFactory.create(t.getDatas());

			logger.info("========{}", sr.getScreenId());
			// 如果控件类型为按钮，屏幕id为4，按钮id为1,2,3，就认为是打开网络界面
			if (sr.getControlType() == 0x11 && sr.getScreenId() == 4
					&& (sr.getControlId() == 1 || sr.getControlId() == 2 || sr.getControlId() == 3 || sr.getControlId() == 4 || sr.getControlId() == 5)) {
				this.screenReturnTextControlEntity = sr;
				return true;
			}
		}
		return false;
	}

	private void initScreenParams() {
		this.ip = "";
		this.mask = "";
		this.gateway = "";
		this.serverIp="";
		this.serverPort="";
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, DaCaiMessage t) {
		if (System.currentTimeMillis() - prevGetTime > MAX_SET_AND_WAIT_MILLISECOND) {
			initScreenParams();
		}
		switch (this.screenReturnTextControlEntity.getControlId()) {
		case 1:
			this.ip = new String(ByteUtil.hexStringToBytes(this.screenReturnTextControlEntity.getContent()));
			break;
		case 2:
			this.mask = new String(ByteUtil.hexStringToBytes(this.screenReturnTextControlEntity.getContent()));
			break;
		case 3:
			this.gateway = new String(ByteUtil.hexStringToBytes(this.screenReturnTextControlEntity.getContent()));
			break;
		case 4:
			this.serverIp = new String(ByteUtil.hexStringToBytes(this.screenReturnTextControlEntity.getContent()));
			this.serverIp = this.serverIp.replace("\0", "").replace("\r", "").replace("\n", "");
			break;
		case 5:
			this.serverPort = new String(ByteUtil.hexStringToBytes(this.screenReturnTextControlEntity.getContent()));
			this.serverPort = this.serverPort.replace("\0", "").replace("\r", "").replace("\n", "");
			int port = Integer.valueOf(this.serverPort);
			if (port<1 || port>65535) {//若小屏输入的端口号不规范，则默认设置为1
				port = 1;
			}
			this.serverPort = port+"";
			break;
		default:
			return null;
		}
		this.prevGetTime = System.currentTimeMillis();

		if (this.ip == "" || this.mask == "" || this.gateway == "" || this.serverIp=="" || this.serverPort=="")
			return null;

		byte[] datas = this.NetParamesEntity2Bytes(ByteUtil.hexStringToBytes(t.getDamCode()), this.ip, this.mask, this.gateway,this.serverIp,this.serverPort);
		
		ITaskManager taskManager = DAMManager.getTaskManager(t.getDamCode());
		taskManager.addSendTask(new AbstractTaskDelayedRunnable() {
			@Override
			protected void taskRun() {
				sendMessageToDAM(ctx.channel(), datas);
				// 网卡设置需要过多时间，顾让接收DAM的命令睡一下
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
		});
		initScreenParams(); // 发送后初始化屏幕
		return null;
	}

	/**
	 * 发送获取本地网络的消息
	 * 
	 * @param channel
	 */
	private void sendMessageToDAM(Channel channel, byte[] datas) {
		ByteBuf bs = Unpooled.copiedBuffer(datas);
		ChannelFuture cf = channel.writeAndFlush(bs);
		cf.addListener(new ChannelFutureListener() {

			@Override
			public void operationComplete(ChannelFuture future) throws Exception {
				if (future.isSuccess()) {
					logger.info("RU2000自动下发设置本地网络参数命令成功 下发命令={}  ", ByteUtil.byteArrToHexString(datas, true));
				} else {
					logger.error("RU2000自动下发设置本地网络参数命令失败 下发命令={}  ", ByteUtil.byteArrToHexString(datas, true));
				}
			}
		});
	}

	/**
	 * 网络参数对象转byte数组
	 * 
	 * @param np
	 * @return
	 */
	private byte[] NetParamesEntity2Bytes(byte[] devAddress, String ip, String mask, String gateway,String serverIp,String serverPort) {
		// 创建
		byte[] netParamsDatas = new byte[12];
		// 将ip赋值到4byte数组中
		this.setParamsToBytes(ip, netParamsDatas, 0);
		// 将子网掩码赋值到4byte数组中
		this.setParamsToBytes(mask, netParamsDatas, 4);
		// 将默认网关赋值到4byte数组中
		this.setParamsToBytes(gateway, netParamsDatas, 8);

		String dns = "";                          //DNS
		byte[] datas = new byte[22];   //装数据的byte数组
	    //4byte(本机IP地址)+4byte(掩码)+4byte(网关)+4byte(dns)+2byte(本机端口号)+4byte（目标IP地址）+2byte（目标端口号）
		byte [] pTmp=ByteUtil.ipToBytesByInet(ip);
		System.arraycopy(pTmp, 0, datas, 0, pTmp.length);
		       pTmp=ByteUtil.ipToBytesByInet(mask);
		System.arraycopy(pTmp, 0, datas, 4, pTmp.length);
	           pTmp=ByteUtil.ipToBytesByInet(gateway);
		System.arraycopy(pTmp, 0, datas, 8, pTmp.length);
		       pTmp=ByteUtil.ipToBytesByInet(dns);
	    System.arraycopy(pTmp, 0, datas, 12, pTmp.length);
	           pTmp=ByteUtil.ipToBytesByInet(serverIp);
	    System.arraycopy(pTmp, 0, datas, 16, pTmp.length);
	    ByteUtil.shortToBytes(Integer.valueOf(serverPort).shortValue(), datas,20);
	    
		return gUISOSMessageFactory.createGUISOSMessage(devAddress, SET_NET_COMMAND, datas);
	}

	/**
	 * 设置指定*.*.*.*格式的数据转为4byte数组
	 * 
	 * @param srcString  源数据
	 * @param destBytes  目标数组
	 * @param destOffset 赋值的目标数组偏移量
	 */
	private void setParamsToBytes(String srcString, byte[] destBytes, int destOffset) {
		String[] strIps = srcString.split("[.]");
		for (int i = 0; i < strIps.length; i++) {
			String sNum = strIps[i].replace("\0", "").replace("\r", "").replace("\n", "");
			destBytes[i + destOffset] = Integer.valueOf(sNum, 10).byteValue();
		}
	}
}
