package com.strongbj.iot.devices.dam.request.handle.dam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSONObject;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.dam.common.AbstractTaskDelayedRunnable;
import com.strongbj.iot.devices.dam.common.DAMManager;
import com.strongbj.iot.devices.dam.common.ITaskManager;
import com.strongbj.iot.devices.dam.common.NetMapping;
import com.strongbj.iot.devices.dam.message.DAMMessageFactory;
import com.strongbj.iot.devices.dam.message.MQMessageOfDAM;
import com.strongbj.iot.devices.dam.request.entity.SensorTypeEntity;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;

/**
 * 设置传感器类型
 * 
 * @author yuzhantao
 *
 */
public class SetSensorTypeHandle implements IMessageHandle<MQMessageOfDAM, Object> {
	private static Logger logger = LogManager.getLogger(SetSensorTypeHandle.class.getName());
	private static final String ACTION_CODE = "setSensorType"; // 事件处理编码
	private final DAMMessageFactory damMessageFactory = new DAMMessageFactory(); // DAM消息工厂

	@Override
	public boolean isHandle(MQMessageOfDAM t) {
		if (t.getActioncode().equals(ACTION_CODE)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 将设置网络参数的信息发送到硬件
	 */
	@Override
	public Object handle(ChannelHandlerContext ctx, MQMessageOfDAM t) {
		final SensorTypeEntity st = ((JSONObject) t.getAwsPostdata()).toJavaObject(SensorTypeEntity.class);
		Channel channel = NetMapping.getInstance().getChannel(st.getRackConverCode());
		ITaskManager taskManager = DAMManager.getTaskManager(st.getRackConverCode());
		taskManager.addSendTask(new AbstractTaskDelayedRunnable() {
			@Override
			protected void taskRun() {
				sendDatasToDAM(channel, t, st);
			}
		});
		return null;
	}

	private void sendDatasToDAM(Channel channel, MQMessageOfDAM t, SensorTypeEntity st) {
		if (channel != null && channel.isActive()) {
			final byte[] datas = SensorTypeEntity2Bytes(t.getTimestamp(), st); // 将网络参数对象转为byte数组，用于发送到dam硬件
			ByteBuf bs = Unpooled.copiedBuffer(datas);
			ChannelFuture cf = channel.writeAndFlush(bs);
			// TODO 添加监听,查看发送情况DEBUG
			cf.addListener(new ChannelFutureListener() {

				@Override
				public void operationComplete(ChannelFuture future) throws Exception {
					if (future.isSuccess()) {
						logger.info("WEB下发设置传感器类型命令到DAM 编号:{} 下发命令={}  ", st.getRackConverCode(),
								ByteUtil.byteArrToHexString(datas, true));
					} else {
						logger.error("WEB下发设置传感器类型命令到DAM 编号:{} 下发命令={}  ", st.getRackConverCode(),
								ByteUtil.byteArrToHexString(datas, true));
					}
				}

			});
		} else {
			logger.error("{} 主机没有找到指定channel", st.getRackConverCode());
		}
	}

	/**
	 * 网络参数对象转byte数组
	 * 
	 * @param np
	 * @return
	 */
	private byte[] SensorTypeEntity2Bytes(long time, SensorTypeEntity st) {
		// 创建
		byte[] sensorTypeDatas = new byte[st.getSensorTypes().length];
		for (int i = 0; i < st.getSensorTypes().length; i++) {
			sensorTypeDatas[i] = Integer.valueOf(st.getSensorTypes()[i]).byteValue();
		}

		// 将命令发送到屏幕硬件
		byte[] datas = damMessageFactory.createDAMMessage(ByteUtil.longToBytes(time), st.getRackConverCode(), // 设备编号
				(byte) 0, // 设备类型，默认填0
				(byte) 0, // 版本号，默认填0
				(byte) 0x16, // 设置传感器类型命令
				sensorTypeDatas // 设置的传感器类型数据
		); // 通过dam消息工厂获取设置传感器类型参数的byte命令数组
		return datas;
	}
}
