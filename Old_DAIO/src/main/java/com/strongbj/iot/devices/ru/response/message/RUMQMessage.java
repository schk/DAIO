package com.strongbj.iot.devices.ru.response.message;

public class RUMQMessage {
	
	private String actioncode;
	private String GUID;
	private String rfidtype;
	private Object awsPostdata;
	public String getActioncode() {
		return actioncode;
	}
	public void setActioncode(String actioncode) {
		this.actioncode = actioncode;
	}
	public String getGUID() {
		return GUID;
	}
	public void setGUID(String gUID) {
		GUID = gUID;
	}
	public String getRfidtype() {
		return rfidtype;
	}
	public void setRfidtype(String rfidtype) {
		this.rfidtype = rfidtype;
	}
	public Object getAwsPostdata() {
		return awsPostdata;
	}
	public void setAwsPostdata(Object awsPostdata) {
		this.awsPostdata = awsPostdata;
	}

}
