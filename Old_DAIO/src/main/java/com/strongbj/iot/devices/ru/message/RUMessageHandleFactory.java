package com.strongbj.iot.devices.ru.message;

import io.netty.channel.ChannelHandler;

import com.strongbj.core.message.IMessageHandleFactory;
import com.strongbj.iot.devices.ru.response.coder.RUDecoder;
import com.strongbj.iot.devices.ru.response.handle.RUResponseHandleContext;

public class RUMessageHandleFactory implements IMessageHandleFactory {

	@Override
	public ChannelHandler createMessageDecoder() {
		return new RUDecoder(Integer.MAX_VALUE, 6, 2, 2, 0, true);
	}

	@Override
	public ChannelHandler createMessageHandle() {
		return new RUResponseHandleContext();
	}

}
