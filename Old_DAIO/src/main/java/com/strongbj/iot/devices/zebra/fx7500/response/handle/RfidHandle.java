package com.strongbj.iot.devices.zebra.fx7500.response.handle;

import com.mot.rfid.api3.RfidStatusEvents;
import com.mot.rfid.api3.STATUS_EVENT_TYPE;
import com.strongbj.core.message.IMessageHandle;

import io.netty.channel.ChannelHandlerContext;

public class RfidHandle implements IMessageHandle<RfidStatusEvents,Object> {
//	private HashSet<String> rfidCache;
	
	@Override
	public boolean isHandle(RfidStatusEvents t) {
		STATUS_EVENT_TYPE statusType = t.StatusEventData.getStatusEventType();
		if(statusType == STATUS_EVENT_TYPE.INVENTORY_STOP_EVENT){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, RfidStatusEvents t) {
		System.out.println("标签处理结束==============");
		return null;
	}

}