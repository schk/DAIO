package com.strongbj.iot.devices.dam.sub.dacai.response.entity;

public class ScreenReturnButtonControlFactory {
	public static ScreenReturnButtonControlEntity create(String msg) {
		try {
		ScreenReturnButtonControlEntity sr = new ScreenReturnButtonControlEntity();
		sr.setScreenId(Integer.valueOf(msg.substring(0,4),16).shortValue());
		sr.setControlId(Integer.valueOf(msg.substring(4,8),16).shortValue());
		sr.setControlType(Integer.valueOf(msg.substring(8,10),16).byteValue());
		sr.setSubType(Integer.valueOf(msg.substring(10,12),16).byteValue());
		int sButtonState = Integer.valueOf(msg.substring(12,14));
		if(sButtonState==0) {
			sr.setButtonState(ButtonState.Up);
		}else if(sButtonState==1){
			sr.setButtonState(ButtonState.Down);
		}
//		sr.setSubtype(Integer.valueOf(msg.substring(10,12),16).byteValue());
//		sr.setStatus(Integer.valueOf(msg.substring(12,14),16).byteValue());
		return sr;
		}catch(Exception e) {
			return null;
		}
	}
}
