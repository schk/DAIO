package com.strongbj.iot.devices.zebra.fx7500.service;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mot.rfid.api3.AccessFilter;
import com.mot.rfid.api3.AntennaInfo;
import com.mot.rfid.api3.InvalidUsageException;
import com.mot.rfid.api3.LoginInfo;
import com.mot.rfid.api3.OperationFailureException;
import com.mot.rfid.api3.PostFilter;
import com.mot.rfid.api3.RFIDReader;
import com.mot.rfid.api3.ReaderManagement;
import com.mot.rfid.api3.START_TRIGGER_TYPE;
import com.mot.rfid.api3.STOP_TRIGGER_TYPE;
import com.mot.rfid.api3.TAG_EVENT_REPORT_TRIGGER;
import com.mot.rfid.api3.TriggerInfo;
import com.strongbj.iot.devices.zebra.fx7500.response.Fx7500Listener;

public class Fx7500Client {
	private static Logger logger = LogManager.getLogger(Fx7500Client.class.getName());
	private RFIDReader myReader = null;
	public static final String API_SUCCESS = "Function Succeeded";
	public static final String PARAM_ERROR = "Parameter Error";
	public boolean isConnected;
	public String hostName = "";
	public int port = 5084;
	public int y;// 有的时候通过门禁时，只触发了一次红外，所以造成会一直读，需要加一个判断，假如读超过y秒没有停止读（即没有触发第二道红外），则也停止读，并把数据推送上去
					// 。 此处的y标识 是否触动第二道红外，1是没有，2是有
	public SimpleDateFormat df = null;
	public boolean isAccessSequenceRunning = false;
	public AccessFilter accessFilter = null;
	public boolean isAccessFilterSet = false;
	public PostFilter postFilter = null;
	public boolean isPostFilterSet = false;

	// Antenna Info
	public AntennaInfo antennaInfo = null;
		
	public TriggerInfo triggerInfo = null;
	ReaderManagement rm = null;
	public boolean isRmConnected = false;
	public LoginInfo loginInfo;

	public RFIDReader getReader(){
		return this.myReader;
	}
	
	public RFIDReader getMyReader() {
		return this.myReader;
	}

	public Fx7500Client() {
		this.myReader = new RFIDReader();

		this.isAccessSequenceRunning = false;

		// Create the Access Filter
		accessFilter = new AccessFilter();

		// create the post filter
		postFilter = new PostFilter();

		// Create Antenna Info
		antennaInfo = new AntennaInfo();

		// Create Pre-Filter
//		PreFilters preFilter = new PreFilters();

		
		this.triggerInfo = new TriggerInfo();

		this.triggerInfo.StartTrigger.setTriggerType(START_TRIGGER_TYPE.START_TRIGGER_TYPE_IMMEDIATE);
		this.triggerInfo.StopTrigger.setTriggerType(STOP_TRIGGER_TYPE.STOP_TRIGGER_TYPE_IMMEDIATE);

		this.triggerInfo.TagEventReportInfo.setReportNewTagEvent(TAG_EVENT_REPORT_TRIGGER.MODERATED);
		this.triggerInfo.TagEventReportInfo.setNewTagEventModeratedTimeoutMilliseconds((short) 500);

		this.triggerInfo.TagEventReportInfo.setReportTagInvisibleEvent(TAG_EVENT_REPORT_TRIGGER.MODERATED);
		this.triggerInfo.TagEventReportInfo.setTagInvisibleEventModeratedTimeoutMilliseconds((short) 500);

		this.triggerInfo.TagEventReportInfo.setReportTagBackToVisibilityEvent(TAG_EVENT_REPORT_TRIGGER.MODERATED);
		this.triggerInfo.TagEventReportInfo.setTagBackToVisibilityModeratedTimeoutMilliseconds((short) 500);

		this.triggerInfo.setTagReportTrigger(1);

		this.rm = new ReaderManagement();
	}

	public boolean connectToReader(String readerHostName, int readerPort) {
		boolean retVal = false;
		this.hostName = readerHostName;
		this.port = readerPort;
		this.myReader.setHostName(this.hostName);

		this.myReader.setPort(this.port);
		try {
			this.myReader.connect();

			myReader.Events.setInventoryStartEvent(true);
			myReader.Events.setInventoryStopEvent(true);
			myReader.Events.setAccessStartEvent(true);
			myReader.Events.setAccessStopEvent(true);
			myReader.Events.setAntennaEvent(true);
			myReader.Events.setGPIEvent(true);
			myReader.Events.setBufferFullEvent(true);
			myReader.Events.setBufferFullWarningEvent(true);
			myReader.Events.setReaderDisconnectEvent(true);
			myReader.Events.setReaderExceptionEvent(true);
			myReader.Events.setTagReadEvent(true);
			myReader.Events.setAttachTagDataWithReadEvent(false);
			
			this.myReader.Events.addEventsListener(new Fx7500Listener(this,readerHostName,"E"));
			retVal = true;
			this.isConnected = true;

			logger.info("======= 连接上fx7500设备  IP：" + this.myReader.getHostName()+"=======");
		} catch (InvalidUsageException ex) {
			ex.printStackTrace();
		} catch (OperationFailureException ex) {
			ex.printStackTrace();
		}

		return retVal;
	}

	public void disconnectReader() {
		try {
			this.isConnected = false;
			this.myReader.disconnect();
		} catch (InvalidUsageException ex) {
			ex.printStackTrace();
		} catch (OperationFailureException ex) {
			ex.printStackTrace();
		}
	}

	public void startRead() {
		PostFilter myPostFilter = null;
		AntennaInfo myAntennInfo = null;
//		AccessFilter myAccessFilter = null;

		// Set the Antenna Info
		if (antennaInfo.getAntennaID() != null)
			myAntennInfo = antennaInfo;

		// set the post filter
		if (isPostFilterSet)
			myPostFilter = postFilter;

		// set the access filter
//		if (isAccessFilterSet)
//			myAccessFilter = accessFilter;

		try {

			myReader.Actions.Inventory.perform(myPostFilter, triggerInfo,
					myAntennInfo);

		} catch (InvalidUsageException ex) {
			ex.printStackTrace();
		} catch (OperationFailureException ex) {
			ex.printStackTrace();
		}
	}

	public void stopRead() {
		try {
			this.myReader.Actions.Inventory.stop();
		} catch (InvalidUsageException ex) {
			ex.printStackTrace();
		} catch (OperationFailureException ex) {
			ex.printStackTrace();
		}
	}
	

	public String getSelectedTagID() {
		String tagID = null;

		return tagID;
	}

	public static void main(String[] args) throws Exception {
		try {
			Fx7500Client reader = new Fx7500Client();
			reader.connectToReader("192.168.127.254", 5084);
			reader.startRead();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
			System.out.println(df1.format(new Date()) + "门禁未正常启动");
		}
		
		while(true){
			Thread.sleep(1000);
		}
	}

	/**
	 * 815之后没穿 或 之后717 穿了2次 没扫到标签 就717 扫到就不717
	 * 
	 * 
	 * 
	 * 937之后没穿 或 之后717 穿了2次 没扫到标签 就717 扫到就不717
	 * 
	 * 
	 * 没标签 815之后 不走717 或 之后717 穿了2次
	 * 
	 * 
	 * 没标签 937之后 之后717 穿了2次
	 * 
	 * 2017 12 29 测试 发现的规律
	 */
}
