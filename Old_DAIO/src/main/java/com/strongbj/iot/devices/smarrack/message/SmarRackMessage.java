package com.strongbj.iot.devices.smarrack.message;

/**
 * 资产-SmarRack资产定位模块消息类
 * @author yuzhantao
 *
 */
public class SmarRackMessage {
	/**
	 * 命令码
	 */
	private byte command;
	/**
	 * 设备版本号
	 */
	private short devVersion;
	/**
	 * 4位设备ID
	 */
	private byte[] devId=new byte[4];
	/**
	 * 数据
	 */
	private byte[] body;
	
	public SmarRackMessage(byte command,short devVersion,byte[] devId,byte[] body){
		this.command=command;
		this.devVersion=devVersion;
		this.devId=devId;
		this.body=body;
	}
	
	public byte getCommand() {
		return command;
	}
	public void setCommand(byte command) {
		this.command = command;
	}
	public short getDevVersion() {
		return devVersion;
	}
	public void setDevVersion(short devVersion) {
		this.devVersion = devVersion;
	}
	public byte[] getDevId() {
		return devId;
	}
	public void setDevId(byte[] devId) {
		this.devId = devId;
	}
	public byte[] getBody() {
		return body;
	}
	public void setBody(byte[] body) {
		this.body = body;
	}
}
