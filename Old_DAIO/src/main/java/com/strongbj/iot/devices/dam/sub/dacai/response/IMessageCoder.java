package com.strongbj.iot.devices.dam.sub.dacai.response;

import java.util.List;

import com.strongbj.iot.devices.dam.sub.dacai.message.DaCaiMessage;

public interface IMessageCoder {
	/**
	 * 解码
	 * @param datas
	 * @return
	 */
	List<DaCaiMessage> decode(String datas);
}
