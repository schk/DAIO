package com.strongbj.iot.devices.guis.respnose.handle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.core.util.ContextUtils;
import com.strongbj.iot.devices.guis.message.GUISMessage;
import com.strongbj.iot.devices.guis.request.entity.ReturnUInfoEntity;
import com.strongbj.iot.devices.guis.respnose.common.GuisCommonEntity;
import com.strongbj.iot.devices.guis.respnose.message.GUISMQMessage;
import com.strongbj.iot.mq.producer.TopicSender;

import io.netty.channel.ChannelHandlerContext;

/**
 * 解析“上位机控制打开LED灯闪烁” 下位机的回复报文
 * 
 * @author 25969
 *
 */
public class GUISControlOpenLedTwinkleHandle implements IMessageHandle<GUISMessage, Object> {
	private static Logger logger = LogManager.getLogger(GUISControlOpenLedTwinkleHandle.class.getName());
	private TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");
	private static final String rfidtype = "smarrack";
	private static byte ACTION_CODE = Integer.valueOf("23", 16).byteValue();
//	private static SimpleDateFormat df = null;

	@Override
	public boolean isHandle(GUISMessage t) {
		if (ACTION_CODE == t.getCommand()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, GUISMessage t) {
//		logger.info("收到(14号指令)主机 " + ByteUtil.byteArrToHexString(t.getHostNumber()) + " 上位机控制打开LED灯闪烁” 下位机的回复报文，成功状态  "
//				+ ByteUtil.byteArrToHexString(t.getData(), 8, 1));
		// df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		// RedirectOutputStream.put(df.format(new Date())+" 收到主机
		// "+ByteUtil.byteArrToHexString(t.getHostNumber())+" 上位机控制打开LED灯闪烁”
		// 下位机的回复报文，成功状态 "+ ByteUtil.byteArrToHexString(t.getData()));

		GuisCommonEntity guis;
		GUISMQMessage mes;
		ReturnUInfoEntity devs;
		String result;
		try {
			long timeStamp = ByteUtil.bytesToLong(t.getData());
			// String hostNumber = ByteUtil.byteArrToHexString(t.getHostNumber()); //主机编号
			result = ByteUtil.byteArrToHexString(t.getData(), 8, 1); // HID
			/*
			 * GuisCommonEntity guis = GuisCommonEntity.getInstance();
			 * if("01".equals(result)) { guis.put(timeStamp, "2",true); }else {
			 * guis.put(timeStamp, "1",true); }
			 */
			guis = GuisCommonEntity.getInstance();
			guis.del(timeStamp); // 重置
			guis.selDev(timeStamp);
			devs = new ReturnUInfoEntity();
			devs.setDevAddrCode(ByteUtil.byteArrToHexString(t.getHostNumber()));
			if ("01".equals(result)) {
				devs.setSuccess(true);
			} else {
				devs.setSuccess(false);
			}
			mes = new GUISMQMessage();
			mes.setActioncode("reader605");
			mes.setAwsPostdata(devs);
			mes.setRfidtype(rfidtype);
			mes.setTimeStamp(String.valueOf(timeStamp));
			String json = JSON.toJSONString(mes);
			logger.info("GUIS 返回结果给京东 json={}", json);
			topicSender.send("daioReader", json);
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			result = null;
			guis = null;
			devs = null;
			mes = null;
		}

		return null;
	}

	// public static void main(String[] args) {
	// String timeStamp = String.valueOf(System.currentTimeMillis());
	// System.out.println("timeStamp="+timeStamp);
	// byte[] b = new byte[8];
	// b = timeStamp.getBytes();
	// String str = new String(b);
	//// ByteUtil.hexStringToBytes(timeStamp,b,0);
	// System.out.println("timeStamp1="+str);
	//
	// }
}
