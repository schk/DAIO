package com.strongbj.iot.devices.guis.request.handle;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.guis.message.GUISOSMessageFactory;
import com.strongbj.iot.devices.guis.message.MQMessageOfGUIS;
import com.strongbj.iot.devices.guis.request.entity.SetTheMainframeTheIncomeEntity;
import com.strongbj.iot.devices.guis.respnose.common.RedirectOutputStream;
import com.strongbj.iot.devices.guis.respnose.handle.GUISResponseHandleContext;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;

/**
 * 上位机获取主机网络配置信息
 * @author 25969
 *
 */
public class GetHostNetworkInformationHandle implements IMessageHandle<MQMessageOfGUIS,Object>{
	private static Logger logger = LogManager.getLogger(GetHostNetworkInformationHandle.class.getName());
	private final static String ACTION_CODE = "reader028";   //MQ中判断消息类型的标识符
	private final static byte command = (byte)0x22;
	private GUISOSMessageFactory gUISOSMessageFactory = new GUISOSMessageFactory();
//	private TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");
	private static SimpleDateFormat df=null;
	@Override
	public boolean isHandle(MQMessageOfGUIS t) {
		if(t.getActioncode().equals(ACTION_CODE)){
			return true;
		}else{
			return false;
		}
	}
	@Override
	public Object handle(ChannelHandlerContext ctx, MQMessageOfGUIS t) {
		logger.info("GUIS 收到MQ发来的“上位机获取主机网络配置信息” json={}",JSON.toJSONString(t));
		SetTheMainframeTheIncomeEntity setTheMainframeTheIncome = ((JSONObject)t.getAwsPostdata()).toJavaObject(SetTheMainframeTheIncomeEntity.class);
		 this.messageDataToBytes(setTheMainframeTheIncome);
		return null;
	}
	
	/**
	 * 解析并下发数据给下位机
	 * @param setTheMainframeTheIncome
	 */
	private void messageDataToBytes(SetTheMainframeTheIncomeEntity setTheMainframeTheIncome) {
		byte[] data = new byte[0];   //接受主机编号的临时byte数组		
	    byte[] dest = new byte[3];   //接受主机编号的临时byte数组		
		byte[] datas = gUISOSMessageFactory.createGUISOSMessage(ByteUtil.hexStringToBytes(setTheMainframeTheIncome.getDevAddrCode(),dest,0), command, data); 
		ByteBuf bs  = Unpooled.copiedBuffer(datas);
		GUISResponseHandleContext.channels.writeAndFlush(bs);
		logger.info("GUIS 发送上位机获取主机网络配置信息命令成功 message={}",ByteUtil.byteArrToHexString(datas));
		  df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		RedirectOutputStream.put(df.format(new Date())+"  上位机 发送 获取主机网络配置信息命令，报文是     "+ByteUtil.byteArrToHexString(datas));
	}
}
