package com.strongbj.iot.devices.dam.common;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 按照队列执行的任务
 * @author yuzhantao
 *
 */
public class QueueTaskManager implements ITaskManager {
	protected static final long DELAY_SEND_MILLISENCONDS=100;
	protected static final Logger log = LogManager.getLogger(QueueTaskManager.class);
	protected ExecutorService sendThreadPool = Executors.newSingleThreadExecutor();
	@Override
	public void addSendTask(Runnable runnable) {
		sendThreadPool.execute(runnable);
	}
	@Override
	public void destory() {
		try {
			sendThreadPool.shutdown(); // 发送关闭线程请求
	 
	        // 超时后向所有线程发出中断信号
	        if(!sendThreadPool.awaitTermination(5000, TimeUnit.MILLISECONDS)){
	        	sendThreadPool.shutdownNow();
	        }
	    } catch (InterruptedException e) {
	        log.error("", e);
	        sendThreadPool.shutdownNow();
	    }
	}
}
