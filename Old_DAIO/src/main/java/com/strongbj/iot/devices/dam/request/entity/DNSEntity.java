package com.strongbj.iot.devices.dam.request.entity;

/**
 * 网络dns操作
 * @author yuzhantao
 *
 */
public class DNSEntity extends RackCodeEntity {
	/**
	 * DNS
	 */
	private String dns;

	public String getDns() {
		return dns;
	}
	public void setDns(String dns) {
		this.dns = dns;
	}
	
}
