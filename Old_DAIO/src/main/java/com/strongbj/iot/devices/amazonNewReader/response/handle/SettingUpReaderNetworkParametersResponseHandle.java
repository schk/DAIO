package com.strongbj.iot.devices.amazonNewReader.response.handle;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.amazonNewReader.message.NewReaderMessage;
import com.strongbj.iot.devices.guis.respnose.common.RedirectOutputStream;

import io.netty.channel.ChannelHandlerContext;
/**
 * 设置读写器网络参数 
 * @author 25969
 *
 */
public class SettingUpReaderNetworkParametersResponseHandle implements IMessageHandle<NewReaderMessage,Object>{
	private static Logger logger = LogManager.getLogger(SettingUpReaderNetworkParametersResponseHandle.class.getName());
	private static SimpleDateFormat df=null;
	@Override
	public boolean isHandle(NewReaderMessage t) {
		if(Integer.valueOf("12", 16).byteValue()==t.getType()){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, NewReaderMessage t) {
		System.out.println("第13号指令==="+ByteUtil.byteArrToHexString(t.getBody()));
		String hostNumber =  ByteUtil.byteArrToHexString(t.getAddress());   //reader 地址码
		 String timeStamp  =  ByteUtil.byteArrToHexString(t.getTime());   //时间戳
		//：1 字节设置结果 + 4 字节 IP 地址 + 4 字节子网掩码 + 4 字节网关地址 +
	    //4 字节 目标 IP 地址 + 2 字节目标端口 + 保留字段。
	   int result  = ByteUtil.bytesToUbyte(t.getBody(), 0); //设置结果
	   int nativeIPAddress = ByteUtil.bytesToInt(t.getBody(),1); //本机IP地址
       int subnetMask = ByteUtil.bytesToInt(t.getBody(), 5);//本机掩码
       int gateway = ByteUtil.bytesToInt(t.getBody(), 9);  //本机网关
       int targetIaddressIp = ByteUtil.bytesToInt(t.getBody(), 13);//目标IP地址
       int targetPort = ByteUtil.byteArrToShort(t.getBody(), 15);//目标端口号
       String log = "收到[设置读写器地址]返回报文: reader 地址码-"+hostNumber+"  修改结果-"+result +
                    "/IP 地址"+ByteUtil.intToIp(nativeIPAddress)+
                    "/子网掩码"+ByteUtil.intToIp(subnetMask)+
                    "/网关地址"+ByteUtil.intToIp(gateway)+
                    "/目标 IP 地址"+ByteUtil.intToIp(targetIaddressIp)+
                    "/目标端口号"+String.valueOf(targetPort)+
       		        "/时间戳-"+timeStamp;
       logger.info(log);
       df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
	   RedirectOutputStream.put(df.format(new Date())+ log);
       return null; 
	
	}

}
