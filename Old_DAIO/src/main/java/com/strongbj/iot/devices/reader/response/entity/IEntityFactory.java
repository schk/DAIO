package com.strongbj.iot.devices.reader.response.entity;
/**
 * 实体工厂，将字节转为指定实体类
 * @author yuzhantao
 *
 * @param <T>
 */
public interface IEntityFactory<T> {
	T parse(byte[] datas);
}
