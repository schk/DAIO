package com.strongbj.iot.devices.amazonNewReader.message;

import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.amazonNewReader.response.common.CrcForNewReader;


/**
 * 解析业务系统发送的json,封装对应的byte给硬件   的工厂
 * @author 25969
 *
 */
public class AmazonNewReaderOSMessageFactory {
	private final static short HEADER1=0x5A;	// 协议头
	private final static short HEADER2=0xA5;	// 协议头
	
	/**
	 * 
	 * @param hostNumber     主机编号 
	 * @param command         命令 
	 * @param data            数据段
	 * @return
	 */
	public byte[] createAmazonNewReaderOSMessage(
		byte[] hostNumber,
		byte command, 
		byte[] datas,
		byte[] timeStamp
    ) {
		// 获取数据长度
		int dataLen = datas==null?0:datas.length;
		// 在内存中开辟一条协议数据的空间
		byte[] result = new byte[16+dataLen];
		// 设置协议头
		result[0]=(byte)HEADER1;
		result[1]=(byte)HEADER2;
		//设置主机编号
		System.arraycopy(hostNumber, 0, result, 2, 3);
		//设置命令字
		result[5]=command;
		//设置数据长度
		byte[] bLen = ByteUtil.shortToByteArr((short)(dataLen));
		System.arraycopy(bLen, 0, result, 6, bLen.length);
		result[8]=(byte)1;   //总帧数
		result[9]=(byte)1;   //帧序列号
		//填充数据
		System.arraycopy(datas, 0, result, 10, dataLen);
		
		//时间戳
		System.arraycopy(timeStamp, 0, result, 10+datas.length, timeStamp.length);
		// 设置CRC校验位
		byte[] crc = ByteUtil.shortToByteArr((short)CrcForNewReader.app_cal_crc16(result, result.length-2));
		System.arraycopy(crc, 0, result, 14+datas.length, crc.length);
		return result;
		
		
	}
}
