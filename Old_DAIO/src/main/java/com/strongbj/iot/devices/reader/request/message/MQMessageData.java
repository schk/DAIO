package com.strongbj.iot.devices.reader.request.message;

public class MQMessageData<K> {
	private String devIp;
	private String devAddrCode;
	private K datas;
	public String getDevIp() {
		return devIp;
	}
	public void setDevIp(String devIp) {
		this.devIp = devIp;
	}
	public K getDatas() {
		return datas;
	}
	public void setDatas(K datas) {
		this.datas = datas;
	}
	public String getDevAddrCode() {
		return devAddrCode;
	}
	public void setDevAddrCode(String devAddrCode) {
		this.devAddrCode = devAddrCode;
	}
}
