package com.strongbj.iot.devices.dam.request.handle.dam;

/**
 * 查看网络参数
 * 
 * @author yuzhantao
 *
 */
public class GetNetParamsHandle extends GetParamsHandle {
	public GetNetParamsHandle() {
		super("getNetParams", (byte) 0x13);
	}
}