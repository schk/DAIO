package com.strongbj.iot.devices.hr.message;

import com.strongbj.core.util.ByteUtil;

public class HRMessageFactory {
	private final static byte HEADER=0x1B;		// 协议头
	
	/**
	 * 
	 * @param command	用于区分各个操作的指令
	 * @param sequence	指令序列号(PC->读写器 0x00,读写器->PC 0x01)
	 * @param opcode	指令操作码 0xXX 协议预留字节(在 485 通讯中可当作设备地址使用)
	 * @param datas		数据
	 * @return
	 * @throws Exception
	 */
	public byte[] createHRMessage(
			byte command,
			byte sequence,
			byte opcode,
			byte[] datas
			) {
		// 获取数据长度
		int dataLen = datas==null?0:datas.length;
		// 在内存中开辟一条协议数据的空间
		byte[] result = new byte[7+dataLen];
		// 设置协议头
		result[0]=HEADER;
		// 设置命令吗
		result[1]=command;
		// 设置指令序列号(PC->读写器 0x00,读写器->PC 0x01)
		result[2]=sequence;
		// 指令操作码 0xXX 协议预留字节(在 485 通讯中可当作设备地址使用)
		result[3]=opcode;
		// 设置数据长度
		ByteUtil.shortToByteArr(dataLen,result,4,1);
		// 设置校验码
		byte bcc=0;
		if(dataLen>0){
			System.arraycopy(datas, 0, result, 6, datas.length);
			for(byte b : datas){
				bcc^=b;
			}
		}
		result[result.length-1]=bcc;
		return result;
	}
}
