package com.strongbj.iot.devices.amazonreader.request.entity;

/**
 * 数据采集时间间隔类
 * @author yuzhantao
 *
 */
public class ReaderCollectionTimeEntity {
	private byte readerTime;

	public byte getReaderTime() {
		return readerTime;
	}
	public void setReaderTime(byte readerTime) {
		this.readerTime = readerTime;
	}
}
