package com.strongbj.iot.devices.dam.response.entity;

public class SensorParamesEntity {
	private int sensorPosition;
	private int sensorType;// 1温度   2湿度  3门, 
	private Object sensorDatas; // 温度或湿度值
	public int getSensorPosition() {
		return sensorPosition;
	}
	public void setSensorPosition(int sensorPosition) {
		this.sensorPosition = sensorPosition;
	}
	public int getSensorType() {
		return sensorType;
	}
	public void setSensorType(int sensorType) {
		this.sensorType = sensorType;
	}
	public Object getSensorDatas() {
		return sensorDatas;
	}
	public void setSensorDatas(Object sensorDatas) {
		this.sensorDatas = sensorDatas;
	}

}
