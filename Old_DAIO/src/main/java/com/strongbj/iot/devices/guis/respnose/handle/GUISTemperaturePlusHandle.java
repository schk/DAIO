package com.strongbj.iot.devices.guis.respnose.handle;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.dam.request.entity.ScreenCommandEntity;
import com.strongbj.iot.devices.dam.response.entity.SensorParamesEntity;
import com.strongbj.iot.devices.guis.message.GUISMessage;
import com.strongbj.iot.devices.guis.message.MQMessageOfGUIS;

import io.netty.channel.ChannelHandlerContext;

/**
 * 扩展温度的处理
 * 
 * @author yuzhantao
 *
 */
public class GUISTemperaturePlusHandle  implements IMessageHandle<GUISMessage, Object>{
	private final static Logger logger = LogManager.getLogger(GUISTemperaturePlusHandle.class);

	/**
	 * 帧头
	 */
	protected final static String COMMAND_HEADER = "EE";
	/**
	 * 帧尾
	 */
	protected final static String COMMAND_FOOTER = "FFFCFFFF";
	protected final static String COMMAND_CODE = "B112";

	@Override
	public boolean isHandle(GUISMessage t) {
		if (Integer.valueOf("88", 16).byteValue() == t.getCommand()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, GUISMessage t) {
		//super.handle(ctx,t);
		if (t.getData() == null)
			return null;
		int temperatureCount = t.getData().length / 3; // 获取温度的数量
		if (temperatureCount == 0)
			return null;

		Map<Integer, Integer> uMap = new HashMap<>();
		int[] us = new int[temperatureCount];
		for (int i = 0; i < temperatureCount; i++) {
			us[i] = t.getData()[i * 3];
			int temp = (int) ByteUtil.byteArrToShort(t.getData(), i * 3 + 1) / 16;
			uMap.put(us[i], temp);
			byte[] dd = new byte[2];
			System.arraycopy(t.getData(), i * 3 + 1, dd, 0, 2);
			logger.debug("[{}]扩展温度:{} value:{}", us[i], ByteUtil.byteArrToHexString(dd), temp);
		}
		Arrays.sort(us);

		List<SensorParamesEntity> sensorList = new ArrayList<>();

		SensorParamesEntity sp1 = new SensorParamesEntity();
		if (us.length > 0) {
			sp1.setSensorPosition(4);
			sp1.setSensorDatas(uMap.get(us[0]));
			sp1.setSensorType(1);
			sensorList.add(sp1);
		}

		SensorParamesEntity sp2 = new SensorParamesEntity();
		if (us.length > 1) {
			sp2.setSensorPosition(6);
			sp2.setSensorDatas(uMap.get(us[1]));
			sp2.setSensorType(1);
			sensorList.add(sp2);
		}

		if (sensorList != null && sensorList.size() > 0) {
			String devCode = ByteUtil.byteArrToHexString(t.getHostNumber());
			JSONObject object = new JSONObject();
			object.put("devAddrCode", devCode);
			try {
				object.put("backTop", String.valueOf(sensorList.get(0).getSensorDatas()));
			} catch (Exception e) {
				logger.error("", e);
			}
			try {
				object.put("backBottom", String.valueOf(sensorList.get(1).getSensorDatas()));
			} catch (Exception e) {
				logger.error("", e);
			}

			// TODO 后门两个温度暂时硬件未实现
//			object.put("backMiddle", String.valueOf(sensorList.get(4).getSensorDatas()));
//			object.put("backBottom", String.valueOf(sensorList.get(5).getSensorDatas()));
			logger.debug("收到温度数据：{}", object.toJSONString());
		}
		try {
			sendtempHistoryDataToScreen(ctx, t, sp1, sp2);
		} catch (Exception e) {
			logger.error("", e);
		}
		return null;
	}

	/**
	 * 发送温度数据到屏幕
	 * 
	 * @param ctx
	 * @param t
	 * @throws UnsupportedEncodingException
	 */
	private void sendtempHistoryDataToScreen(ChannelHandlerContext ctx, GUISMessage t, SensorParamesEntity se1,
			SensorParamesEntity se2) throws UnsupportedEncodingException {
		SensorParamesEntity se3 = new SensorParamesEntity();
		Map<Short, String> sensorMap = new HashMap<>();
		if (se1 != null) {
			sensorMap.put((short) se1.getSensorPosition(), "0".equals(String.valueOf(se1.getSensorDatas()))?"--℃":String.valueOf(se1.getSensorDatas()) + "℃");
		}else{
			sensorMap.put((short) se1.getSensorPosition(),  "--℃");
		}
		if (se2 != null) {
			sensorMap.put((short) se2.getSensorPosition(), "0".equals(String.valueOf(se2.getSensorDatas()))?"--℃":String.valueOf(se2.getSensorDatas()) + "℃");
		}else{
			sensorMap.put((short) se2.getSensorPosition(),  "--℃");
		}
		se3.setSensorPosition(5);
		if (se3 != null) {
			sensorMap.put((short) se3.getSensorPosition(), "--℃");
		}

		sendMessage(ctx, t.getHostNumber(), sensorMap);
	}

	private void sendMessage(ChannelHandlerContext ctx, byte[] devAddress, Map<Short, String> sensorMap)
			throws UnsupportedEncodingException {
		String datas = createSensorMessage(sensorMap);
		MQMessageOfGUIS mm = new MQMessageOfGUIS();
		ScreenCommandEntity sc = new ScreenCommandEntity();
		sc.setRackConverCode(ByteUtil.byteArrToHexString(devAddress));
		sc.setCommand(datas);
		mm.setAwsPostdata(JSON.toJSONString(sc));
		com.strongbj.iot.devices.guis.request.handle.TransmissionHandle th = new com.strongbj.iot.devices.guis.request.handle.TransmissionHandle();
		th.handle(ctx, mm);
	}

	private String createSensorMessage(Map<Short, String> sensorMap) throws UnsupportedEncodingException {
		StringBuffer sb = new StringBuffer(COMMAND_HEADER);
		sb.append(COMMAND_CODE);
		sb.append("0001"); // 界面id
		for (Entry<Short, String> item : sensorMap.entrySet()) {
			sb.append(ByteUtil.byteArrToHexString(ByteUtil.shortToByteArr(item.getKey()))); // 控件id
			byte[] datas = item.getValue().getBytes("GBK");
			short dataLen = (short) datas.length;
			sb.append(ByteUtil.byteArrToHexString(ByteUtil.shortToByteArr(dataLen))); // 控件内容长度
			sb.append(ByteUtil.byteArrToHexString(datas)); // 控件内容
		}
		sb.append(COMMAND_FOOTER);

		return sb.toString();
	}
}
