package com.strongbj.iot.devices.ru.response.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.ru.message.RuMessageFactory;
import com.strongbj.iot.devices.ru.response.handle.RUResponseHandleContext;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.group.ChannelGroupFuture;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;

/**
 * 定时发送获取温度命令
 * @author yuzhantao
 *
 */
@Service
@Lazy(false)
public class QuestRu1000TemperatureTask {
	private Logger logger = LoggerFactory.getLogger(QuestRu1000TemperatureTask.class);
	private final static byte[] QUEST_TEMPERATURE_COMMAND = RuMessageFactory.createMessage(
			new byte[] {(byte) 0xFF,(byte) 0xFF,(byte) 0xFF}, (byte)0x12, new byte[] {0x01});
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Scheduled(cron = "0/10 * * * * ?")
	public void sendQuestOfTemperature() {
		ByteBuf bs = Unpooled.copiedBuffer(QUEST_TEMPERATURE_COMMAND);
		ChannelGroupFuture future = RUResponseHandleContext.channels.writeAndFlush(bs);
		future.addListener(new GenericFutureListener() {
			private final String SEND_COMMAND = ByteUtil.byteArrToHexString(QUEST_TEMPERATURE_COMMAND,true);
			@Override
			public void operationComplete(Future future) throws Exception {
				if(future.isSuccess()) {
					logger.info("发送温度请求命令成功,发送命令:"+SEND_COMMAND);
				}else {
					logger.info("发送温度请求命令失败,发送命令:"+SEND_COMMAND);
				}
			}
			
		});
	}
}
