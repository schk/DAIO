package com.strongbj.iot.devices.guis.respnose.handle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.core.util.ContextUtils;
import com.strongbj.iot.devices.dam.message.MQMessageOfDAM;
import com.strongbj.iot.devices.dam.request.entity.ScreenCommandEntity;
import com.strongbj.iot.devices.dam.response.handle.v1.ScreenToServerCommandHandle;
import com.strongbj.iot.devices.guis.message.GUISMessage;
import com.strongbj.iot.devices.guis.message.MQMessageOfGUIS;
import com.strongbj.iot.devices.guis.respnose.common.CommonSendToCustomer;
import com.strongbj.iot.mq.producer.TopicSender;

import io.netty.channel.ChannelHandlerContext;

/**
 * 数据透传的处理
 * 
 * @author yuzhantao
 *
 */
public class TransmissionHandle implements IMessageHandle<GUISMessage, Object> {
	private TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");
	private static Logger logger = LogManager.getLogger(ScreenToServerCommandHandle.class.getName());
	private static final String TOPIC = "damCommandToService";
	private static final String ACTION_CODE = "screenRequest";
	private static final String DEV_TYPE = "damDc";
	private static final String TAG_NULL = "00000000";

	@Override
	public boolean isHandle(GUISMessage t) {
		if (Integer.valueOf("32", 16).byteValue() == t.getCommand()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, GUISMessage t) {
		String hostNumber = ByteUtil.byteArrToHexString(t.getHostNumber()); // 主机编号
		logger.info("收到硬件发来的小屏透传数据 recive screen");
		if (TAG_NULL.equals(hostNumber)) return null;
		
		try {
			String command = ByteUtil.byteArrToHexString(t.getData());

			if (command.replace("0", "").length() == 0)
				return null; // 如果返回的数据全是0，则不处理

			MQMessageOfDAM msg = new MQMessageOfDAM();
			msg.setActioncode(ACTION_CODE);
			ScreenCommandEntity result = new ScreenCommandEntity();
			result.setCommand(command);

			result.setRackConverCode(hostNumber);
			msg.setAwsPostdata(result);
			msg.setDevType(DEV_TYPE);
			msg.setTimestamp(System.currentTimeMillis());
			String json = JSON.toJSONString(msg);
			logger.info("GUIS设备 向MQ发送屏幕命令数据	 JSON={}", json);
			topicSender.send(TOPIC, json);
			
			CommonSendToCustomer.commonSendMessageToCustomer(ctx, t, (byte) 0, 1); // 上位机回复信息
			
			this.test(ctx, hostNumber, "EEB11000150004"+ByteUtil.byteArrToHexString(command.getBytes("GBK"))+"FFFCFFFF");
		} catch (Exception e) {
			CommonSendToCustomer.commonSendMessageToCustomer(ctx, t, (byte) 1, 1); // 上位机回复信息
			logger.error("", e);
		}
		return null;
	}
	
	private void test(ChannelHandlerContext ctx,String devCode,String datas) {
		MQMessageOfGUIS mm = new MQMessageOfGUIS();
		ScreenCommandEntity sc =new ScreenCommandEntity();
		sc.setRackConverCode(devCode);
		sc.setCommand(datas);
		mm.setAwsPostdata(sc);
		com.strongbj.iot.devices.guis.request.handle.TransmissionHandle th = new com.strongbj.iot.devices.guis.request.handle.TransmissionHandle();
		th.handle(ctx, mm);
	}

}
