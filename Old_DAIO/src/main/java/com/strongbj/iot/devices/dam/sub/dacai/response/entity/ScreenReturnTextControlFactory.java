package com.strongbj.iot.devices.dam.sub.dacai.response.entity;

/**
 * 屏幕返回的文本控件信息
 * @author yuzhantao
 *
 */
public class ScreenReturnTextControlFactory {
	public static ScreenReturnTextControlEntity create(String msg) {
		ScreenReturnTextControlEntity sr = new ScreenReturnTextControlEntity();
		sr.setScreenId(Integer.valueOf(msg.substring(0,4),16).shortValue());
		sr.setControlId(Integer.valueOf(msg.substring(4,8),16).shortValue());
		sr.setControlType(Integer.valueOf(msg.substring(8,10),16).byteValue());
		sr.setContent(msg.substring(10));
		return sr;
	}
}
