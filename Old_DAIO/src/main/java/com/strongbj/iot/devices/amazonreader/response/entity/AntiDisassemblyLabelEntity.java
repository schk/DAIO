package com.strongbj.iot.devices.amazonreader.response.entity;
/**
 * 防拆标签
 * @author yuzhantao
 *
 */
public class AntiDisassemblyLabelEntity extends BaseLabelEntity {

	@Override
	public int getLength() {
		return 10;
	}

}
