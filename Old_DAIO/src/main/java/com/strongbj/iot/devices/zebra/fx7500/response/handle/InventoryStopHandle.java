package com.strongbj.iot.devices.zebra.fx7500.response.handle;

import com.mot.rfid.api3.RfidStatusEvents;
import com.mot.rfid.api3.STATUS_EVENT_TYPE;
import com.mot.rfid.api3.TagData;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.iot.devices.zebra.fx7500.response.entity.TagCache;

import io.netty.channel.ChannelHandlerContext;

/**
 * 标签清单结束的处理事件
 * @author jeff
 *
 */
public class InventoryStopHandle implements IMessageHandle<RfidStatusEvents,Object> {
	private TagCache tagCache;
	public InventoryStopHandle(TagCache tagCache){
		this.tagCache=tagCache;
	}
	
	@Override
	public boolean isHandle(RfidStatusEvents t) {
		STATUS_EVENT_TYPE statusType = t.StatusEventData.getStatusEventType();
		if(statusType == STATUS_EVENT_TYPE.INVENTORY_STOP_EVENT){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, RfidStatusEvents t) {
		System.out.println("标签处理结束==============");
		for(TagData tg : this.tagCache.getTags()) {
			System.out.println(tg.getTagID());
		}
		this.tagCache.clear();
		return null;
	}

}