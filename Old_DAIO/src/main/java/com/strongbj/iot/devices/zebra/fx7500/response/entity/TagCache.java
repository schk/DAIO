package com.strongbj.iot.devices.zebra.fx7500.response.entity;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.mot.rfid.api3.TagData;

/**
 * 标签缓存
 * @author yuzhantao
 *
 */
public class TagCache {
	private Map<String,TagData> tagMap;
	
	public TagCache(){
		this.tagMap = new HashMap<String, TagData>();
	}
	
	public int size() {
		return tagMap.size();
	}
	
	public synchronized void addTags(TagData[] tags){
		for(int i=0;i<tags.length;i++){
			String key = tags[i].getTagID();
			tagMap.put(key, tags[i]);
		}
	}
	
	public void clear(){
		this.tagMap.clear();
	}
	
	public Collection<TagData> getTags(){
		return this.tagMap.values();
	}
}
