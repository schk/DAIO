package com.strongbj.iot.devices.dam.sub.dacai.response.handle;

import java.util.List;

import com.strongbj.core.message.MessageHandleContext;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.dam.message.DAMMessage;
import com.strongbj.iot.devices.dam.response.handle.v1.DAMV1Handle;
import com.strongbj.iot.devices.dam.sub.dacai.message.DaCaiMessage;
import com.strongbj.iot.devices.dam.sub.dacai.response.IMessageCoder;
import com.strongbj.iot.devices.dam.sub.dacai.response.coder.DaCaiMessageCoder;

import io.netty.channel.ChannelHandlerContext;

/**
 * DAM转大彩屏幕的消息处理
 * @author yuzhantao
 *
 */
public class DAMToDaCaiScreenHandleContext extends DAMV1Handle {
	protected static final byte COMMAND = Integer.valueOf("FE", 16).byteValue();

	protected IMessageCoder dcMessageCoder; // 大彩消息编码器
	protected MessageHandleContext<DaCaiMessage, Object> dcMessageHandleContent;
	
	public DAMToDaCaiScreenHandleContext(DAMMessage dam) {
		this.dcMessageCoder = new DaCaiMessageCoder();
		this.dcMessageHandleContent = new MessageHandleContext<>();
		this.dcMessageHandleContent.addHandleClass(new OpenNetParamsPageHandle());
		this.dcMessageHandleContent.addHandleClass(new OpenSensorParamsPageHandle());
		this.dcMessageHandleContent.addHandleClass(new SetLocalNetParamsHandle());
		this.dcMessageHandleContent.addHandleClass(new SetDestServerNetParamsHandle());
		this.dcMessageHandleContent.addHandleClass(new SetSensorParamsHandle());
		this.dcMessageHandleContent.addHandleClass(new FindDeviceCodeByIP());
		this.dcMessageHandleContent.addHandleClass(new SelectBindRFIDMenuItemHandle(dam));
//		this.dcMessageHandleContent.addHandleClass(new OpenXModeHandle());
	}

	@Override
	protected 
	boolean isHandleDAMMessage(DAMMessage t) {
		if (t.getCommand() == COMMAND) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public Object handle(ChannelHandlerContext ctx, DAMMessage t) {
		List<DaCaiMessage> list = dcMessageCoder.decode(ByteUtil.byteArrToHexString(t.getDatas()));
		if (list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				list.get(i).setDamCode(ByteUtil.byteArrToHexString(t.getAddressCode()));
				this.dcMessageHandleContent.handle(ctx, list.get(i));
			}
		}
		return null;
	}
}
