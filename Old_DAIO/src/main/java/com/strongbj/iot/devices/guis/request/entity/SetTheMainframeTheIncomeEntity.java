package com.strongbj.iot.devices.guis.request.entity;
/**
 * 上位机设置主机流水号  实体类
 * @author 25969
 *
 */
public class SetTheMainframeTheIncomeEntity {
  
	private String devAddrCode;   //机柜编号
	
	private String devIp;         // 机柜IP
	
	private int hardwareMainVersionNumber;  //硬件主版本号
	
	private int hardwareMinorVersionNumber; //硬件副版本号
	
	private int softwareMainVersionNumbe;   //软件主版本号
	
	private int softwareMinorVersionNumber; //软件副版本号
	
	private String macAddress;              //6个字节mac地址
	
	private String nativeIPAddress;         //本机IP地址
	
	private String subnetMask;              //子网掩码
	
	private String gateway;                 //网关
	
	private int localPort;                  //本机端口
	
	private String targetIaddressIp;        //目标IP
	
	private int targetPort;                 //目标端口
	
	private String dns;                     //dns
	
	private int ubitReadingSequence;        //U位读取顺序，01表示1到X；FF表示从X到1
	
	private int startingUbitNumber;         //起始U位编号
	
	private int timingPushSeconds;          //定时推送U位信息的秒数，如为0则不推送
	
	public String getDevAddrCode() {
		return devAddrCode;
	}

	public void setDevAddrCode(String devAddrCode) {
		this.devAddrCode = devAddrCode;
	}

	public String getDevIp() {
		return devIp;
	}

	public void setDevIp(String devIp) {
		this.devIp = devIp;
	}

	public int getHardwareMainVersionNumber() {
		return hardwareMainVersionNumber;
	}

	public void setHardwareMainVersionNumber(int hardwareMainVersionNumber) {
		this.hardwareMainVersionNumber = hardwareMainVersionNumber;
	}

	public int getHardwareMinorVersionNumber() {
		return hardwareMinorVersionNumber;
	}

	public void setHardwareMinorVersionNumber(int hardwareMinorVersionNumber) {
		this.hardwareMinorVersionNumber = hardwareMinorVersionNumber;
	}

	public int getSoftwareMainVersionNumbe() {
		return softwareMainVersionNumbe;
	}

	public void setSoftwareMainVersionNumbe(int softwareMainVersionNumbe) {
		this.softwareMainVersionNumbe = softwareMainVersionNumbe;
	}

	public int getSoftwareMinorVersionNumber() {
		return softwareMinorVersionNumber;
	}

	public void setSoftwareMinorVersionNumber(int softwareMinorVersionNumber) {
		this.softwareMinorVersionNumber = softwareMinorVersionNumber;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getNativeIPAddress() {
		return nativeIPAddress;
	}

	public void setNativeIPAddress(String nativeIPAddress) {
		this.nativeIPAddress = nativeIPAddress;
	}

	public String getSubnetMask() {
		return subnetMask;
	}

	public void setSubnetMask(String subnetMask) {
		this.subnetMask = subnetMask;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}


	public String getTargetIaddressIp() {
		return targetIaddressIp;
	}

	public void setTargetIaddressIp(String targetIaddressIp) {
		this.targetIaddressIp = targetIaddressIp;
	}


	public String getDns() {
		return dns;
	}

	public void setDns(String dns) {
		this.dns = dns;
	}

	public int getLocalPort() {
		return localPort;
	}

	public void setLocalPort(int localPort) {
		this.localPort = localPort;
	}

	public int getTargetPort() {
		return targetPort;
	}

	public void setTargetPort(int targetPort) {
		this.targetPort = targetPort;
	}

	public int getUbitReadingSequence() {
		return ubitReadingSequence;
	}

	public void setUbitReadingSequence(int ubitReadingSequence) {
		this.ubitReadingSequence = ubitReadingSequence;
	}

	public int getStartingUbitNumber() {
		return startingUbitNumber;
	}

	public void setStartingUbitNumber(int startingUbitNumber) {
		this.startingUbitNumber = startingUbitNumber;
	}

	public int getTimingPushSeconds() {
		return timingPushSeconds;
	}

	public void setTimingPushSeconds(int timingPushSeconds) {
		this.timingPushSeconds = timingPushSeconds;
	}

	
	
	
	
}
