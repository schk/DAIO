package com.strongbj.iot.devices.hr.response.entity;

/**
 * 控制蜂鸣器发声反馈实体类
 * 
 * @author Pingfan
 *
 */
public class HRControlBuzzerSoundResponseEntity {
	/**
	 * 门禁设备的IP地址
	 */
	private String devIp;
	/**
	 * 用于区分各个操作的指令
	 */
	private byte command;
	/**
	 * 控制蜂鸣器发声是否成功(data为0x00成功, 0x80失败)
	 */
	private boolean isControlBuzzerSoundSuccess;
	public String getDevIp() {
		return devIp;
	}
	public void setDevIp(String devIp) {
		this.devIp = devIp;
	}
	public byte getCommand() {
		return command;
	}
	public void setCommand(byte command) {
		this.command = command;
	}
	public boolean isControlBuzzerSoundSuccess() {
		return isControlBuzzerSoundSuccess;
	}
	public void setControlBuzzerSoundSuccess(boolean isControlBuzzerSoundSuccess) {
		this.isControlBuzzerSoundSuccess = isControlBuzzerSoundSuccess;
	}
}
