package com.strongbj.iot.devices.amazonreader.response.entity;

public enum LedLabelLightState {
	/**
	 * 开
	 */
	On,
	/**
	 * 关
	 */
	Off
}
