package com.strongbj.iot.devices.guis.message;

import com.alibaba.fastjson.annotation.JSONField;

public class MQMessageOfGUIS {
	private String actioncode;
	@JSONField(name="postdata")
	private Object awsPostdata;
	
	private long timeStamp;
	public String getActioncode() {
		return actioncode;
	}
	public void setActioncode(String actioncode) {
		this.actioncode = actioncode;
	}
	public Object getAwsPostdata() {
		return awsPostdata;
	}
	public void setAwsPostdata(Object awsPostdata) {
		this.awsPostdata = awsPostdata;
	}
	public long getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	
}
