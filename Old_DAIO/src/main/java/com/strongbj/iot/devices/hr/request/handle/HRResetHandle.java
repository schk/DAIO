package com.strongbj.iot.devices.hr.request.handle;

import java.net.InetSocketAddress;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.hr.message.HRMessageFactory;
import com.strongbj.iot.devices.hr.message.MQMessageOfHR;
import com.strongbj.iot.devices.hr.response.handle.HRResponseHandleContext;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.group.ChannelMatcher;

/**
 * 门禁重启处理类
 * 
 * @author Pingfan
 *
 */
public class HRResetHandle implements IMessageHandle<MQMessageOfHR, Object> {
	private static Logger logger = LogManager.getLogger(BuzzerHandle.class.getName());
	private final static String ACTION_CODE = "collect013";
	private HRMessageFactory hrMessageFactory = new HRMessageFactory();
	
	@Override
	public boolean isHandle(MQMessageOfHR t) {
		if (t.getActioncode().equals(ACTION_CODE)) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public Object handle(ChannelHandlerContext ctx, MQMessageOfHR t) {
		// 取门禁ip
		JSONObject jsonObject = (JSONObject) JSON.toJSON(t.getAwsPostdata());
		String ip = jsonObject.getString("devIp");
		// 发送重启指令到门禁
		byte[] hrResetCommand = hrMessageFactory.createHRMessage((byte) 0x0D, (byte) 0x00, (byte) 0x00, null);
		ByteBuf bs = Unpooled.copiedBuffer(hrResetCommand);
		HRResponseHandleContext.channels.writeAndFlush(bs, new MyChannelMatchers(ip));
		logger.info("向HR设备发送【重启】成功, ip={}, 数据={}", ip, ByteUtil.byteArrToHexString(hrResetCommand, true));
		return null;
	}
	
	class MyChannelMatchers implements ChannelMatcher {
		private String currentIP;

		public MyChannelMatchers(String ip) {
			this.currentIP = ip;
		}

		@Override
		public boolean matches(Channel channel) {
			InetSocketAddress insocket = (InetSocketAddress) channel.remoteAddress();
			if (insocket.getAddress().getHostAddress().equals(this.currentIP)) {
				return true;
			} else {
				return false;
			}
		}
	}
}
