package com.strongbj.iot.devices.reader.response.entity;

public enum LedLabelLightState {
	/**
	 * 开
	 */
	On,
	/**
	 * 关
	 */
	Off
}
