package com.strongbj.iot.devices.amazonNewReader.response.handle;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.amazonNewReader.message.NewReaderMessage;
import com.strongbj.iot.devices.amazonNewReader.request.entity.T;
import com.strongbj.iot.devices.guis.respnose.common.RedirectOutputStream;

import io.netty.channel.ChannelHandlerContext;
/**
 * LED 控制 针对多个标签
 * @author 25969
 *
 */
public class LedControlForManyResponseHandle implements IMessageHandle<NewReaderMessage,Object>{
	private static Logger logger = LogManager.getLogger(LedControlForManyResponseHandle.class.getName());
	private static SimpleDateFormat df=null;
	@Override
	public boolean isHandle(NewReaderMessage t) {
		if(Integer.valueOf("6", 16).byteValue()==t.getType()){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, NewReaderMessage t) {
		System.out.println("多个LED控制reader返回报文==="+ByteUtil.byteArrToHexString(t.getBody()));
		String hostNumber =  ByteUtil.byteArrToHexString(t.getAddress());   //reader 地址码
		 String timeStamp  =  ByteUtil.byteArrToHexString(t.getTime());   //时间戳
		 //1 字节控制成功个数 + 1 字节控制失败个数 + （2 字节标签短地址 + 2 字节标签 信息）* 控制成功个数 + 2 字节标签短地址 * 控制失败个数 + 保留字段
		 int ControlSuccessNumber  = ByteUtil.bytesToUbyte(t.getBody(), 0); //控制成功个数
		 int ControlFailureNumber  = ByteUtil.bytesToUbyte(t.getBody(), 1); //控制失败个数
		 
		 if(ControlSuccessNumber >0) {
			 handleControlSuccessNumber(ControlSuccessNumber,hostNumber,timeStamp,t);
			 handleControlFailureNumber(ControlSuccessNumber,ControlFailureNumber,hostNumber,timeStamp,t);
		 }else {
			 handleControlFailureNumber(ControlFailureNumber,hostNumber,timeStamp,t);
		 }
		 
		return null;
	
	
	
	}



	private void handleControlSuccessNumber(int ControlSuccessNumber,String hostNumber,String timeStamp,NewReaderMessage t) {
		 for(int i = 2;i<(2+4*ControlSuccessNumber);i+=4) {
		 String shortAddress = ByteUtil.byteArrToHexString(t.getBody(), i, 2);  // LED 短地址
       String led = T.byteToBit(t.getBody()[i+2]);  //LED  字节
       //LED 状态 ： 0 ：灭   1： 慢闪   2：快闪  3：长亮
       int led4 = Integer.parseInt(led.substring(0, 2),2);  //LED4 状态 
       int led3 = Integer.parseInt(led.substring(2, 4),2);  //LED3 状态
       int led2 = Integer.parseInt(led.substring(4, 6),2);  //LED2 状态
       int led1 = Integer.parseInt(led.substring(led.length()-2),2);  //LED1 状态
       //
       String state  = T.byteToBit(t.getBody()[i+3]);
       int electricityThreshold=Integer.parseInt(state.substring(0, 3),2);  //电量阈值，0~6 
       int electricityQuantity =Integer.parseInt(state.substring(3, 6),2);  //当前电量,0~6 
       int keyState = Integer.parseInt(state.substring(state.length()-2),2); //按键状态   
       String log ="收到【LED 控制，针对多个标签】 请求返回报文: reader 地址码-"+hostNumber+"  LED 短地址-"+shortAddress +
    		" 控制成功个数 -"+ControlSuccessNumber+
       		"/ LED4 状态 -"+led4+
       		"/ LED3 状态 -"+led3+
       		"/ LED2 状态 -"+led2+
       		"/ LED1 状态 -"+led1+
       		"/ 电量阈值 -"+electricityThreshold+
       		" 当前电量 -"+electricityQuantity+
       		" 按键状态 -"+keyState
       		+" 时间戳-"+timeStamp;
       logger.info(log);
       df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
	     RedirectOutputStream.put(df.format(new Date())+ log);
		 }
		
	}
	
	
	private void handleControlFailureNumber(int controlFailureNumber, String hostNumber, String timeStamp,
			NewReaderMessage t) {
		 for(int i = 2;i<(2+2*controlFailureNumber);i+=2) {
			 String shortAddress = ByteUtil.byteArrToHexString(t.getBody(), i, 2);  // LED 短地址
		     String log= "收到【LED 控制，针对多个标签】 请求返回报文: reader 地址码-"+hostNumber+" 失败的 LED 短地址-"+shortAddress;
			 logger.info(log);
			 df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		     RedirectOutputStream.put(df.format(new Date())+ log);
		 }
		
	}
	
	
	private void handleControlFailureNumber(int controlSuccessNumber, int controlFailureNumber, String hostNumber,
			String timeStamp, NewReaderMessage t) {
		 for(int i = 2+4*controlSuccessNumber+1;i<(2+4*controlSuccessNumber+2*controlFailureNumber);i+=2) {
			 String shortAddress = ByteUtil.byteArrToHexString(t.getBody(), i, 2);  // LED 短地址
		     String log= "收到【LED 控制，针对多个标签】 请求返回报文: reader 地址码-"+hostNumber+" 失败的 LED 短地址-"+shortAddress;
			 logger.info(log);
			 df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		     RedirectOutputStream.put(df.format(new Date())+ log);
		 }
		
	}

}
