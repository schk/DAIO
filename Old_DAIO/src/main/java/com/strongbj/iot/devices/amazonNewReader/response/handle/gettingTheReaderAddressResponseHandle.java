package com.strongbj.iot.devices.amazonNewReader.response.handle;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.amazonNewReader.message.NewReaderMessage;
import com.strongbj.iot.devices.guis.respnose.common.RedirectOutputStream;

import io.netty.channel.ChannelHandlerContext;
/**
 * 获取读写器当前地址 
 * @author 25969
 *
 */
public class gettingTheReaderAddressResponseHandle implements IMessageHandle<NewReaderMessage,Object>{
	private static Logger logger = LogManager.getLogger(gettingTheReaderAddressResponseHandle.class.getName());
	private static SimpleDateFormat df=null;
	@Override
	public boolean isHandle(NewReaderMessage t) {
		if(Integer.valueOf("11", 16).byteValue()==t.getType()){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, NewReaderMessage t) {
		String hostNumber =  ByteUtil.byteArrToHexString(t.getAddress());   //reader 地址码
		 String timeStamp  =  ByteUtil.byteArrToHexString(t.getTime());   //时间戳
		 //3 字节读写器地址 + 保留字段
		 String readerAddress = ByteUtil.byteArrToHexString(t.getBody(), 0, 3);  // 读写器地址
         String log = "收到[获取读写器当前地址]返回报文: reader 地址码-"+hostNumber+
                 "读写器地址"+readerAddress
            		+" 时间戳-"+timeStamp;
       logger.info(log);
       df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
	    RedirectOutputStream.put(df.format(new Date())+ log);
		return null;
	}

}
