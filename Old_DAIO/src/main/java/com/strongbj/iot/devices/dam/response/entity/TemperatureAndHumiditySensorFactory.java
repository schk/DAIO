package com.strongbj.iot.devices.dam.response.entity;

import com.strongbj.core.util.ByteUtil;

/**
 * 温湿度传感器工厂
 * @author yuzhantao
 *
 */
public class TemperatureAndHumiditySensorFactory implements ISensorFactory {
	@Override
	public SensorEntity createSensor(byte[] src, int offset) {
		TemperatureAndHumiditySensorEntity sensor = new TemperatureAndHumiditySensorEntity();
		sensor.setSensorType((byte)1);
		sensor.setTemperature((short)((float)ByteUtil.byteArrToShort(src[offset+2],src[offset+1])/10.0f));
		sensor.setHumidity((short)((float)ByteUtil.byteArrToShort(src[offset+4],src[offset+3])/10.0f));
		
		return sensor;
	}

}
