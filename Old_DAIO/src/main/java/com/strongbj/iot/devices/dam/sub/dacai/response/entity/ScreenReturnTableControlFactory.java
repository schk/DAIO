package com.strongbj.iot.devices.dam.sub.dacai.response.entity;

public class ScreenReturnTableControlFactory {
	public static ScreenReturnTableControlEntity create(String msg) {
		ScreenReturnTableControlEntity sr = new ScreenReturnTableControlEntity();
		sr.setScreenId(Integer.valueOf(msg.substring(0,4),16).shortValue());
		sr.setControlId(Integer.valueOf(msg.substring(4,8),16).shortValue());
		sr.setControlType(Integer.valueOf(msg.substring(8,10),16).byteValue());
		sr.setSelectIndex(Short.valueOf(msg.substring(10,14),16));
		return sr;
	}
}
