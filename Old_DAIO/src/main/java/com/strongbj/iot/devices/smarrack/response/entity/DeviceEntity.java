package com.strongbj.iot.devices.smarrack.response.entity;
/**
 * 设备实体
 * @author yuzhantao
 *
 */
public class DeviceEntity {
	private String ip;
	private String deviceCode;
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getDeviceCode() {
		return deviceCode;
	}
	public void setDeviceCode(String deviceCode) {
		this.deviceCode = deviceCode;
	}
	
	
}
