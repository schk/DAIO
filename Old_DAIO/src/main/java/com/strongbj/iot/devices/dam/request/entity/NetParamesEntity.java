package com.strongbj.iot.devices.dam.request.entity;

public class NetParamesEntity extends RackCodeEntity {
	/**
	 * IP地址
	 */
	private String ip;
	/**
	 * 子网掩码
	 */
	private String mask;
	/**
	 * 网关
	 */
	private String gateway;
	
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getMask() {
		return mask;
	}
	public void setMask(String mask) {
		this.mask = mask;
	}
	public String getGateway() {
		return gateway;
	}
	public void setGateway(String gateway) {
		this.gateway = gateway;
	}
}
