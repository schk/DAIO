package com.strongbj.iot.devices.amazonNewReader.message;

public class NewReaderMessage {


	// 地址
	private byte[] address;

	// 类型
	private short type;

	// 数据
	private byte[] body;
	
	//时间戳
	private byte[] time;

	public NewReaderMessage(byte[] address,short type,byte[] body,byte[] time) { 
		this.address = address;
		this.type = type;
		this.body = body;
		this.time = time;
	}

	public byte[] getAddress() {
		return address;
	}

	public void setAddress(byte[] address) {
		this.address = address;
	}

	public short getType() {
		return type;
	}

	public void setType(short type) {
		this.type = type;
	}

	public byte[] getBody() {
		return body;
	}

	public void setBody(byte[] body) {
		this.body = body;
	}

	public byte[] getTime() {
		return time;
	}

	public void setTime(byte[] time) {
		this.time = time;
	}

}
