package com.strongbj.iot.devices.dam.request.entity;
/**
 * 设置新机柜编号
 * @author yuzhantao
 *
 */
public class DeviceCodeEntity extends RackCodeEntity {
	private String newDeviceCode;

	public String getNewDeviceCode() {
		return newDeviceCode;
	}

	public void setNewDeviceCode(String newDeviceCode) {
		this.newDeviceCode = newDeviceCode;
	}
	
}
