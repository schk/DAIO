package com.strongbj.iot.devices.amazondb.response.entity;
/**
 * Led型标签按键的状态
 * @author yuzhantao
 *
 */
public enum LedLabelKeyState {
	/**
	 * 按键按下
	 */
	KeyDown,
	/**
	 * 按键抬起
	 */
	KeyUp
}
