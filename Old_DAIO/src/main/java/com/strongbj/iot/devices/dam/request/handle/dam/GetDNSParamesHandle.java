package com.strongbj.iot.devices.dam.request.handle.dam;

/**
 * 查看DNS
 * 
 * @author yuzhantao
 *
 */
public class GetDNSParamesHandle extends GetParamsHandle {
	public GetDNSParamesHandle() {
		super("getDNSParames", (byte) 0x14);
	}
}
