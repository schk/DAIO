package com.strongbj.iot.devices.amazondb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.strongbj.core.util.PropertiesUtil;
import com.strongbj.iot.devices.amazondb.entity.Tag;

@Service("mongoDBService")
public class MongoDBService {

//	@Autowired
	private MongoTemplate mongoTemplate;

	
	@SuppressWarnings("deprecation")
	public MongoDBService() {
		String host = PropertiesUtil.getString("mongo.host");
		int port = PropertiesUtil.getInt("mongo.port");
		String dbname = PropertiesUtil.getString("mongo.dbname");
		try {
//			Mongo mongo = new Mongo(host,port);
			MongoClient mongo = new MongoClient(host, port);
			mongoTemplate = new MongoTemplate(mongo,dbname);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public List<Tag> find(Query query) {
		return mongoTemplate.find(query, Tag.class);
//        return new ArrayList<Tag>();
	}

	public void save(Tag tag) {
		mongoTemplate.save(tag);
	}

	public void remove(Tag tag) {
		mongoTemplate.remove(tag);
	}

	public Tag findById(String id) {
		return mongoTemplate.findById(id, Tag.class);
	}

}
