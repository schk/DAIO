package com.strongbj.iot.devices.guis.message;

import io.netty.channel.ChannelHandler;

import com.strongbj.core.message.IMessageHandleFactory;
import com.strongbj.iot.devices.guis.respnose.coder.GUISDecoder;
import com.strongbj.iot.devices.guis.respnose.handle.GUISResponseHandleContext;

public class GUISMessageHandleFactory implements IMessageHandleFactory {

	@Override
	public ChannelHandler createMessageDecoder() {
		return new GUISDecoder(Integer.MAX_VALUE, 6, 2, 2, 0, true);
	}

	@Override
	public ChannelHandler createMessageHandle() {
		return new GUISResponseHandleContext();
	}

}
