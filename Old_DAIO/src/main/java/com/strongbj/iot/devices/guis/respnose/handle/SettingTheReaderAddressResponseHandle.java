package com.strongbj.iot.devices.guis.respnose.handle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.amazonNewReader.message.NewReaderMessage;

import io.netty.channel.ChannelHandlerContext;
/**
 * 设置读写器地址  handle
 * @author 25969
 *
 */
public class SettingTheReaderAddressResponseHandle implements IMessageHandle<NewReaderMessage,Object>{
	private static Logger logger = LogManager.getLogger(SettingTheReaderAddressResponseHandle.class.getName());
//	private static SimpleDateFormat df=null;
	@Override
	public boolean isHandle(NewReaderMessage t) {
		if(Integer.valueOf("10", 16).byteValue()==t.getType()){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, NewReaderMessage t) {

		String hostNumber =  ByteUtil.byteArrToHexString(t.getAddress());   //reader 地址码
		 String timeStamp  =  ByteUtil.byteArrToHexString(t.getTime());   //时间戳
		 //1 字节地址修改结果 + 3 字节修改后地址 + 保留字段。    
		 int result  = ByteUtil.bytesToUbyte(t.getBody(), 0); //修改结果
		 String readerAddress = ByteUtil.byteArrToHexString(t.getBody(), 1, 3);  // 修改后地址
        
        logger.info("收到[设置读写器地址]返回报文: reader 地址码-"+hostNumber+"  修改结果-"+result +
                "修改后地址"+readerAddress
        		+" 时间戳-"+timeStamp);
		return null;
	
	}

}
