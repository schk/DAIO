package com.strongbj.iot.devices.amazondb.request.message;

import com.alibaba.fastjson.annotation.JSONField;

public class MQBodyMessage<T>{
	private String actioncode;
	private T postdata;
	
	@JSONField(name="GUID")
	private String GUID;
	public String getActioncode() {
		return actioncode;
	}
	public void setActioncode(String actioncode) {
		this.actioncode = actioncode;
	}
	public T getPostdata() {
		return postdata;
	}
	public void setPostdata(T postdata) {
		this.postdata = postdata;
	}
	public String getGUID() {
		return GUID;
	}
	public void setGUID(String gUID) {
		GUID = gUID;
	}
	
}
