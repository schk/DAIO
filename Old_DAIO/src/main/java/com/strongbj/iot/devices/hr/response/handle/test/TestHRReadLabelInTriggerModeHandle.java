package com.strongbj.iot.devices.hr.response.handle.test;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.netty.channel.ChannelHandlerContext;

import com.alibaba.fastjson.JSON;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.core.util.ContextUtils;
import com.strongbj.iot.devices.hr.message.HRMessage;
import com.strongbj.iot.devices.hr.message.MQMessageOfHR;
import com.strongbj.iot.devices.hr.response.entity.ScanLabInfoOfDoor;
import com.strongbj.iot.mq.producer.TopicSender;

/**
 * 在触发模式下读取标签的处理
 * @author yuzhantao
 *
 */
public class TestHRReadLabelInTriggerModeHandle  implements IMessageHandle<HRMessage,Object>{
	private static Logger logger = LogManager.getLogger(TestHRReadLabelInTriggerModeHandle.class.getName());
	private TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");
	private static final String ACTION_CODE = "collect020";
	private Map<String,List<String>> labs=new HashMap<>();		// 存取在结束扫描前保存的标签数组
	
	@Override
	public boolean isHandle(HRMessage t) {
		if(t.getCommand()==0x39){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, HRMessage t) {
		if(t.getData().length>3){
			// 数据长度大于3位即读取标签数据
			int validDataLen = t.getData()[0];				// 有效数据长度
			byte rSSI = t.getData()[1];						// RSSI值
			byte antennaNum = t.getData()[validDataLen];	// 读取天线号
			String labCode = ByteUtil.byteArrToHexString(t.getData(), 2, validDataLen-3);  // 标签编号
			this.labHandle(ctx, rSSI, antennaNum, labCode); // 标签处理
		}else if(t.getData().length==2){
			// 数据长度等于2表示开始寻卡
			byte triggerInfraredNum = t.getData()[0];		// 触发的红外引脚编号
			this.startAntennaScanHandle(ctx, triggerInfraredNum);
		}else if(t.getData().length==3){
			byte antennaNum = t.getData()[2];		// 读取天线号
			if(t.getData()[0]==0xFF && t.getData()[1]==0xFF){
				// 当所有天线工作结束返回结束标记数据 0xFFFF 和结束的天线号
				this.endAllAntennaScanHandle(ctx, antennaNum);
			}else{
				// 当前天线工作结束时主动返回当前天线号和读到的标签记录条数
				int labCount = t.getData()[0]+t.getData()[1]*256;   // 记录条数= Data[0]+ Data[1]*256
				this.endCurrentAntennaScanHandle(ctx, antennaNum, labCount);
			}
		}
		return null;
	}

	/**
	 * 标签的处理
	 * @param ctx
	 * @param rSSI			RSSI值
	 * @param antennaNum	天线号
	 * @param labCode		标签编码
	 */
	private void labHandle(ChannelHandlerContext ctx, byte rSSI,byte antennaNum,String labCode){
		logger.info("HR设备[ip:{}] 读取标签，标签号={},		天线号={}		RSSI={}",
				ctx.channel().remoteAddress().toString(),
				labCode,
				antennaNum,
				rSSI);
		List<String> labList = labs.get(ctx.channel().remoteAddress().toString());
		if(labList==null){
			labList = new ArrayList<>();
			labs.put(ctx.channel().remoteAddress().toString(), labList);
		}
		if(!labList.contains(labCode)) labList.add(labCode);
		
		MQMessageOfHR msg = new MQMessageOfHR();
		msg.setActioncode(ACTION_CODE);
		ScanLabInfoOfDoor slid = new ScanLabInfoOfDoor();
		InetSocketAddress insocket = (InetSocketAddress) ctx.channel()
				.remoteAddress();
		slid.setDevIp(insocket.getAddress().getHostAddress());
		slid.setDirection("I");
		List<String> list = new ArrayList<String>();
		list.add(labCode);
		slid.setRfids(list);
		msg.setAwsPostdata(slid);
		String json = JSON.toJSONString(msg);
		logger.info("HR设备 向MQ发送标签数据	 JSON={}",json);
		topicSender.send("hrReader", json);
	}
	
	/**
	 * 开始寻卡的处理
	 * @param ctx
	 * @param triggerInfraredNum	触发的红外编号
	 */
	private void startAntennaScanHandle(ChannelHandlerContext ctx, byte triggerInfraredNum){
		logger.info("HR设备[ip:{}] 触发了第{}条红外",ctx.channel().remoteAddress(),triggerInfraredNum);
		labs.put(ctx.channel().remoteAddress().toString(), new ArrayList<>());
	}
	
	/**
	 * 结束所有天线的寻卡处理
	 * @param ctx
	 * @param antennaNum	天线编号
	 */
	private void endAllAntennaScanHandle(ChannelHandlerContext ctx, byte antennaNum){
		logger.info("所有天线工作已结束，天线号={}",antennaNum);
	}
	
	/**
	 * 结束当前天线的寻卡处理
	 * @param ctx
	 * @param antennaNum	天线编号
	 * @param labCount		扫到的标签数量
	 */
	private void endCurrentAntennaScanHandle(ChannelHandlerContext ctx, byte antennaNum,int labCount){
		logger.info("当前天线工作已结束，天线号={},	标签记录条数={}",antennaNum,labCount);
	}
}

