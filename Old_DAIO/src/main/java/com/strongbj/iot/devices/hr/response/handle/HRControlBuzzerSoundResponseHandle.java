package com.strongbj.iot.devices.hr.response.handle;

import java.net.InetSocketAddress;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ContextUtils;
import com.strongbj.iot.devices.hr.message.HRMessage;
import com.strongbj.iot.devices.hr.message.MQMessageOfHR;
import com.strongbj.iot.devices.hr.response.entity.HRControlBuzzerSoundResponseEntity;
import com.strongbj.iot.mq.producer.TopicSender;

import io.netty.channel.ChannelHandlerContext;

/**
 * 控制蜂鸣器发送反馈处理类
 * 
 * @author Pingfan
 *
 */
public class HRControlBuzzerSoundResponseHandle implements IMessageHandle<HRMessage, Object> {
	protected static Logger logger = LogManager.getLogger(HRResetResponseHandle.class.getName());
	protected TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");
	protected static final String ACTION_CODE = "collect015";
	
	public HRControlBuzzerSoundResponseHandle(String doorDirection) {
		super();
	}
	
	@Override
	public boolean isHandle(HRMessage t) {
		if (t.getCommand() == 0x0F) { // 控制蜂鸣器发声反馈命令类型为0x0F
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, HRMessage t) {
		// 取门禁ip
		InetSocketAddress insocket = (InetSocketAddress) ctx.channel().remoteAddress();
		String ip = insocket.getAddress().getHostAddress();
		
		// 发送重启反馈到MQ, 资产监听
		MQMessageOfHR msg = new MQMessageOfHR();
		msg.setActioncode(ACTION_CODE);
		HRControlBuzzerSoundResponseEntity controlBuzzerSoundRsp = new HRControlBuzzerSoundResponseEntity();
		controlBuzzerSoundRsp.setDevIp(ip);
		controlBuzzerSoundRsp.setCommand(t.getCommand());
		if(t.getData()[0] == (byte)0x00) { // data段0x00表示重启成功, 0x80表示失败
			controlBuzzerSoundRsp.setControlBuzzerSoundSuccess(true);
		} else {
			controlBuzzerSoundRsp.setControlBuzzerSoundSuccess(false);
		}
		msg.setAwsPostdata(controlBuzzerSoundRsp);
		String json = JSON.toJSONString(msg);
		topicSender.send("hrReader", json);
		logger.info("HR设备发送【控制蜂鸣器发声反馈】成功! ip={}, topic=hrReader, 数据={}", ip, json);
		return null;
	}
}
