package com.strongbj.iot.devices.dam.request.entity;

public class ScreenCommandEntity extends RackCodeEntity {	
	/**
	 * 十六进制字符命令
	 */
	private String command;

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}
}
