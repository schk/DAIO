package com.strongbj.iot.devices.guis.respnose.handle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.strongbj.core.message.MessageHandleContext;
import com.strongbj.iot.devices.dam.common.NetMapping;
import com.strongbj.iot.devices.guis.message.GUISMessage;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

public class GUISResponseHandleContext  extends SimpleChannelInboundHandler<GUISMessage>{
	private MessageHandleContext<GUISMessage,Object> messageHandleContent;
	private static Logger logger = LogManager.getLogger(GUISResponseHandleContext.class.getName());
	public static final ChannelGroup channels = new DefaultChannelGroup("GUISChannelGroup",GlobalEventExecutor.INSTANCE);
//	private static SimpleDateFormat df=null;
	public GUISResponseHandleContext(){
		super();
		
		this.messageHandleContent = new MessageHandleContext<>();
		this.messageHandleContent.setOnlyHandle(false);
		this.messageHandleContent.addHandleClass(new OnlineHandle());              	// 上线信息
		this.messageHandleContent.addHandleClass(new OfflineHandle());              // 离线信息
		this.messageHandleContent.addHandleClass(new TransmissionHandle());              // 数据透传
	    this.messageHandleContent.addHandleClass(new DacaiHandleContext());				// 监听大彩的命令
	    this.messageHandleContent.addHandleClass(new GUISTimeUploadUScanLaterHandle());  // 处理下位机定时上传U位扫描后的信息的类
		this.messageHandleContent.addHandleClass(new GUISUScanChangeUploadULocationMegHandle());  // 处理下位机在U位信息有变化时主动上传U位扫描信息的类
		this.messageHandleContent.addHandleClass(new GUISSendHeartPackage());  // 下位机发送心跳包
		this.messageHandleContent.addHandleClass(new GUISControlOpenLedTwinkleHandle());// 解析“上位机控制打开LED灯闪烁” 下位机的回复报文
	    this.messageHandleContent.addHandleClass(new GUISGetHostBasicInfoHandle());     //解析“上位机获取主机基本信息”回复报文
	    this.messageHandleContent.addHandleClass(new GUISGetHostNetworkInfoHandle());   //解析"上位机获取主机网络配置信息"回复报文
	    this.messageHandleContent.addHandleClass(new GUISStartReadCardHandle());        //解析"上位机启动读卡"
//	    this.messageHandleContent.addHandleClass(new GUISTemperatureHandle());              // 温度信息
	    this.messageHandleContent.addHandleClass(new GUISTemperatureV2Handle());              // 温度信息
//	    this.messageHandleContent.addHandleClass(new GUISTemperaturePlusHandle());              // 温度信息
	    
	}
	
	@Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        logger.info("GUIS设备已连接"+ctx.channel().remoteAddress().toString());
        
        try {
			NetMapping.getInstance().addChannel(ctx.channel());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}
        
        try {
        GUISMessage msg = new GUISMessage();
		msg.setCommand(Integer.valueOf(99).byteValue());
		this.messageHandleContent.handle(ctx, msg);
        }catch(Exception e) {
        	logger.error("处理在线信息异常。", e);
        }
        channels.add(ctx.channel());
		super.channelActive(ctx);
    }

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		logger.info("GUIS设备已断开"+ctx.channel().remoteAddress().toString());
		
		try {
			NetMapping.getInstance().removeChannel(ctx.channel());
		} catch (Exception e) {
			logger.error("", e);
		}
		
		try {
			GUISMessage msg = new GUISMessage();
			msg.setCommand(Integer.valueOf(-99).byteValue());
			this.messageHandleContent.handle(ctx, msg);
		}catch(Exception e) {
			logger.error("处理离线信息异常。", e);
		}
		
		channels.remove(ctx.channel());
		this.messageHandleContent.clearHandleClass();
		super.channelInactive(ctx);
	}
	
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, GUISMessage msg)
			throws Exception {
		if(msg==null) return;
		this.messageHandleContent.handle(ctx,msg);
	}
}
