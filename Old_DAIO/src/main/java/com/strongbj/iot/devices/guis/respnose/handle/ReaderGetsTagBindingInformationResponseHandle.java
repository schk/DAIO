package com.strongbj.iot.devices.guis.respnose.handle;

import com.strongbj.core.message.IMessageHandle;
import com.strongbj.iot.devices.amazonNewReader.message.NewReaderMessage;

import io.netty.channel.ChannelHandlerContext;

/**
 * 读写器获取标签绑定信息   handle
 * @author 25969
 *
 */
public class ReaderGetsTagBindingInformationResponseHandle implements IMessageHandle<NewReaderMessage,Object>{
//	private static Logger logger = LogManager.getLogger(ReaderGetsTagBindingInformationResponseHandle.class.getName());
//	private static SimpleDateFormat df=null;
	@Override
	public boolean isHandle(NewReaderMessage t) {
		if(Integer.valueOf("0B", 16).byteValue()==t.getType()){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, NewReaderMessage t) {
		// TODO Auto-generated method stub
		return null;
	}

}
