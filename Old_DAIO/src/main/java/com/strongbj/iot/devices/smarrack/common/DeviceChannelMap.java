package com.strongbj.iot.devices.smarrack.common;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.channel.Channel;

public class DeviceChannelMap {
	private final static Logger logger = LoggerFactory.getLogger(DeviceChannelMap.class);
	private Map<String,Channel> map = new ConcurrentHashMap<>();

	private static DeviceChannelMap instance;
	
	public static DeviceChannelMap getInstance() {
		if(instance==null) {
			instance = new DeviceChannelMap();
		}
		return instance;
	}
	
	private DeviceChannelMap() {}
	
	public void put(String deviceCode ,Channel channel) {
		this.map.put(deviceCode, channel);
		logger.debug("记录(put)数码人设备到设备管理类,deviceCode="+deviceCode);
	}
	
	public Channel getChannel (String deviceCode) {
		Channel channel = this.map.get(deviceCode);
		logger.debug("获取(get)数码人设备到设备管理类,deviceCode="+deviceCode+"Channel="+channel==null?"null":channel.toString());
		return channel;
	}
}
