package com.strongbj.iot.devices.amazonNewReader.response.handle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.strongbj.core.message.MessageHandleContext;
import com.strongbj.iot.devices.amazonNewReader.message.NewReaderMessage;
import com.strongbj.iot.devices.guis.respnose.handle.ReaderGetsTagBindingInformationResponseHandle;
import com.strongbj.iot.devices.guis.respnose.handle.SettingTheReaderAddressResponseHandle;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

public class NewReaderResponseHandleContext extends SimpleChannelInboundHandler<NewReaderMessage>{
	private MessageHandleContext<NewReaderMessage,Object> messageHandleContent;
	private static Logger logger = LogManager.getLogger(NewReaderResponseHandleContext.class.getName());
	public static final ChannelGroup channels = new DefaultChannelGroup("NewReaderChannelGroup",GlobalEventExecutor.INSTANCE);

	public NewReaderResponseHandleContext() {
		super();
		this.messageHandleContent = new MessageHandleContext<>();
	    this.messageHandleContent.addHandleClass(new LabelBindingRequestResponseHandle());
	    this.messageHandleContent.addHandleClass(new LabelBindingResponseHandle());
	    this.messageHandleContent.addHandleClass(new LabelRebindingResponseHandle());
	    this.messageHandleContent.addHandleClass(new LabelUntyingResponseHandle());
	    this.messageHandleContent.addHandleClass(new LedControlForSingleResponseHandle());
	    this.messageHandleContent.addHandleClass(new LedControlForManyResponseHandle());
	    this.messageHandleContent.addHandleClass(new LedInventoryForSingleResponseHandle());
	    this.messageHandleContent.addHandleClass(new LedInventoryForManyResponseHandle());
	    this.messageHandleContent.addHandleClass(new TagElectricityAlarmResponseHandle());
	    this.messageHandleContent.addHandleClass(new LabelKeyTriggerAlarmResponseHandle());
	    this.messageHandleContent.addHandleClass(new ReaderGetsTagBindingInformationResponseHandle());
	    this.messageHandleContent.addHandleClass(new SettingTheReaderAddressResponseHandle());
	    this.messageHandleContent.addHandleClass(new gettingTheReaderAddressResponseHandle());
	    this.messageHandleContent.addHandleClass(new SettingUpReaderNetworkParametersResponseHandle());
	    this.messageHandleContent.addHandleClass(new gettingUpReaderNetworkParametersResponseHandle());
	    this.messageHandleContent.addHandleClass(new ReaderHeartbeatResponseHandle());
	    this.messageHandleContent.addHandleClass(new SettingLabelParametersResponseHandle());
	    this.messageHandleContent.addHandleClass(new gettingLabelParametersResponseHandle());
	    //读写器获取标签绑定信息 
	    this.messageHandleContent.addHandleClass(new readerGetsTagBindingInformationHandle());
	}
	
	@Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        channels.add(ctx.channel());
        logger.info("AWS 新版reader  已连接"+ctx.channel().remoteAddress().toString());
		super.channelActive(ctx);
    }
	
	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		logger.info("AWS 新版reader 已断开"+ctx.channel().remoteAddress().toString());
		super.channelInactive(ctx);
	}
	
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, NewReaderMessage msg) throws Exception {
		if(msg==null) return;
		// logger.info("收到Reader数据信息，数据类型："+msg.getType()+"	设备编号："+msg.getAddress()+"		数据位："+ByteUtil.byteArrToHexString(msg.getBody()));
		this.messageHandleContent.handle(ctx,msg);
	}

}
