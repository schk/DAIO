package com.strongbj.iot.devices.amazonNewReader.request.handle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.amazonNewReader.message.AmazonNewReaderOSMessageFactory;
import com.strongbj.iot.devices.amazonNewReader.message.MQMessageOfAmazonNewReader;
import com.strongbj.iot.devices.amazonNewReader.request.entity.AmazonNewReaderCommenEntity;
import com.strongbj.iot.devices.amazonNewReader.request.entity.T;
import com.strongbj.iot.devices.amazonNewReader.response.handle.NewReaderResponseHandleContext;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;

/**
 * 设置读写器网络参数
 * @author 25969
 *
 */
public class SettingUpReaderNetworkParametersHandle implements IMessageHandle<MQMessageOfAmazonNewReader,Object>{
	private static Logger logger = LogManager.getLogger(SettingUpReaderNetworkParametersHandle.class.getName());
	private final static String ACTION_CODE = "settingUpReaderNetworkParameters";   //MQ中判断消息类型的标识符
	private final static byte command = (byte)0x12;
	private AmazonNewReaderOSMessageFactory amazonNewReaderOSMessageFactory = new AmazonNewReaderOSMessageFactory();
//	private static SimpleDateFormat df=null;
	
	@Override
	public boolean isHandle(MQMessageOfAmazonNewReader t) {
		if(t.getActioncode().equals(ACTION_CODE)){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, MQMessageOfAmazonNewReader t) {
		logger.info("AmazonNewReader 收到MQ发来的“设置读写器网络参数” json={}",JSON.toJSONString(t));
		AmazonNewReaderCommenEntity amazonNewReaderCommen = ((JSONObject)t.getAwsPostdata()).toJavaObject(AmazonNewReaderCommenEntity.class);		
		 this.messageDataToBytes(amazonNewReaderCommen);  
		return null;
	}

	private void messageDataToBytes(AmazonNewReaderCommenEntity yy) {
	    String nativeIPAddress = yy.getNativeIPAddress();   //本机IP地址
		String subnetMask = yy.getSubnetMask();            //子网掩码
	    String gateway = yy.getGateway();                  //网关
	    String targetIaddressIp = yy.getTargetIaddressIp();//目标IP
	    int targetPort = yy.getTargetPort();            //目标端口
		String readerCode = yy.getReaderCode();  //reader地址码
		int hour = yy.getHour();
		int minute = yy.getMinute();
		int second = yy.getSecond();
		int millisecond = yy.getMillisecond();
		byte[] sendData = new byte[18];   
		byte[] dest = new byte[3];   //接受主机编号的临时byte数组
		  //4byte(本机IP地址)+4byte(掩码)+4byte(网关)+4byte(dns)+2byte(本机端口号)+4byte（目标IP地址）+2byte（目标端口号）
	    byte [] pTmp=ByteUtil.ipToBytesByInet(nativeIPAddress);
	    System.arraycopy(pTmp, 0, sendData, 0, pTmp.length);
	    pTmp=ByteUtil.ipToBytesByInet(subnetMask);
	    System.arraycopy(pTmp, 0, sendData, 4, pTmp.length);
	    pTmp=ByteUtil.ipToBytesByInet(gateway);
	    System.arraycopy(pTmp, 0, sendData, 8, pTmp.length);
	    pTmp=ByteUtil.ipToBytesByInet(targetIaddressIp);
	    System.arraycopy(pTmp, 0, sendData, 12, pTmp.length);
	    ByteUtil.shortToBytes((short)targetPort, sendData,16);
		byte[] timeStamp = new byte[4];
		StringBuffer time = new StringBuffer();
		String hourByte = T.getHourByte(hour);
		String minuteByte = T.getMinuteByte(minute);
		String secondByte = T.getSecondByte(second);
		String millisecondByte = T.getMillisecond(millisecond);	
		time.append(hourByte).append(minuteByte).append(secondByte).append(millisecondByte).toString();
		timeStamp[0] = Integer.valueOf(time.toString().substring(0, 8),2).byteValue();
		timeStamp[1] = Integer.valueOf(time.toString().substring(8, 16),2).byteValue();
		timeStamp[2] = Integer.valueOf(time.toString().substring(16, 24),2).byteValue();
		timeStamp[3] = Integer.valueOf(time.toString().substring(32-8),2).byteValue();
		byte[] datas = amazonNewReaderOSMessageFactory.createAmazonNewReaderOSMessage(ByteUtil.hexStringToBytes(readerCode,dest,0), command, sendData,timeStamp);
		ByteBuf bs  = Unpooled.copiedBuffer(datas);
		NewReaderResponseHandleContext.channels.writeAndFlush(bs);
		logger.info("AmazonNewReader 发送[设置读写器网络参数]命令成功 message={}",ByteUtil.byteArrToHexString(datas));
	}
}
