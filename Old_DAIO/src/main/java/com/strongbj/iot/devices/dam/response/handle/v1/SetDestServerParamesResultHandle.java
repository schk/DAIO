package com.strongbj.iot.devices.dam.response.handle.v1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.core.util.ContextUtils;
import com.strongbj.iot.devices.dam.message.DAMMessage;
import com.strongbj.iot.devices.dam.message.MQMessageOfDAM;
import com.strongbj.iot.devices.dam.response.entity.DefaultResultEntity;
import com.strongbj.iot.mq.producer.TopicSender;

import io.netty.channel.ChannelHandlerContext;

public class SetDestServerParamesResultHandle extends DAMV1Handle {
	private static Logger logger = LogManager.getLogger(SetDestServerParamesResultHandle.class.getName());
	private TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");
	private static final String TOPIC="damCommandToService";
	private static final String ACTION_CODE = "resultSetDestServerParamesResultHandle";
	private static final String DEV_TYPE="damDc";
	
	@Override
	protected boolean isHandleDAMMessage(DAMMessage t) {
		if(t.getCommand()==0x12){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, DAMMessage t) {
		boolean isSetSuccess = t.getDatas()[0]==1?true:false;
		MQMessageOfDAM msg = new MQMessageOfDAM();
		msg.setActioncode(ACTION_CODE);
		
		DefaultResultEntity result=new DefaultResultEntity();
		result.setSuccess(isSetSuccess);
		msg.setAwsPostdata(result);
		msg.setDevType(DEV_TYPE);
		msg.setTimestamp(ByteUtil.bytesToLong(t.getSn()));
		String json = JSON.toJSONString(msg);
		logger.info("DAM设备 向MQ发送设置目标服务器网络参数的返回信息	 JSON={}",json);
		topicSender.send(TOPIC, json);
		return null;
	}
}

