package com.strongbj.iot.devices.amazonNewReader.response.handle;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.amazonNewReader.message.NewReaderMessage;
import com.strongbj.iot.devices.guis.respnose.common.RedirectOutputStream;

import io.netty.channel.ChannelHandlerContext;
/**
 * 读写器心跳 
 * @author 25969
 *
 */
public class ReaderHeartbeatResponseHandle implements IMessageHandle<NewReaderMessage,Object>{
	private static Logger logger = LogManager.getLogger(ReaderHeartbeatResponseHandle.class.getName());
	private static SimpleDateFormat df=null;
	@Override
	public boolean isHandle(NewReaderMessage t) {
		if(Integer.valueOf("14", 16).byteValue()==t.getType()){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, NewReaderMessage t) {
		String hostNumber =  ByteUtil.byteArrToHexString(t.getAddress());   //reader 地址码
		 String timeStamp  =  ByteUtil.byteArrToHexString(t.getTime());   //时间戳
		  logger.info("收到[ 读写器心跳 ]返回报文: reader 地址码-"+hostNumber+
	      		"时间戳-"+timeStamp);
		  df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		   RedirectOutputStream.put(df.format(new Date())+ "收到[ 读写器心跳 ]返回报文: reader 地址码-"+hostNumber+
		      		" /时间戳-"+timeStamp);
		return null;
	}

}
