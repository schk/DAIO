package com.strongbj.iot.devices.dam.request.entity;

public class AssetInfoOfBindRFIDEntity {
	private static final long serialVersionUID = 1L;
	private Long id;
	private int startU;
	private int endU;
	private String assetNo;
	private String assetName;
	private int assetStatus;
	private String rackConverCode;
	private String assetModelName;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getStartU() {
		return startU;
	}
	public void setStartU(int startU) {
		this.startU = startU;
	}
	public int getEndU() {
		return endU;
	}
	public void setEndU(int endU) {
		this.endU = endU;
	}
	public String getAssetNo() {
		return assetNo;
	}
	public void setAssetNo(String assetNo) {
		this.assetNo = assetNo;
	}
	public String getAssetName() {
		return assetName;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	public int getAssetStatus() {
		return assetStatus;
	}
	public void setAssetStatus(int assetStatus) {
		this.assetStatus = assetStatus;
	}
	public String getRackConverCode() {
		return rackConverCode;
	}
	public void setRackConverCode(String rackConverCode) {
		this.rackConverCode = rackConverCode;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getAssetModelName() {
		return assetModelName;
	}
	public void setAssetModelName(String assetModelName) {
		this.assetModelName = assetModelName;
	}
	
	
}
