package com.strongbj.iot.devices.amazondb.message;

import com.strongbj.core.message.IMessageHandleFactory;
import com.strongbj.iot.devices.amazondb.response.coder.ReaderDecoder;
import com.strongbj.iot.devices.amazondb.response.handle.ReaderResponseHandleContext;

import io.netty.channel.ChannelHandler;

/**
 * 从点亮标签设备接收到的消息工厂
 * @author yuzhantao
 *
 */
public class ReaderMessageHandleFactory implements IMessageHandleFactory {

	@Override
	public ChannelHandler createMessageDecoder() {
		return new ReaderDecoder(1024);
	}

	@Override
	public ChannelHandler createMessageHandle() {
		return new ReaderResponseHandleContext();
	}

}
