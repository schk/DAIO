package com.strongbj.iot.devices.dam.request.handle.dam;

/**
 * 查看目标服务器参数
 * 
 * @author yuzhantao
 *
 */
public class GetDestServerParamsHandle extends GetParamsHandle {
	public GetDestServerParamsHandle() {
		super("getDestServerParams", (byte) 0x15);
	}
}
