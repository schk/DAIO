package com.strongbj.iot.devices.dam.request.handle.damtoscreen;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSONObject;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.dam.common.AbstractTaskDelayedRunnable;
import com.strongbj.iot.devices.dam.common.DAMManager;
import com.strongbj.iot.devices.dam.common.ITaskManager;
import com.strongbj.iot.devices.dam.common.NetMapping;
import com.strongbj.iot.devices.dam.message.DAMMessageFactory;
import com.strongbj.iot.devices.dam.message.MQMessageOfDAM;
import com.strongbj.iot.devices.dam.request.entity.ScreenCommandEntity;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;

/**
 * 命令转到屏幕的数据处理
 * 
 * @author yuzhantao
 *
 */
public class CommandToScreenHandle implements IMessageHandle<MQMessageOfDAM, Object> {
	private static Logger logger = LogManager.getLogger(CommandToScreenHandle.class.getName());
	private static final DAMMessageFactory damMessageFactory = new DAMMessageFactory();
	private static final String ACTION_CODE = "screenCommand";

	@Override
	public boolean isHandle(MQMessageOfDAM t) {
		if (t.getActioncode().equals(ACTION_CODE)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 将收到的屏幕命令下发到屏幕硬件
	 */
	@Override
	public Object handle(ChannelHandlerContext ctx, MQMessageOfDAM t) {
		final ScreenCommandEntity sc = ((JSONObject) t.getAwsPostdata()).toJavaObject(ScreenCommandEntity.class);		
		Channel channel = NetMapping.getInstance().getChannel(sc.getRackConverCode());
		ITaskManager taskManager = DAMManager.getTaskManager(sc.getRackConverCode());
		taskManager.addSendTask(new AbstractTaskDelayedRunnable() {
			@Override
			protected void taskRun() {
				sendDatasToDAM(channel, t, sc);
			}
		});

		return null;
	}

	private void sendDatasToDAM(Channel channel, MQMessageOfDAM t, ScreenCommandEntity sc) {
		if (channel != null && channel.isActive()) {
			// 将命令发送到屏幕硬件
			byte[] commandDatas = ByteUtil.hexStringToBytes(sc.getCommand()); // 将十六进制字符转为byte数组
			
			
			
			final byte[] datas = damMessageFactory.createDAMMessage(ByteUtil.longToBytes(t.getTimestamp()),
					sc.getRackConverCode(), (byte) 0, (byte) 0, (byte) 0xFF, commandDatas);
			ByteBuf bs = Unpooled.copiedBuffer(datas);
			ChannelFuture cf = channel.writeAndFlush(bs);
			// TODO 添加监听,查看发送情况DEBUG
			cf.addListener(new ChannelFutureListener() {

				@Override
				public void operationComplete(ChannelFuture future) throws Exception {
					if (future.isSuccess()) {
						logger.info("WEB下发命令到屏幕成功 编号:{} 命令={}  ", sc.getRackConverCode(),
								ByteUtil.byteArrToHexString(datas, true));
					} else {
						logger.error("WEB下发命令到屏幕失败 编号:{} 命令={}  ", sc.getRackConverCode(),
								ByteUtil.byteArrToHexString(datas, true));
					}
				}

			});
		} else {
			logger.error("{} 主机没有找到指定channel", sc.getRackConverCode());
		}
	}
}
