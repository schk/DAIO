package com.strongbj.iot.devices.smarrack.response.handle;

import com.strongbj.iot.devices.smarrack.message.SmarRackMessage;

import io.netty.channel.ChannelHandlerContext;
/**
 * 上传机架信息的处理
 * @author yuzhantao
 *
 */
public class UploadRackInfoHandle extends WithResponseHandle{

	@Override
	public boolean isHandle(SmarRackMessage t) {
		if(t.getCommand()==0xA3){
			return true;
		}else{
			return false;
		}
	}

	@Override
	void messageHandle(ChannelHandlerContext ctx, SmarRackMessage t) {
		// TODO Auto-generated method stub
		
	}
}
