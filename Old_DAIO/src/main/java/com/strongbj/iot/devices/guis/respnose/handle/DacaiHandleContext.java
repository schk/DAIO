package com.strongbj.iot.devices.guis.respnose.handle;

import java.util.List;

import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.message.MessageHandleContext;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.dam.sub.dacai.message.DaCaiMessage;
import com.strongbj.iot.devices.dam.sub.dacai.response.IMessageCoder;
import com.strongbj.iot.devices.dam.sub.dacai.response.coder.DaCaiMessageCoder;
import com.strongbj.iot.devices.guis.message.GUISMessage;

import io.netty.channel.ChannelHandlerContext;

/**
 * 大彩数据处理
 * @author yuzhantao
 *
 */
public class DacaiHandleContext implements IMessageHandle<GUISMessage, Object> {
	protected MessageHandleContext<DaCaiMessage, Object> dcMessageHandleContent;
	protected IMessageCoder dcMessageCoder; // 大彩消息编码器
	
	public DacaiHandleContext() {
		this.dcMessageCoder = new DaCaiMessageCoder();
		this.dcMessageHandleContent = new MessageHandleContext<>();
		this.dcMessageHandleContent.addHandleClass(new OpenNetParamsPageHandle());
		this.dcMessageHandleContent.addHandleClass(new SetLocalNetParamsHandle());
	}
	
	@Override
	public boolean isHandle(GUISMessage t) {
		if (Integer.valueOf("32", 16).byteValue() == t.getCommand()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, GUISMessage t) {
		List<DaCaiMessage> list = dcMessageCoder.decode(ByteUtil.byteArrToHexString(t.getData()));
		if (list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				list.get(i).setDamCode(ByteUtil.byteArrToHexString(t.getHostNumber()));
				this.dcMessageHandleContent.handle(ctx, list.get(i));
			}
		}
		return null;
	}

}