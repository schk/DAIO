package com.strongbj.iot.devices.dam.sub.dacai.response.entity;

/**
 *  屏幕返回的信息实体
 * @author yuzhantao
 *
 */
public class ScreenReturnEntity {
	/**
	 * 屏幕Id
	 */
	private short screenId;
	/**
	 * 控件id
	 */
	private short controlId;
	/**
	 * 控件类型
	 */
	private byte controlType;
	/**
	 * 控件子类型
	 */
//	private byte subtype;
	/**
	 * 控件状态
	 */
//	private byte status;
	public short getScreenId() {
		return screenId;
	}
	public void setScreenId(short screenId) {
		this.screenId = screenId;
	}
	public short getControlId() {
		return controlId;
	}
	public void setControlId(short controlId) {
		this.controlId = controlId;
	}
	public byte getControlType() {
		return controlType;
	}
	public void setControlType(byte controlType) {
		this.controlType = controlType;
	}
//	public byte getSubtype() {
//		return subtype;
//	}
//	public void setSubtype(byte subtype) {
//		this.subtype = subtype;
//	}
//	public byte getStatus() {
//		return status;
//	}
//	public void setStatus(byte status) {
//		this.status = status;
//	}
}
