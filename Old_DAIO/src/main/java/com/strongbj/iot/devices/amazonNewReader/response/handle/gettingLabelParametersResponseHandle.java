package com.strongbj.iot.devices.amazonNewReader.response.handle;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.amazonNewReader.message.NewReaderMessage;
import com.strongbj.iot.devices.guis.respnose.common.RedirectOutputStream;

import io.netty.channel.ChannelHandlerContext;

/**
 * 获取标签参数 
 * @author 25969
 *
 */
public class gettingLabelParametersResponseHandle implements IMessageHandle<NewReaderMessage,Object>{
	private static Logger logger = LogManager.getLogger(SettingLabelParametersResponseHandle.class.getName());
	private static SimpleDateFormat df=null;
	@Override
	public boolean isHandle(NewReaderMessage t) {
		if(Integer.valueOf("21", 16).byteValue()==t.getType()){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, NewReaderMessage t) {
		String hostNumber =  ByteUtil.byteArrToHexString(t.getAddress());   //reader 地址码
		 String timeStamp  =  ByteUtil.byteArrToHexString(t.getTime());   //时间戳
		 //4 字节 LED 标签地址 + 1 字节获取结果（0x00，获取失败；0x01，
		 //获取成功，仅 获取成功时才会在后续返回参数值，否则只返回保留字节） + 2 字节 LED 标签休 眠时间（*100MS）+ 1 字节电池阈值 + 保留字段。 
		 String ledTag = ByteUtil.byteArrToHexString(t.getBody(), 0, 4);  // LED 标签地址
		 int result  = ByteUtil.bytesToUbyte(t.getBody(), 4); //LED
		 if(result==1) {
	     String ledSleepTime  = ByteUtil.byteArrToHexString(t.getBody(), 5, 2);  //标签休 眠时间（*100MS
         int electricityThreshold  = ByteUtil.bytesToUbyte(t.getBody(), 6); //电池阈值 
         String log ="收到[获取标签参数  ]返回报文: reader 地址码-"+hostNumber+"  LED 标签地址-"+ledTag +
                   "/标签休 眠时间（*100MS）-"+ledSleepTime+
                   "/电池阈值  -"+electricityThreshold+
          		   "/时间戳-"+timeStamp;
       logger.info(log);
       df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
	   RedirectOutputStream.put(df.format(new Date())+ log);
		 }else {
			 String log ="收到[获取标签参数   获取失败]返回报文: reader 地址码-"+hostNumber+"  LED 标签地址-"+ledTag 
		          		+"时间戳-"+timeStamp;
		    logger.info(log); 
		    df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		    RedirectOutputStream.put(df.format(new Date())+ log);
		 }
		return null;
	}

}
