package com.strongbj.iot.devices.smarrack.message;

import io.netty.channel.ChannelHandler;

import com.strongbj.core.message.IMessageHandleFactory;
import com.strongbj.iot.devices.smarrack.response.coder.SmarRackDecoder;
import com.strongbj.iot.devices.smarrack.response.handle.SmarRackResponseHandleContext;

/**
 * SmarRack消息的工厂类，用来解析和处理消息。
 * @author yuzhantao
 *
 */
public class SmarRackMessageHandleFactory  implements IMessageHandleFactory {

	@Override
	public ChannelHandler createMessageDecoder() {
		return new SmarRackDecoder(Integer.MAX_VALUE, 6, 2, 2, 0, true);
	}

	@Override
	public ChannelHandler createMessageHandle() {
		return new SmarRackResponseHandleContext();
	}

}