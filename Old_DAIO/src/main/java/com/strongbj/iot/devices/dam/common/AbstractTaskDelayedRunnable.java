package com.strongbj.iot.devices.dam.common;
/**
 * 任务延时的Runnable
 * @author yuzhantao
 *
 */
public abstract class AbstractTaskDelayedRunnable implements Runnable {
	/**
	 * 任务执行后等待时间
	 */
	private long taskWaitMillisenconds = 100;
	@Override
	public void run() {
		this.taskRun();
		try {
			Thread.sleep(this.taskWaitMillisenconds);
		} catch (InterruptedException e) {}
	}
	
	public long getTaskWaitMillisenconds() {
		return taskWaitMillisenconds;
	}

	public void setTaskWaitMillisenconds(long taskWaitMillisenconds) {
		this.taskWaitMillisenconds = taskWaitMillisenconds;
	}

	protected abstract void taskRun();
}
