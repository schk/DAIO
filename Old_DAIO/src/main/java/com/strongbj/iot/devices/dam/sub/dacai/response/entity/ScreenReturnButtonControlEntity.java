package com.strongbj.iot.devices.dam.sub.dacai.response.entity;

/**
 * 屏幕返回到按钮信息
 * @author yuzhantao
 *
 */
public class ScreenReturnButtonControlEntity {
	/**
	 * 屏幕Id
	 */
	private short screenId;
	/**
	 * 控件id
	 */
	private short controlId;
	/**
	 * 控件类型
	 */
	private byte controlType;
	/**
	 * 按钮状态
	 */
	private ButtonState buttonState;
	/**
	 * 控件子类型
	 */
	private byte subType;
	
	public short getScreenId() {
		return screenId;
	}
	public void setScreenId(short screenId) {
		this.screenId = screenId;
	}
	public short getControlId() {
		return controlId;
	}
	public void setControlId(short controlId) {
		this.controlId = controlId;
	}
	public byte getControlType() {
		return controlType;
	}
	public void setControlType(byte controlType) {
		this.controlType = controlType;
	}
	public ButtonState getButtonState() {
		return buttonState;
	}
	public void setButtonState(ButtonState buttonState) {
		this.buttonState = buttonState;
	}
	public byte getSubType() {
		return subType;
	}
	public void setSubType(byte subType) {
		this.subType = subType;
	}
	
}
