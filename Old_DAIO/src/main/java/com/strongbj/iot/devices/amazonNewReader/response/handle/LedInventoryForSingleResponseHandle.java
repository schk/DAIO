package com.strongbj.iot.devices.amazonNewReader.response.handle;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.amazonNewReader.message.NewReaderMessage;
import com.strongbj.iot.devices.amazonNewReader.request.entity.T;
import com.strongbj.iot.devices.guis.respnose.common.RedirectOutputStream;

import io.netty.channel.ChannelHandlerContext;
/**
 * 标签盘点，针对单独标签
 * @author 25969
 *
 */
public class LedInventoryForSingleResponseHandle  implements IMessageHandle<NewReaderMessage,Object>{
	private static Logger logger = LogManager.getLogger(LedInventoryForSingleResponseHandle.class.getName());
	private static SimpleDateFormat df=null;
	@Override
	public boolean isHandle(NewReaderMessage t) {
		if(Integer.valueOf("7", 16).byteValue()==t.getType()){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, NewReaderMessage t) {
		String hostNumber =  ByteUtil.byteArrToHexString(t.getAddress());   //reader 地址码
		 String timeStamp  =  ByteUtil.byteArrToHexString(t.getTime());   //时间戳
		 //1 字节盘点成功个数（最大 1） + 4 字节 LED 标签全地址 + 2 字节标签信息（如 果成功个数为 0，即盘点失败，则无该字段）+ 保留字段。
		 System.out.println("标签盘点，针对单独标签返回报文==="+ByteUtil.byteArrToHexString(t.getBody()));
		 int ControlSuccessNumber  = ByteUtil.bytesToUbyte(t.getBody(), 0); //盘点成功个数
		 if(ControlSuccessNumber>0) {
			 handleControlSuccessNumber(ControlSuccessNumber,hostNumber,timeStamp,t);
		 }else {
			 handleControlFailureNumber(ControlSuccessNumber,hostNumber,timeStamp,t);
		 }
		return null;
		 
	
	}
	
	private void handleControlSuccessNumber(int ControlSuccessNumber,String hostNumber,String timeStamp,NewReaderMessage t) {

		 String ledTag = ByteUtil.byteArrToHexString(t.getBody(), 1, 4);  // LED 标签地址
      String led = T.byteToBit(t.getBody()[5]);  //LED  字节
      //LED 状态 ： 0 ：灭   1： 慢闪   2：快闪  3：长亮
      int led4 = Integer.parseInt(led.substring(0, 2),2);  //LED4 状态 
      int led3 = Integer.parseInt(led.substring(2, 4),2);  //LED3 状态
      int led2 = Integer.parseInt(led.substring(4, 6),2);  //LED2 状态
      int led1 = Integer.parseInt(led.substring(led.length()-2),2);  //LED1 状态
      //
      String state  = T.byteToBit(t.getBody()[6]); //LED
      int electricityThreshold=Integer.parseInt(state.substring(0, 3),2);  //电量阈值，0~6 
      int electricityQuantity =Integer.parseInt(state.substring(3, 6),2);  //当前电量,0~6 
      int keyState = Integer.parseInt(state.substring(state.length()-2),2); //按键状态   
      logger.info("收到【LED盘点，针对单独标签】请求返回报文: reader 地址码-"+hostNumber+"  LED 标签地址-"+ledTag +
    		"盘点成功个数 -"+ControlSuccessNumber+
      		"/ LED4 状态 -"+led4+
      		"/ LED3 状态 -"+led3+
      		"/ LED2 状态 -"+led2+
      		"/ LED1 状态 -"+led1+
      		"/ 电量阈值 -"+electricityThreshold+
      		" 当前电量 -"+electricityQuantity+
      		" 按键状态 -"+keyState
      		+" 时间戳-"+timeStamp);
      df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
	   RedirectOutputStream.put(df.format(new Date())+"收到【LED盘点，针对单独标签】请求返回报文: reader 地址码-"+hostNumber+"  LED 标签地址-"+ledTag +
	    		"盘点成功个数 -"+ControlSuccessNumber+
	      		"/ LED4 状态 -"+led4+
	      		"/ LED3 状态 -"+led3+
	      		"/ LED2 状态 -"+led2+
	      		"/ LED1 状态 -"+led1+
	      		"/ 电量阈值 -"+electricityThreshold+
	      		" 当前电量 -"+electricityQuantity+
	      		" 按键状态 -"+keyState
	      		+" 时间戳-"+timeStamp);
	}
	
	private void handleControlFailureNumber(int ControlSuccessNumber,String hostNumber,String timeStamp,NewReaderMessage t) {

		 String ledTag = ByteUtil.byteArrToHexString(t.getBody(), 1, 4);  // LED 标签地址
        logger.info("收到【LED盘点，针对单独标签】请求返回报文: reader 地址码-"+hostNumber+"盘点失效  LED 标签地址-"+ledTag +
     		" 时间戳-"+timeStamp);
        df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
 	   RedirectOutputStream.put(df.format(new Date())+"收到【LED盘点，针对单独标签】请求返回报文: reader 地址码-"+hostNumber+"盘点失效  LED 标签地址-"+ledTag +
 	     		" 时间戳-"+timeStamp);
	}

}
