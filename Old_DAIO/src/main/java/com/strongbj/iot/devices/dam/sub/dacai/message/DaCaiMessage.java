package com.strongbj.iot.devices.dam.sub.dacai.message;
/**
 * 大彩消息类型
 * @author yuzhantao
 *
 */
public class DaCaiMessage {
	/**
	 * 消息头
	 */
	public final static String SYS_HEADER = "EE";
	/**
	 * 消息尾
	 */
	public final static String SYS_FOOTER = "FFFCFFFF";
	
	/**
	 * 消息头
	 */
	public final static String SELF_HEADER = "AB";
	/**
	 * 消息尾
	 */
	public final static String SELF_FOOTER = "BB01B1BB";
	/**
	 *  指令
	 */
	private String command;
	/**
	 * 数据
	 */
	private String datas;
	
	private DaCaiMessageType messageType;
	
	private String damCode;
	
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public String getDatas() {
		return datas;
	}
	public void setDatas(String datas) {
		this.datas = datas;
	}
	public DaCaiMessageType getMessageType() {
		return messageType;
	}
	public void setMessageType(DaCaiMessageType messageType) {
		this.messageType = messageType;
	}
	public String getDamCode() {
		return damCode;
	}
	public void setDamCode(String damCode) {
		this.damCode = damCode;
	}
	
}
