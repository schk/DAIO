package com.strongbj.iot.devices.reader.request.entity;
/**
 * 点亮标签的结构体
 * @author yuzhantao
 *
 */
public class CallLabel {
	/**
	 * 设备IP
	 */
	private String devIp;
	/**
	 * 设备编号
	 */
	private String devAddrCode;
	/**
	 * 是否点亮（1点亮；0取消点亮）
	 */
	private int isLight;
	/**
	 * 预点亮的2.4G标签
	 */
	private String label24g;
	public String getDevIp() {
		return devIp;
	}
	public void setDevIp(String devIp) {
		this.devIp = devIp;
	}
	public String getDevAddrCode() {
		return devAddrCode;
	}
	public void setDevAddrCode(String devAddrCode) {
		this.devAddrCode = devAddrCode;
	}
	public int getIsLight() {
		return isLight;
	}
	public void setIsLight(int isLight) {
		this.isLight = isLight;
	}
	public String getLabel24g() {
		return label24g;
	}
	public void setLabel24g(String label24g) {
		this.label24g = label24g;
	}
	
	
}
