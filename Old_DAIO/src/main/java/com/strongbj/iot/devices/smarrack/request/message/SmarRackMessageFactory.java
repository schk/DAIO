package com.strongbj.iot.devices.smarrack.request.message;

import com.google.common.base.Preconditions;
import com.strongbj.core.util.ByteUtil;

public class SmarRackMessageFactory {
	private final static byte[] HEADER = {(byte)0xFE ,(byte)0xFE};
	public static byte[] createMessage(Byte command, byte[] deviceCode, byte[] datas) {
		Preconditions.checkNotNull(command);
		Preconditions.checkNotNull(deviceCode);
		Preconditions.checkArgument(deviceCode.length==4);
		Preconditions.checkNotNull(datas);
		
		byte[] retDatas = new byte[11+datas.length];
		System.arraycopy(HEADER, 0, retDatas, 0, 2); // 拷贝头
		short len = (short) (7+datas.length);
		System.arraycopy(ByteUtil.shortToByteArr(len), 0, retDatas, 2, 2); // 拷贝数据长度
		retDatas[4]=command; // 拷贝命令
		System.arraycopy(deviceCode, 0, retDatas, 7, 4); // 拷贝设备ID
		System.arraycopy(datas, 0, retDatas, 11, datas.length);
		return retDatas;
	}
}
