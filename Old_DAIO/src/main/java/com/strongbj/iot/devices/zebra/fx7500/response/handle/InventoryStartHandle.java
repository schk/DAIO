package com.strongbj.iot.devices.zebra.fx7500.response.handle;

import com.mot.rfid.api3.RfidStatusEvents;
import com.mot.rfid.api3.STATUS_EVENT_TYPE;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.iot.devices.zebra.fx7500.response.entity.TagCache;

import io.netty.channel.ChannelHandlerContext;

public class InventoryStartHandle implements IMessageHandle<RfidStatusEvents,Object> {
	private TagCache tagCache;
	
	public InventoryStartHandle(TagCache tagCache){
		this.tagCache=tagCache;
	}
	
	@Override
	public boolean isHandle(RfidStatusEvents t) {
		STATUS_EVENT_TYPE statusType = t.StatusEventData.getStatusEventType();
		if(statusType == STATUS_EVENT_TYPE.INVENTORY_START_EVENT){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, RfidStatusEvents t) {
		tagCache.clear();
		return null;
	}

}