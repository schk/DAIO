package com.strongbj.iot.devices.dam.request.handle.dam;

/**
 * 获取设备参数
 * 
 * @author yuzhantao
 *
 */
public class GetDeviceParamsHandle extends GetParamsHandle {
	public GetDeviceParamsHandle() {
		super("getDeviceParams", (byte) 0x19);
	}
}
