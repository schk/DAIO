package com.strongbj.iot.devices.hr.response.entity;

/**
 * 门禁心跳包实体类
 * 
 * @author Pingfan
 *
 */
public class HeartPackageEntity {
	/**
	 * 门禁设备ip
	 */
	private String devIp;
	/**
	 * 用于区分各个操作的指令
	 */
	private byte command;
	/**
	 * 设备主动上传心跳包的时间间隔值，单位为秒
	 */
	private int heartTime;
	
	public String getDevIp() {
		return devIp;
	}
	public void setDevIp(String devIp) {
		this.devIp = devIp;
	}
	public byte getCommand() {
		return command;
	}
	public void setCommand(byte command) {
		this.command = command;
	}
	public int getHeartTime() {
		return heartTime;
	}
	public void setHeartTime(int heartTime) {
		this.heartTime = heartTime;
	}
}
