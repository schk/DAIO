package com.strongbj.iot.devices.dam.response.entity;

import com.strongbj.core.util.ByteUtil;

/**
 * 温度传感器工厂
 * @author yuzhantao
 *
 */
public class TemperatureSensorFactory implements ISensorFactory {
	
	@Override
	public SensorEntity createSensor(byte[] src, int offset) {
		TemperatureSensorEntity sensor = new TemperatureSensorEntity();
		sensor.setSensorType((byte)0);
		sensor.setTemperature((short)((float)ByteUtil.byteArrToShort(src[offset+2],src[offset+1])/10.0f));
		
//		byte[] temp = new byte[] {src[offset+2],src[offset+1]};
//		System.out.println("源数据："+ByteUtil.byteArrToHexString(temp)+"    value="+sensor.getTemperature());
		return sensor;
	}

}
