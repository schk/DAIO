package com.strongbj.iot.devices.amazonreader.response.entity;
/**
 * Led标签型设备的电压状态
 * @author yuzhantao
 *
 */
public enum LedLabelVoltageState {
	/**
	 * 电压正常
	 */
	Normal,
	/**
	 * 电压预警
	 */
	Alarm
}
