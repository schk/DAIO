package com.strongbj.iot.devices.dam.common;

/**
 * 任务管理
 * 
 * @author yuzhantao
 *
 */
public interface ITaskManager {
	/**
	 * 添加发送任务
	 * 
	 * @param runnable
	 */
	void addSendTask(Runnable runnable);
	
	void destory();
}
