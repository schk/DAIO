package com.strongbj.iot.devices.dam.response.handle.v1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.core.util.ContextUtils;
import com.strongbj.iot.devices.dam.message.DAMMessage;
import com.strongbj.iot.devices.dam.message.MQMessageOfDAM;
import com.strongbj.iot.devices.dam.response.entity.NetParamsResultEntity;
import com.strongbj.iot.mq.producer.TopicSender;

import io.netty.channel.ChannelHandlerContext;

public class GetNetParamsResultHandle  extends DAMV1Handle {
	private static Logger logger = LogManager.getLogger(TemperatureAndHumidityHandle.class.getName());
	private TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");
	private static final String TOPIC="damCommandToService";
	private static final String ACTION_CODE = "resultNetParams";
	private static final String DEV_TYPE="damDc";
	
	@Override
	protected boolean isHandleDAMMessage(DAMMessage t) {
		if(t.getCommand()==0x13){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, DAMMessage t) {
		MQMessageOfDAM msg = new MQMessageOfDAM();
		msg.setActioncode(ACTION_CODE);
		
		NetParamsResultEntity netParams=message2NetParamsResultEntity(t);
		msg.setAwsPostdata(netParams);
		msg.setDevType(DEV_TYPE);
		msg.setTimestamp(ByteUtil.bytesToLong(t.getSn()));
		String json = JSON.toJSONString(msg);
		logger.info("DAM设备 向MQ发送获取网络参数的返回信息	 JSON={}",json);
		topicSender.send(TOPIC, json);
		return null;
	}
	
	private NetParamsResultEntity message2NetParamsResultEntity(DAMMessage t) {
		NetParamsResultEntity netParams=new NetParamsResultEntity();
		byte[] srcDatas = t.getDatas();
		String ip = Integer.toString(srcDatas[0])+"."+
				Integer.toString(srcDatas[1])+"."+
				Integer.toString(srcDatas[2])+"."+
				Integer.toString(srcDatas[3]);
		String mask = Integer.toString(srcDatas[4])+"."+
				Integer.toString(srcDatas[5])+"."+
				Integer.toString(srcDatas[6])+"."+
				Integer.toString(srcDatas[7]);
		String gateway = Integer.toString(srcDatas[8])+"."+
				Integer.toString(srcDatas[9])+"."+
				Integer.toString(srcDatas[10])+"."+
				Integer.toString(srcDatas[11]);
		netParams.setIp(ip);
		netParams.setMask(mask);
		netParams.setGateway(gateway);
		return netParams;
	}
}

