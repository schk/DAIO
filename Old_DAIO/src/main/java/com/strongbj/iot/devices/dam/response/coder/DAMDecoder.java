package com.strongbj.iot.devices.dam.response.coder;

import com.strongbj.iot.devices.dam.message.DAMMessage;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;

public class DAMDecoder extends DelimiterBasedFrameDecoder {
	/**
	 * 头部参数的数据长度
	 */
	private static final int PARAM_SIZE=18;
	// 头部起始码 长度
	private static final int HEADER_SIZE = 2;
	
	private static int SN_SIZE=8;
	
	private static int ADDRESS_CODE_SIZE=3;
	
	// 验证码长度
	private static final int CRC_SIZE=2;
	
	private final static byte HEADER_1 = Integer.valueOf("AA", 16).byteValue();
	
	private final static byte HEADER_2 = Integer.valueOf("55", 16).byteValue();
	
	private final static byte[] END_FRAMES = {0x0D,0x0A,0x55,0x0D,0x0A};
	
	/**
	 * 字节头
	 */
	private byte header1;
	private byte header2;
	/**
	 * 8位标记位
	 */
	private byte[] sn = new byte[SN_SIZE];
	/**
	 * 地址吗
	 */
	private byte[] addressCode=new byte[ADDRESS_CODE_SIZE];
	/**
	 * 除head头、校验位和结束符的所有数据长度
	 */
	private short len;
	/**
	 * 设备类型
	 */
	private byte deviceType;
	/**
	 * 版本
	 */
	private byte version;
	/**
	 * 命令码
	 */
	private byte command;
	/**
	 * 总帧数
	 */
	private byte frameTotal;
	/**
	 * 当前帧索引
	 */
	private byte currentFrameIndex;
	/**
	 * 数据区
	 */
	private byte[] datas=null;
	/**
	 * 校验码
	 */
	private byte[] crc=new byte[CRC_SIZE];
	
	/** 
     *  
     * @param maxFrameLength 解码时，处理每个帧数据的最大长度 
     */  
	public DAMDecoder(int maxFrameLength) {
		super(maxFrameLength, Unpooled.copiedBuffer(END_FRAMES)); // 解决粘包问题
	}

	@Override  
	protected Object decode(ChannelHandlerContext ctx, ByteBuf in) throws Exception { 
		in = (ByteBuf) super.decode(ctx, in);
//		int len = in.readableBytes();
//		byte [] datas = new byte[len];
//		in.readBytes(datas);
//		System.out.println(ByteUtil.byteArrToHexString(datas));
//		return null;
//		
    	if (in == null) {  
            return null;  
        }
    	
    	if(this.datas==null){
//    		System.out.println("#info 可读信息长度="+in.readableBytes());
	        if (in.readableBytes() < (HEADER_SIZE+PARAM_SIZE)) {  
	            return null;
	        }
	        header1=in.readByte();
	        if(header1!= HEADER_1){
	        	return null;
	        }
	        header2=in.readByte();
	        if(header2!= HEADER_2){
	        	return null;
	        }
	        in.readBytes(sn); // 读取8位标记位
	        
	        len = in.readShort();
	        
	        in.readBytes(addressCode);
	        deviceType = in.readByte();
	        version = in.readByte();
	        command = in.readByte();
	        
	        frameTotal = in.readByte();
	        currentFrameIndex = in.readByte();
	        
	        datas=new byte[len-PARAM_SIZE];
    	}

        if(len-PARAM_SIZE+CRC_SIZE>in.readableBytes()) return null;
        
        in.readBytes(datas);
        in.readBytes(crc);
        
//        short tmpCrc = this.getCrc();
//        if(tmpCrc!=ByteUtil.byteArrToShort(crc)) return null;
        
        in.release();
        
        DAMMessage msg = new DAMMessage(sn,addressCode,deviceType,version,command,frameTotal,currentFrameIndex,datas);
//        if(msg.getCommand()==0x32) {
//        	DAMMessageFactory factory = new DAMMessageFactory();
//        	byte[] dd = factory.createDAMMessage(sn, addressCode, deviceType, version, command, datas);
//        	System.out.println("收到数据:"+ByteUtil.byteArrToHexString(dd));
//        }
        this.datas=null;
        
        return  msg;
    }
    
//    private short getCrc(){
//    	short tmpCrc = addressCode[0];
//        tmpCrc ^= addressCode[1];
//        tmpCrc ^= addressCode[2];
//        tmpCrc ^= deviceType;
//        tmpCrc ^= version;
//        tmpCrc ^= command;
//        tmpCrc ^= frameTotal;
//        tmpCrc ^= currentFrameIndex;
//        for(byte b:datas){
//        	tmpCrc ^= b;
//        }
//        return tmpCrc;
//    }
}
