package com.strongbj.iot.devices.amazondb.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "tags")
public class Tag {
	@Id
	private String id;
	@Field("readerCode")
	private String readerCode;
	@Field("batterState")
	private String batterState;
	@Field("shortAddr")
	private String shortAddr;
	@Field("wlightstate")
	private String wlightstate;
	@Field("blightstate")
	private String blightstate;
	@Field("gightstate")
	private String gightstate;
	@Field("rlightstate")
	private String rlightstate;
	@Field("btnstate")
	private String btnstate;
	@Field("updateTime")
	private Date updateTime;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getReaderCode() {
		return readerCode;
	}
	public void setReaderCode(String readerCode) {
		this.readerCode = readerCode;
	}
	public String getBatterState() {
		return batterState;
	}
	public void setBatterState(String batterState) {
		this.batterState = batterState;
	}
	public String getShortAddr() {
		return shortAddr;
	}
	public void setShortAddr(String shortAddr) {
		this.shortAddr = shortAddr;
	}
	public String getWlightstate() {
		return wlightstate;
	}
	public void setWlightstate(String wlightstate) {
		this.wlightstate = wlightstate;
	}
	public String getBlightstate() {
		return blightstate;
	}
	public void setBlightstate(String blightstate) {
		this.blightstate = blightstate;
	}
	public String getGightstate() {
		return gightstate;
	}
	public void setGightstate(String gightstate) {
		this.gightstate = gightstate;
	}
	public String getRlightstate() {
		return rlightstate;
	}
	public void setRlightstate(String rlightstate) {
		this.rlightstate = rlightstate;
	}
	public String getBtnstate() {
		return btnstate;
	}
	public void setBtnstate(String btnstate) {
		this.btnstate = btnstate;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}
