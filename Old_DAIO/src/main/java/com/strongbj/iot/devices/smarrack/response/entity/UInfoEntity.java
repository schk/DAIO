package com.strongbj.iot.devices.smarrack.response.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UInfoEntity {
	
	private List<DevAddr> devAddrList=new ArrayList<>();
	
	public class DevAddr{
		
		private String devAddrCode;
		private List<UdevInfo> udevInfo=new ArrayList<>();
		private byte doorState;
		private Date doorStateUpdateTime;
		private Date checkUpdateTime;
		
		public String getDevAddrCode() {
			return devAddrCode;
		}
		public void setDevAddrCode(String devAddrCode) {
			this.devAddrCode = devAddrCode;
		}
		public List<UdevInfo> getUdevInfo() {
			return udevInfo;
		}
		public void setUdevInfo(List<UdevInfo> udevInfo) {
			this.udevInfo = udevInfo;
		}
		public byte getDoorState() {
			return doorState;
		}

		public void setDoorState(byte doorState) {
			this.doorState = doorState;
		}

		public Date getDoorStateUpdateTime() {
			return doorStateUpdateTime;
		}

		public void setDoorStateUpdateTime(Date doorStateUpdateTime) {
			this.doorStateUpdateTime = doorStateUpdateTime;
		}
		public Date getCheckUpdateTime() {
			return checkUpdateTime;
		}
		public void setCheckUpdateTime(Date checkUpdateTime) {
			this.checkUpdateTime = checkUpdateTime;
		}
	}
	
	public class UdevInfo{
		private Integer height;
		private Integer U;
		private String rfid;
		
		public Integer getU() {
			return U;
		}
		public void setU(Integer u) {
			U = u;
		}
		public String getRfid() {
			return rfid;
		}
		public void setRfid(String rfid) {
			this.rfid = rfid;
		}
		public Integer getHeight() {
			return height;
		}
		public void setHeight(Integer height) {
			this.height = height;
		}

	}

	public List<DevAddr> getDevAddrList() {
		return devAddrList;
	}

	public void setDevAddrList(List<DevAddr> devAddrList) {
		this.devAddrList = devAddrList;
	}
	
}
