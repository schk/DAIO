package com.strongbj.iot.devices.dam.request.handle.dam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSONObject;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.dam.common.AbstractTaskDelayedRunnable;
import com.strongbj.iot.devices.dam.common.DAMManager;
import com.strongbj.iot.devices.dam.common.ITaskManager;
import com.strongbj.iot.devices.dam.common.NetMapping;
import com.strongbj.iot.devices.dam.message.DAMMessageFactory;
import com.strongbj.iot.devices.dam.message.MQMessageOfDAM;
import com.strongbj.iot.devices.dam.request.entity.NetParamesEntity;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;

/**
 * 设置网络参数的处理
 * 
 * @author yuzhantao
 *
 */
public class SetNetParamesHandle implements IMessageHandle<MQMessageOfDAM, Object> {
	private static Logger logger = LogManager.getLogger(SetNetParamesHandle.class.getName());
	private static final String ACTION_CODE = "setNetParames"; // 事件处理编码

	private final DAMMessageFactory damMessageFactory = new DAMMessageFactory(); // DAM消息工厂

	@Override
	public boolean isHandle(MQMessageOfDAM t) {
		if (t.getActioncode().equals(ACTION_CODE)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 将设置网络参数的信息发送到硬件
	 */
	@Override
	public Object handle(ChannelHandlerContext ctx, MQMessageOfDAM t) {
		final NetParamesEntity np = ((JSONObject) t.getAwsPostdata()).toJavaObject(NetParamesEntity.class);
		Channel channel = NetMapping.getInstance().getChannel(np.getRackConverCode());
		ITaskManager taskManager = DAMManager.getTaskManager(np.getRackConverCode());
		taskManager.addSendTask(new AbstractTaskDelayedRunnable() {
			@Override
			protected void taskRun() {
				sendDatasToDAM(channel,t,np);
			}
			
		});
		return null;
	}

	private void sendDatasToDAM(Channel channel, MQMessageOfDAM t, NetParamesEntity np) {
		if (channel != null && channel.isActive()) {
			final byte[] datas = NetParamesEntity2Bytes(t.getTimestamp(), np); // 将网络参数对象转为byte数组，用于发送到dam硬件
			ByteBuf bs = Unpooled.copiedBuffer(datas);
			ChannelFuture cf = channel.writeAndFlush(bs);
			// TODO 添加监听,查看发送情况DEBUG
			cf.addListener(new ChannelFutureListener() {

				@Override
				public void operationComplete(ChannelFuture future) throws Exception {
					if (future.isSuccess()) {
						logger.info("WEB下发设置网络参数命令到DAM 编号:{} 下发命令={}  ", np.getRackConverCode(),
								ByteUtil.byteArrToHexString(datas, true));
					} else {
						logger.error("WEB下发设置网络参数命令到DAM 编号:{} 下发命令={}  ", np.getRackConverCode(),
								ByteUtil.byteArrToHexString(datas, true));
					}
				}

			});
		} else {
			logger.error("{} 主机没有找到指定channel", np.getRackConverCode());
		}
	}

	/**
	 * 网络参数对象转byte数组
	 * 
	 * @param np
	 * @return
	 */
	private byte[] NetParamesEntity2Bytes(long time, NetParamesEntity np) {
		// 创建
		byte[] netParamsDatas = new byte[12];
		// 将ip赋值到4byte数组中
		this.setParamsToBytes(np.getIp(), netParamsDatas, 0);
		// 将子网掩码赋值到4byte数组中
		this.setParamsToBytes(np.getMask(), netParamsDatas, 4);
		// 将默认网关赋值到4byte数组中
		this.setParamsToBytes(np.getGateway(), netParamsDatas, 8);

		// 将命令发送到屏幕硬件
		byte[] datas = damMessageFactory.createDAMMessage(ByteUtil.longToBytes(time), np.getRackConverCode(), // 设备编号
				(byte) 0, // 设备类型，默认填0
				(byte) 0, // 版本号，默认填0
				(byte) 0x10, // 设置网络参数命令
				netParamsDatas // 设置的网络参数数据
		); // 通过dam消息工厂获取设置网络参数的byte命令数组
		return datas;
	}

	/**
	 * 设置指定*.*.*.*格式的数据转为4byte数组
	 * 
	 * @param srcString  源数据
	 * @param destBytes  目标数组
	 * @param destOffset 赋值的目标数组偏移量
	 */
	private void setParamsToBytes(String srcString, byte[] destBytes, int destOffset) {
		String[] strIps = srcString.split("[.]");
		for (int i = 0; i < strIps.length; i++) {
			destBytes[i + destOffset] = Integer.valueOf(strIps[i], 10).byteValue();
		}
	}
}
