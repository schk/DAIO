package com.strongbj.iot.devices.guis.respnose.common;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.strongbj.core.util.ContextUtils;
import com.strongbj.iot.devices.guis.request.entity.ReturnUInfoEntity;
import com.strongbj.iot.devices.guis.respnose.message.GUISMQMessage;
import com.strongbj.iot.mq.producer.TopicSender;

/**
 * 
 * 循环遍历存储时间戳的数组，如果时间差值大于4秒向上返回点亮失败
 * @author 25969
 *
 */
public class SendLightAnswerListener extends Thread{
	private static Logger logger = LogManager.getLogger(SendLightAnswerListener.class.getName());
	private TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");
    private static final String rfidtype="smarrack";
	
/*	public long hid;   // 要判断的机柜流水号
	


	public long getHid() {
	return hid;
}



public void setHid(long hid) {
	this.hid = hid;
}*/



	public void run(){
		GuisCommonEntity guis = GuisCommonEntity.getInstance();
		while(true) {   //循环遍历存储时间戳的数组，如果时间差值大于4秒向上返回点亮失败
			   for(Map.Entry<Long, Long> entry: guis.lightReturnMap.entrySet()) {  
		  //           System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue()); 
		             Long s = (System.currentTimeMillis() - entry.getValue()) / 1000;
		             if(s >25) {
		       		  ReturnUInfoEntity devs = new ReturnUInfoEntity();
		       		   devs.setDevAddrCode(guis.getDevByTimeStamp(entry.getKey()));
		       		 devs.setSuccess(false);
		       		   GUISMQMessage mes = new GUISMQMessage();
		       		   mes.setActioncode("reader605");
		       		   mes.setTimeStamp(String.valueOf(entry.getKey()));
		       		   mes.setAwsPostdata(devs);
		       		   mes.setRfidtype(rfidtype);
		       		   mes.setTimeout(true);
		       		  String json = JSON.toJSONString(mes);
		       		  logger.info("GUIS 返回结果给京东 json={}",json);
		       		  topicSender.send("daioReader", json);
		       		  try {
		       			guis.del(entry.getKey());   //重置
		       			 guis.selDev(entry.getKey());
		       		} catch (Exception e) {
		       			e.printStackTrace();
		       		}
		       		  devs=null;
		       		  mes=null;
		       		  json=null;
		             }
		        }  
	
		}
		}
	
}
