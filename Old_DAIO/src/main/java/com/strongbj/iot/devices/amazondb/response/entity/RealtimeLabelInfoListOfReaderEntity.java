package com.strongbj.iot.devices.amazondb.response.entity;

import java.util.List;

public class RealtimeLabelInfoListOfReaderEntity {
	private String devsn;
	
	private List<RealtimeLabelInfoOfReaderEntity> cardlist;

	public String getDevsn() {
		return devsn;
	}

	public void setDevsn(String devsn) {
		this.devsn = devsn;
	}

	public List<RealtimeLabelInfoOfReaderEntity> getCardlist() {
		return cardlist;
	}

	public void setCardlist(List<RealtimeLabelInfoOfReaderEntity> cardlist) {
		this.cardlist = cardlist;
	}

}
