package com.strongbj.iot.devices.amazonNewReader.response.handle;

import com.strongbj.core.message.IMessageHandle;
import com.strongbj.iot.devices.amazonNewReader.message.NewReaderMessage;

import io.netty.channel.ChannelHandlerContext;

public class readerGetsTagBindingInformationHandle implements IMessageHandle<NewReaderMessage,Object>{

//	private static Logger logger = LogManager.getLogger(readerGetsTagBindingInformationHandle.class.getName());
//	private static SimpleDateFormat df=null;
	@Override
	public boolean isHandle(NewReaderMessage t) {
		if(Integer.valueOf("0x0B", 16).byteValue()==t.getType()){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, NewReaderMessage t) {
		System.out.println("收到读写器获取标签绑定信息 ");
		return null;
	}

}
