package com.strongbj.iot.devices.reader.response.entity;

import java.util.List;
/**
 * 实时在Reader中采集的标签信息
 * @author yuzhantao
 *
 */
public class RealtimeLabelInfoOfReaderEntity {
	private String devAddrCode;
	private List<String> devInfo;
	public String getDevAddrCode() {
		return devAddrCode;
	}
	public void setDevAddrCode(String devAddrCode) {
		this.devAddrCode = devAddrCode;
	}
	public List<String> getDevInfo() {
		return devInfo;
	}
	public void setDevInfo(List<String> devInfo) {
		this.devInfo = devInfo;
	}
	
	
}
