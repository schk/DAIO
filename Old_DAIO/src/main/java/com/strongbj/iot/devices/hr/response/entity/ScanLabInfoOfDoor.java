package com.strongbj.iot.devices.hr.response.entity;

import java.util.List;

public class ScanLabInfoOfDoor {
	private String devIp;
	private String direction;
	private List<String> rfids;
	public String getDevIp() {
		return devIp;
	}
	public void setDevIp(String devIp) {
		this.devIp = devIp;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public List<String> getRfids() {
		return rfids;
	}
	public void setRfids(List<String> rfids) {
		this.rfids = rfids;
	}
 }
