package com.strongbj.iot.devices.amazonNewReader.request.listener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.listener.adapter.MessageListenerAdapter;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.google.gson.reflect.TypeToken;
import com.strongbj.core.message.MessageHandleContext;
import com.strongbj.iot.devices.amazonNewReader.message.MQMessageOfAmazonNewReader;
import com.strongbj.iot.devices.amazonNewReader.request.handle.LabelBindHandle;
import com.strongbj.iot.devices.amazonNewReader.request.handle.LabelBindingHandle;
import com.strongbj.iot.devices.amazonNewReader.request.handle.LabelRebindingHandle;
import com.strongbj.iot.devices.amazonNewReader.request.handle.LabelUntyingHandle;
import com.strongbj.iot.devices.amazonNewReader.request.handle.LedControlForManyHandle;
import com.strongbj.iot.devices.amazonNewReader.request.handle.LedControlForSingleHandle;
import com.strongbj.iot.devices.amazonNewReader.request.handle.LedInventoryForManyHandle;
import com.strongbj.iot.devices.amazonNewReader.request.handle.LedInventoryForSingleHandle;
import com.strongbj.iot.devices.amazonNewReader.request.handle.GettingTheReaderAddressHandle;
import com.strongbj.iot.devices.amazonNewReader.request.handle.GettingUpReaderNetworkParametersHandle;
import com.strongbj.iot.devices.amazonNewReader.request.handle.SettingTheReaderAddressHandle;
import com.strongbj.iot.devices.amazonNewReader.request.handle.SettingUpReaderNetworkParametersHandle;

@Component
public class MQMessageOfAmazonNewReaderListener extends MessageListenerAdapter{

	private static Logger logger = LogManager.getLogger(MQMessageOfAmazonNewReaderListener.class.getName());
	// 通过策略模式处理不同的信息
	private MessageHandleContext<MQMessageOfAmazonNewReader,Object> messageHandleContent;
	
	
	public MQMessageOfAmazonNewReaderListener(){
		super();
		this.messageHandleContent=new MessageHandleContext<>();
		// 添加消息处理类 ----（标签绑定请求）
		this.messageHandleContent.addHandleClass(new LabelBindingHandle());
		// 添加消息处理类 --- （标签绑定）
		this.messageHandleContent.addHandleClass(new LabelBindHandle());
		// 添加消息处理类 --  (LED 控制，针对单独标签)
		this.messageHandleContent.addHandleClass(new LedControlForSingleHandle());
		// 添加消息处理类 -- (LED 控制 针对多个标签)
		this.messageHandleContent.addHandleClass(new LedControlForManyHandle());
		// 添加消息处理类 -- (标签盘点，针对单独标签) 
	    this.messageHandleContent.addHandleClass(new LedInventoryForSingleHandle());
	    // 添加消息处理类 -- (标签重新绑定)
	    this.messageHandleContent.addHandleClass(new LabelRebindingHandle());
	    // 添加消息处理类 -- (标签解绑)
	    this.messageHandleContent.addHandleClass(new LabelUntyingHandle());
	    // 添加消息处理类 --- (标签盘点，针对多个标签，最大数量为 50)
	    this.messageHandleContent.addHandleClass(new LedInventoryForManyHandle());
	    // 添加消息处理类 --- (设置读写器地址)
	    this.messageHandleContent.addHandleClass(new SettingTheReaderAddressHandle());
	    // 添加消息处理类 --- (获取读写器当前地址)
	    this.messageHandleContent.addHandleClass(new GettingTheReaderAddressHandle());
	    // 添加消息处理类 --- (设置读写器网络参数) 
	    this.messageHandleContent.addHandleClass(new SettingUpReaderNetworkParametersHandle());
	    // 添加消息处理类 --- (获取读写器网络参数)
	    this.messageHandleContent.addHandleClass(new GettingUpReaderNetworkParametersHandle());
	    
	}
	
	@JmsListener(destination="DAIO",concurrency="1")
    public void onMessage(Message message, Session session) throws JMSException {
		if(message instanceof TextMessage){
			TextMessage tm = (TextMessage)message;
			String json = tm.getText();
			MQMessageOfAmazonNewReader rm = JSON.parseObject(json,new TypeToken<MQMessageOfAmazonNewReader>(){}.getType());
			this.messageHandleContent.handle(null,rm);
		}else{
			logger.info("无法解析的mq对象消息:"+message.getClass().getName());
		}
    }
	
	
}
