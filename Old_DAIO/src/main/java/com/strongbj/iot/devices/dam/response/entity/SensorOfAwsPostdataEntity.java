package com.strongbj.iot.devices.dam.response.entity;

import java.util.List;

public class SensorOfAwsPostdataEntity {
	private List<SensorParamesEntity> sensorList;
	private String rackConverCode;
	public List<SensorParamesEntity> getSensorList() {
		return sensorList;
	}
	public void setSensorList(List<SensorParamesEntity> sensorList) {
		this.sensorList = sensorList;
	}
	public String getRackConverCode() {
		return rackConverCode;
	}
	public void setRackConverCode(String rackConverCode) {
		this.rackConverCode = rackConverCode;
	}
}
