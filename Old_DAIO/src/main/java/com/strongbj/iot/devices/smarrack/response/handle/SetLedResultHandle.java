package com.strongbj.iot.devices.smarrack.response.handle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Preconditions;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ContextUtils;
import com.strongbj.iot.devices.smarrack.message.SmarRackMQMessage;
import com.strongbj.iot.devices.smarrack.message.SmarRackMessage;
import com.strongbj.iot.mq.producer.TopicSender;

import io.netty.channel.ChannelHandlerContext;

/**
 * 设置LED的返回结果处理
 * 
 * @author yuzhantao
 *
 */
public class SetLedResultHandle implements IMessageHandle<SmarRackMessage, Object> {
	private static Logger logger = LogManager.getLogger(UploadUInfoHandle.class.getName());
	private final static byte COMMAND_ID = (byte) 0xAB;
	private final static String ACTION_CODE = "SetLedResult";
	private final static String RFID_TYPE = "smarrack";
	private TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");

	@Override
	public boolean isHandle(SmarRackMessage t) {
		if (t.getCommand() == COMMAND_ID) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, SmarRackMessage t) {
		Preconditions.checkArgument(t.getBody().length > 0);

		SmarRackMQMessage mqMessage = new SmarRackMQMessage();
		mqMessage.setActioncode(ACTION_CODE);
		mqMessage.setRfidtype(RFID_TYPE);
		mqMessage.setAwsPostdata(t.getBody()[0] == 0 ? true : false);
		String json = JSON.toJSONString(mqMessage);
		topicSender.send("smarRackToService", json);
		logger.info("上传设置LED的返回信息=", json);

		return null;
	}
}
