package com.strongbj.iot.devices.smarrack.message;

import com.strongbj.core.message.IMessageFactory;
import com.strongbj.core.util.ByteUtil;

public class SmarRackMessageFactory implements IMessageFactory<SmarRackMessage,byte[]> {

	private final static byte[] RESULT_DATAS=new byte[]{0};
	/**
	 * 创建基站主动上发标签的协议
	 * @param msg
	 * @return
	 * @throws Exception 
	 */
	private byte[] createSmarRackMessage(
			byte command,short devVersion,byte[] devId,byte[] body
			) throws Exception{
		
		byte[] result = new byte[11+body.length];
		// 设置协议头为FE FE
		result[0]=(byte)254;
		result[1]=(byte)254;
		// 数据长度
		int len = 7+body.length;
		byte[] bLen = ByteUtil.shortToByteArr((short)len);
		System.arraycopy(bLen, 0, result, 2, bLen.length);
		
		// 命令码（1位）
		result[4]=command;
		// 版本号（2位）
		byte[] ver = ByteUtil.shortToByteArr(devVersion);
		System.arraycopy(ver, 0, result, 5, 2);
		// 设备ID（4位）
		System.arraycopy(devId, 0, result, 7, devId.length);
		// 数据位
		System.arraycopy(body, 0, result, 11, body.length);
		
		return result;
	}

	@Override
	public byte[] parse(SmarRackMessage src) {
		try {
			return this.createSmarRackMessage(src.getCommand(), src.getDevVersion(), src.getDevId(), RESULT_DATAS);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
