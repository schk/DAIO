package com.strongbj.iot.devices.amazonreader.request.message;

public class MQMessage<T> {
	private String GUDI;
	private String actionCode;
	private T awsPostdata;
	public String getGUDI() {
		return GUDI;
	}
	public void setGUDI(String gUDI) {
		GUDI = gUDI;
	}

	public String getActionCode() {
		return actionCode;
	}
	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}
	public T getAwsPostdata() {
		return awsPostdata;
	}
	public void setAwsPostdata(T awsPostdata) {
		this.awsPostdata = awsPostdata;
	}
	
}
