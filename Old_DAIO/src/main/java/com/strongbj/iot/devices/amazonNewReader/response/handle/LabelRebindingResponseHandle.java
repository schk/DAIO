package com.strongbj.iot.devices.amazonNewReader.response.handle;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.amazonNewReader.message.NewReaderMessage;
import com.strongbj.iot.devices.guis.respnose.common.RedirectOutputStream;

import io.netty.channel.ChannelHandlerContext;
/**
 * 标签重新绑定  Handle
 * @author 25969
 *
 */
public class LabelRebindingResponseHandle implements IMessageHandle<NewReaderMessage,Object>{
	private static Logger logger = LogManager.getLogger(LabelRebindingResponseHandle.class.getName());
	private static SimpleDateFormat df=null;
	@Override
	public boolean isHandle(NewReaderMessage t) {
		if(Integer.valueOf("3", 16).byteValue()==t.getType()){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, NewReaderMessage t) {

		String hostNumber =  ByteUtil.byteArrToHexString(t.getAddress());   //reader 地址码
		 String timeStamp  =  ByteUtil.byteArrToHexString(t.getTime());   //时间戳
		 //4 字节标签地址 + 2 字节标签短地址 + 2 字节标签信息（参考 LED 标签信息格 式，下同） + 保留字段。
		 String ledTag = ByteUtil.byteArrToHexString(t.getBody(), 0, 4);  // LED 标签地址
//		 String shortAddress  = ByteUtil.byteArrToHexString(t.getBody(), 4, 2);  //签短地址 
        int led  = ByteUtil.bytesToUbyte(t.getBody(), 6); //LED
        //LED 状态 ： 0 ：灭   1： 慢闪   2：快闪  3：长亮
        int led4 = ((led >> 6) & 0x3);  //LED4 状态 
        int led3 = ((led >> 4) & 0x3);  //LED3 状态
        int led2 = ((led >> 2) & 0x1);  //LED2 状态
        int led1 = ((led >> 0) & 0x1);  //LED1 状态
        //
        int state  = ByteUtil.bytesToUbyte(t.getBody(), 7); //LED
        int electricityThreshold=((state >> 5) & 0x3);  //电量阈值，0~6 
        int electricityQuantity =((state >> 2) & 0x1);  //当前电量,0~6 
        int keyState = ((state >> 0) & 0x1); //按键状态
        String log ="收到标签重新绑定请求返回报文: reader 地址码-"+hostNumber+"  LED 标签地址-"+ledTag +
        		" LED4 状态 -"+led4+
        		" LED3 状态 -"+led3+
        		" LED2 状态 -"+led2+
        		" LED1 状态 -"+led1+
        		" 电量阈值 -"+electricityThreshold+
        		" 当前电量 -"+electricityQuantity+
        		" 按键状态 -"+keyState
        		+" 时间戳-"+timeStamp;
        logger.info(log);
        df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
 	     RedirectOutputStream.put(df.format(new Date())+ log);
		return null;
	
	}

}
