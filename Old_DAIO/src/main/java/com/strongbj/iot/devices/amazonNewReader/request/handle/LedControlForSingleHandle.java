package com.strongbj.iot.devices.amazonNewReader.request.handle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.amazonNewReader.message.AmazonNewReaderOSMessageFactory;
import com.strongbj.iot.devices.amazonNewReader.message.MQMessageOfAmazonNewReader;
import com.strongbj.iot.devices.amazonNewReader.request.entity.AmazonNewReaderCommenEntity;
import com.strongbj.iot.devices.amazonNewReader.request.entity.T;
import com.strongbj.iot.devices.amazonNewReader.response.handle.NewReaderResponseHandleContext;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
/**
 * LED 控制，针对单独标签
 * @author 25969
 *
 */
public class LedControlForSingleHandle implements IMessageHandle<MQMessageOfAmazonNewReader,Object>{
	private static Logger logger = LogManager.getLogger(LedControlForSingleHandle.class.getName());
	private final static String ACTION_CODE = "ledControlForSingle";   //MQ中判断消息类型的标识符
	private final static byte command = (byte)0x05;
	private AmazonNewReaderOSMessageFactory amazonNewReaderOSMessageFactory = new AmazonNewReaderOSMessageFactory();
//	private static SimpleDateFormat df=null;
	@Override
	public boolean isHandle(MQMessageOfAmazonNewReader t) {
		if(t.getActioncode().equals(ACTION_CODE)){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, MQMessageOfAmazonNewReader t) {
		logger.info("AmazonNewReader 收到MQ发来的“LED 控制，针对单独标签” json={}",JSON.toJSONString(t));
		AmazonNewReaderCommenEntity amazonNewReaderCommen = ((JSONObject)t.getAwsPostdata()).toJavaObject(AmazonNewReaderCommenEntity.class);		
		 this.messageDataToBytes(amazonNewReaderCommen);  
		return null;
	}
	
	
	private void messageDataToBytes(AmazonNewReaderCommenEntity yy) {
		String readerCode = yy.getReaderCode();  //reader地址码
		String legAddress = yy.getLedAddress();  //标签地址
		int led1 = yy.getLed1();
		int led2 = yy.getLed2();
		int led3 = yy.getLed3();
		int led4 = yy.getLed4();
		int led1State = yy.getLed1State();
		int led2State = yy.getLed2State();
		int led3State = yy.getLed3State();
		int led4State = yy.getLed4State();
		int hour = yy.getHour();
		int minute = yy.getMinute();
		int second = yy.getSecond();
		int millisecond = yy.getMillisecond();
//		StringBuffer sbLed = new StringBuffer();
//		StringBuffer control = new StringBuffer();
		byte led = T.getLedByte(led1, led2, led3, led4);
		byte controlLed = T.getStateByte(led1State, led2State, led3State, led4State);
		byte[] sendData = new byte[6];   
		byte[] dest = new byte[3];   //接受主机编号的临时byte数组
		ByteUtil.hexStringToBytes(legAddress,sendData,0);
		sendData[4] =led;
		sendData[5] =controlLed;
		byte[] timeStamp = new byte[4];
		StringBuffer time = new StringBuffer();
		String hourByte = T.getHourByte(hour);
		String minuteByte = T.getMinuteByte(minute);
		String secondByte = T.getSecondByte(second);
		String millisecondByte = T.getMillisecond(millisecond);	
		time.append(hourByte).append(minuteByte).append(secondByte).append(millisecondByte).toString();
		timeStamp[0] = Integer.valueOf(time.toString().substring(0, 8),2).byteValue();
		timeStamp[1] = Integer.valueOf(time.toString().substring(8, 16),2).byteValue();
		timeStamp[2] = Integer.valueOf(time.toString().substring(16, 24),2).byteValue();
		timeStamp[3] = Integer.valueOf(time.toString().substring(32-8),2).byteValue();
		byte[] datas = amazonNewReaderOSMessageFactory.createAmazonNewReaderOSMessage(ByteUtil.hexStringToBytes(readerCode,dest,0), command, sendData,timeStamp);
		ByteBuf bs  = Unpooled.copiedBuffer(datas);
		NewReaderResponseHandleContext.channels.writeAndFlush(bs);
		logger.info("AmazonNewReader 发送[LED 控制，针对单独标签]命令成功 message={}",ByteUtil.byteArrToHexString(datas));
	}
}
