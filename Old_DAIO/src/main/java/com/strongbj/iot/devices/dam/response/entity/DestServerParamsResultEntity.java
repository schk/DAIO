package com.strongbj.iot.devices.dam.response.entity;

public class DestServerParamsResultEntity {
	private String destIp;
	private short destPort;
	public String getDestIp() {
		return destIp;
	}
	public void setDestIp(String destIp) {
		this.destIp = destIp;
	}
	public short getDestPort() {
		return destPort;
	}
	public void setDestPort(short destPort) {
		this.destPort = destPort;
	}
	
	
}
