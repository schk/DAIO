package com.strongbj.iot.devices.guis.respnose.entity;

/**
 * 设备在线所传输的实体对象
 * @author yuzhantao
 *
 */
public class DeviceOnlineEntity {
	private long id;
	private String ip;
	private String deviceCode;
	private int type;
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public String getDeviceCode() {
		return deviceCode;
	}
	public void setDeviceCode(String deviceCode) {
		this.deviceCode = deviceCode;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
}
