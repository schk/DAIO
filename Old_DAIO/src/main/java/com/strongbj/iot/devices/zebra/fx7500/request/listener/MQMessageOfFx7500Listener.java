package com.strongbj.iot.devices.zebra.fx7500.request.listener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.listener.adapter.MessageListenerAdapter;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.google.gson.reflect.TypeToken;
import com.strongbj.core.message.MessageHandleContext;
import com.strongbj.iot.devices.hr.message.MQMessageOfHR;
import com.strongbj.iot.devices.zebra.fx7500.request.handle.BuzzerHandle;

/**
 * 从MQ接收门禁信息的监听类
 * @author yuzhantao
 *
 */
@Component
public class MQMessageOfFx7500Listener  extends MessageListenerAdapter{
	private static Logger logger = LogManager.getLogger(MQMessageOfFx7500Listener.class.getName());
	// 通过策略模式处理不同的信息
	private MessageHandleContext<MQMessageOfHR,Object> messageHandleContent;
	

	public MQMessageOfFx7500Listener(){
		super();
		
		this.messageHandleContent=new MessageHandleContext<>();
		// 添加消息处理类
		this.messageHandleContent.addHandleClass(new BuzzerHandle());
	}
	
	@JmsListener(destination="ServiceToZebraFx7500",concurrency="1")
    public void onMessage(Message message, Session session) throws JMSException {
		if(message instanceof TextMessage){
			TextMessage tm = (TextMessage)message;
			String json = tm.getText();
			MQMessageOfHR rm = JSON.parseObject(json,new TypeToken<MQMessageOfHR>(){}.getType());
			this.messageHandleContent.handle(null,rm);
		}else{
			logger.info("无法解析的mq对象消息:"+message.getClass().getName());
		}
    }
}
