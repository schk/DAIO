package com.strongbj.iot.devices.amazonreader.message;

import io.netty.channel.ChannelHandler;

import com.strongbj.core.message.IMessageHandleFactory;
import com.strongbj.iot.devices.amazonreader.response.coder.ReaderDecoder;
import com.strongbj.iot.devices.amazonreader.response.handle.ReaderResponseHandleContext;

/**
 * 从点亮标签设备接收到的消息工厂
 * @author yuzhantao
 *
 */
public class ReaderMessageHandleFactory implements IMessageHandleFactory {

	@Override
	public ChannelHandler createMessageDecoder() {
		return new ReaderDecoder(1024);
	}

	@Override
	public ChannelHandler createMessageHandle() {
		return new ReaderResponseHandleContext();
	}

}
