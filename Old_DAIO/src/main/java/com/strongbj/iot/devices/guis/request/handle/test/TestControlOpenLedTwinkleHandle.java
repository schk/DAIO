package com.strongbj.iot.devices.guis.request.handle.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.core.util.ContextUtils;
import com.strongbj.iot.devices.guis.message.GUISOSMessageFactory;
import com.strongbj.iot.devices.guis.message.MQMessageOfGUIS;
import com.strongbj.iot.devices.guis.request.entity.LightCommandEntity;
import com.strongbj.iot.devices.guis.request.entity.LightCommandEntity.LightCommand;
import com.strongbj.iot.devices.guis.request.entity.ReturnUInfoEntity;
import com.strongbj.iot.devices.guis.respnose.handle.GUISResponseHandleContext;
import com.strongbj.iot.devices.guis.respnose.message.GUISMQMessage;
import com.strongbj.iot.mq.producer.TopicSender;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
/**
 * 	上位机控制打开LED灯闪烁  Handle   测试类   模拟接受京东下发，并自动返回结果
 * @author 25969
 *
 */
public class TestControlOpenLedTwinkleHandle implements IMessageHandle<MQMessageOfGUIS,Object>{
	private static Logger logger = LogManager.getLogger(TestControlOpenLedTwinkleHandle.class.getName());
	private final static String ACTION_CODE = "reader005";   //MQ中判断消息类型的标识符
	private final static byte command = (byte)0x23;
	private GUISOSMessageFactory gUISOSMessageFactory = new GUISOSMessageFactory();
	private TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");
	@Override
	public boolean isHandle(MQMessageOfGUIS t) {
		if(t.getActioncode().equals(ACTION_CODE)){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, MQMessageOfGUIS t) {
		logger.info("GUIS 收到MQ发来的“向机柜发送亮灭灯的指令” json={}",JSON.toJSONString(t));
		LightCommandEntity lightCommand = ((JSONObject)t.getAwsPostdata()).toJavaObject(LightCommandEntity.class);		
		 this.messageDataToBytes(lightCommand);  
		return null;
	}
	/**
	 * 自动返回结果给京东
	 * @param devAddrCode 
	 */
    private void returnResultToJd(String devAddrCode) {
	   ReturnUInfoEntity devs = new ReturnUInfoEntity();
	   devs.setDevAddrCode(devAddrCode);
	   devs.setSuccess(true);
	   GUISMQMessage mes = new GUISMQMessage();
	   mes.setActioncode("reader605");
	  // mes.setGUID(String.valueOf(new Date().getTime()));
	//   mes.setRfidtype("smarrack");
	   mes.setAwsPostdata(devs);
		   
	  String json = JSON.toJSONString(mes);
	  logger.info("GUIS 返回结果给京东 json={}",json);
	  topicSender.send("daioReader", json);
		
	}

/**
 * 解析并下发数据给下位机
 * @param lightCommand
 */
	private void messageDataToBytes(LightCommandEntity lightCommand) {
		for(LightCommandEntity.DevAddr devAddr : lightCommand.getDevAddrList()) {
			String devAddrCode = devAddr.getDevAddrCode();   //主机编号
			byte[] lightComByte = new byte[devAddr.getLightCommandList().size()*4];   //装数据的byte数组
			int i=0;  //指针
			for(LightCommand lightCom : devAddr.getLightCommandList()) {
				int onTime = lightCom.getOnTime();
				int offTime = lightCom.getOffTime();
				int loopCount = lightCom.getLoopCount();
				int u = lightCom.getU();
				lightComByte[i] =(byte)u;   
				lightComByte[i+1] =(byte)onTime;
				lightComByte[i+2] =(byte)offTime;
				lightComByte[i+3] =(byte)loopCount;
				i+=4;
			}
			
			byte[] dest = new byte[3];   //接受主机编号的临时byte数组
			byte[] datas = gUISOSMessageFactory.createGUISOSMessage(ByteUtil.hexStringToBytes(devAddrCode,dest,0), command, lightComByte);
			//打印
			System.out.println("ByteUtil.byteArrToHexString(datas)  " + ByteUtil.byteArrToHexString(datas));
			ByteBuf bs  = Unpooled.copiedBuffer(datas);
			GUISResponseHandleContext.channels.writeAndFlush(bs);
			logger.info("GUIS 发送上位机控制主机"+devAddrCode+"打开LED灯闪烁命令成功 message={}",ByteUtil.byteArrToHexString(datas));
			 //修改5秒，自动返回成功或失败
			 this.returnResultToJd(devAddrCode);
		}
		 
	}

}
