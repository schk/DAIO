package com.strongbj.iot.devices.amazonNewReader.request.entity;

/**
 * 公共类
 * @author 25969
 *
 */
public class T {
/**
 * 将 byte 转换为一个长度为8的byte数组，数组每个值代表bit
 * @param b
 * @return
 */
	public static byte[] getBooleanArray(byte b) {
	   byte [] array = new byte[8];
		for(int i=7;i>=0;i--) {
			array[i] = (byte)(b & 1);
			b = (byte) (b >> 1);
		}
		return array;
		
	}
	
	/**
	 * 把byte 转换为字符串的bit
	 * @param b
	 * @return
	 */
	public static String byteToBit(byte by) {
		StringBuffer sb = new StringBuffer();
		sb.append((by>>7)&0x1)
		.append((by>>6)&0x1)
		.append((by>>5)&0x1)
		.append((by>>4)&0x1)
		.append((by>>3)&0x1)
		.append((by>>2)&0x1)
		.append((by>>1)&0x1)
		.append((by>>0)&0x1);
		return sb.toString();
	}
	
	
	 public static void main(String args[]) {
		 String str = "1234567890111";
		 int n = 4;
		 System.out.println(str.substring(str.length()-n));
		/* System.out.println(Integer.toBinaryString(2));
		 System.out.println(Integer.toBinaryString(1));
		 System.out.println(Integer.toBinaryString(0)); 
		 StringBuffer sbLed = new StringBuffer();
		 System.out.println(sbLed.append(Integer.toBinaryString(1))
					.append(Integer.toBinaryString(0))
					.append(Integer.toBinaryString(1))
					.append(Integer.toBinaryString(0)).toString());
			byte led = Integer.valueOf(sbLed.toString(),2).byteValue();
			System.out.println(led);*/
	 }
	
	 
	 /**
	  * 转换为 Led 字节 
	  */
	 public static byte getLedByte(int led1,int led2,int led3,int led4)
	 {
		 StringBuffer sbLed = new StringBuffer();
		 sbLed.append(Integer.toBinaryString(led4))
			.append(Integer.toBinaryString(led3))
			.append(Integer.toBinaryString(led2))
			.append(Integer.toBinaryString(led1)).toString();
		 byte led = Integer.valueOf(sbLed.toString(),2).byteValue();
		return led;
		 
	 }
	 
	 
	 /**
	  * 转换为控制（state）字节
	  */
	 
	 public static byte getStateByte(int led1State,int led2State,int led3State,int led4State)
	 {
		 StringBuffer sbLed = new StringBuffer();
		 sbLed.append(Integer.toBinaryString(led4State));
	     addZero(sbLed,led3State);
	     addZero(sbLed,led2State);
	     addZero(sbLed,led1State);
		 byte led = Integer.valueOf(sbLed.toString(),2).byteValue();
		return led;
		 
	 }

	 /**
	  * 当 参数param等于1或0时，前面补0
	  */
	public static void addZero(StringBuffer sbLed, int param) {
	    if(param==1 || param==0) {
	    	sbLed.append(0).append(Integer.toBinaryString(param));	
	    }else {
	    	sbLed.append(Integer.toBinaryString(param));	
	    }
		
	}
	 
	
	
	/**
	 * 获取  小时 二进制字符串
	 */
	 public static String getHourByte(int hour)
	 {
		 StringBuffer sbLed = new StringBuffer();
		 String result = Integer.toBinaryString(hour);
         addZeroForTime(result,"hour",sbLed);
		return sbLed.toString();
	 }

	 
	 /**
	  * 获取 分钟
	  */
	 public static String getMinuteByte(int minute)
	 {
		 StringBuffer sbLed = new StringBuffer();
		 String result = Integer.toBinaryString(minute);
         addZeroForTime(result,"minute",sbLed);
         return sbLed.toString();
	 }
	 
	 
	 /**
	  * 获取 秒
	  */
	 public static String getSecondByte(int second)
	 {
		 StringBuffer sbLed = new StringBuffer();
		 String result = Integer.toBinaryString(second);
         addZeroForTime(result,"second",sbLed);
         return sbLed.toString();
	 }
	 
	 /**
	  * 获取毫秒
	  */
	 public static String getMillisecond(int millisecond)
	 {
		 StringBuffer sbLed = new StringBuffer();
		 String result = Integer.toBinaryString(millisecond);
         addZeroForTime(result,"millisecond",sbLed);
         return sbLed.toString();
		 
	 }
	 
	 
	 
	 /**
	  * 时间戳解析    不够位数的前面补0
	  * @param result
	 * @param sbLed 
	  */
	private static void addZeroForTime(String result,String style, StringBuffer sbLed) {
		int length = result.length();
		switch(style) {
		case "hour":
			if(length<4) {
				sbLed = getDeviations(sbLed,length,4);
				sbLed.append(result);
			}else {
				sbLed.append(result);
			}
			break;
		case "minute":
         if(length<6) {
     		sbLed = getDeviations(sbLed,length,6);
			sbLed.append(result);
			}else {
				sbLed.append(result);
			}
			break;
		case "second":
			 if(length<6) {
					sbLed = getDeviations(sbLed,length,6);
					sbLed.append(result);
				}else {
					sbLed.append(result);
				}
			break;
		case "millisecond":
			 if(length<16) {
					sbLed = getDeviations(sbLed,length,16);
					sbLed.append(result);	
				}else {
					sbLed.append(result);
				}
			break;	
			
		}
	}
	
	
	
	public static StringBuffer getDeviations(StringBuffer sbLed,int length,int minuend) {
		int deviations = minuend-length;
		for(int i=0;i<deviations;i++) {
			sbLed.append(0);
		}
		return sbLed;
		
	}
	
}
 