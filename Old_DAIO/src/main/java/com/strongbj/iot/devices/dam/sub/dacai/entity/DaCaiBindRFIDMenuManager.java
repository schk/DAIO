package com.strongbj.iot.devices.dam.sub.dacai.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 大彩绑定RFID菜单的管理
 * 
 * @author yuzhantao
 *
 */
public class DaCaiBindRFIDMenuManager {
	private Map<String, MenuContext> menuMap = new HashMap<>();
	private static DaCaiBindRFIDMenuManager instance;

	public static DaCaiBindRFIDMenuManager getInstance() {
		if (instance == null) {
			instance = new DaCaiBindRFIDMenuManager();
		}
		return instance;
	}

	private DaCaiBindRFIDMenuManager() {
	}

	public void addMenu(String deviceCode, List<MenuItem> menuItemList) {
		MenuContext menu = new MenuContext();
		menu.setMenuItemList(menuItemList);
		this.menuMap.put(deviceCode, menu);
	}

	public MenuContext getMenu(String deviceCode) {
		if (this.menuMap.containsKey(deviceCode)) {
			return this.menuMap.get(deviceCode);
		} else {
			return null;
		}
	}

	public void removeMenu(String deviceCode) {
		if (this.menuMap.containsKey(deviceCode)) {
			this.menuMap.remove(deviceCode);
		}
	}

	public class MenuContext {
		private int selectIndex;
		private List<MenuItem> menuItemList;

		public int getSelectIndex() {
			return selectIndex;
		}

		public void setSelectIndex(int selectIndex) {
			this.selectIndex = selectIndex;
		}

		public List<MenuItem> getMenuItemList() {
			return menuItemList;
		}

		public void setMenuItemList(List<MenuItem> menuItemList) {
			this.menuItemList = menuItemList;
		}
	}

	public class MenuItem {
		private String assetId;
		private String assetName;
		private int startU;

		public String getAssetId() {
			return assetId;
		}

		public void setAssetId(String assetId) {
			this.assetId = assetId;
		}

		public String getAssetName() {
			return assetName;
		}

		public void setAssetName(String assetName) {
			this.assetName = assetName;
		}

		public int getStartU() {
			return startU;
		}

		public void setStartU(int startU) {
			this.startU = startU;
		}

	}
}
