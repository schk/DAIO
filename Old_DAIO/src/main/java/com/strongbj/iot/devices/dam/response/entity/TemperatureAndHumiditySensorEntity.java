package com.strongbj.iot.devices.dam.response.entity;

/**
 * 温湿度传感器
 * @author yuzhantao
 *
 */
public class TemperatureAndHumiditySensorEntity extends SensorEntity {
	/**
	 * 湿度
	 */
	private short humidity;
	/**
	 * 温度
	 */
	private short temperature;

	public short getTemperature() {
		return temperature;
	}

	public void setTemperature(short temperature) {
		this.temperature = temperature;
	}
	public short getHumidity() {
		return humidity;
	}

	public void setHumidity(short humidity) {
		this.humidity = humidity;
	}
}
