package com.strongbj.iot.devices.dam.response.entity;
/**
 * 传感器
 * @author yuzhantao
 *
 */
public class SensorEntity {
	/**
	 * 传感器所在位置
	 */
	private byte potion;
	/**
	 * 传感器类型
	 */
	private byte sensorType;
	
	public byte getPotion() {
		return potion;
	}
	public void setPotion(byte potion) {
		this.potion = potion;
	}
	public byte getSensorType() {
		return sensorType;
	}
	public void setSensorType(byte sensorType) {
		this.sensorType = sensorType;
	}
	
}
