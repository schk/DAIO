package com.strongbj.iot.devices.zebra.fx7500.request.handle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSONObject;
import com.mot.rfid.api3.GPO_PORT_STATE;
import com.mot.rfid.api3.RFIDReader;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.iot.devices.hr.message.MQMessageOfHR;
import com.strongbj.iot.devices.hr.request.entity.BuzzerEntity;
import com.strongbj.iot.devices.zebra.fx7500.service.Fx7500Client;
import com.strongbj.iot.devices.zebra.fx7500.service.Fx7500ClientService;

import io.netty.channel.ChannelHandlerContext;

/**
 * 蜂鸣器命令的处理
 * 
 * @author yuzhantao
 *
 */
public class BuzzerHandle implements IMessageHandle<MQMessageOfHR, Object> {
	private static Logger logger = LogManager.getLogger(BuzzerHandle.class.getName());
	private final static String ACTION_CODE = "collect021";
	@Override
	public boolean isHandle(MQMessageOfHR t) {
		if (t.getActioncode().equals(ACTION_CODE)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, MQMessageOfHR t) {
		BuzzerEntity buzzer = ((JSONObject) t.getAwsPostdata()).toJavaObject(BuzzerEntity.class);
		String ip=buzzer.getDevIp();
		Fx7500Client client = Fx7500ClientService.getClient(ip);
		RFIDReader reader = client.getMyReader();
		try {
			reader.Config.GPO.setPortState( 1, GPO_PORT_STATE.TRUE);
			reader.Config.GPO.setPortState( 2, GPO_PORT_STATE.TRUE);
			reader.Config.GPO.setPortState( 3, GPO_PORT_STATE.TRUE);
			Thread.sleep(Math.min(60000,buzzer.getAlarmTime()*100));
			reader.Config.GPO.setPortState( 1, GPO_PORT_STATE.FALSE);
			reader.Config.GPO.setPortState( 2, GPO_PORT_STATE.FALSE);
			reader.Config.GPO.setPortState( 3, GPO_PORT_STATE.FALSE);
		} catch (Exception e) {
			logger.error("Fx7500启动报警蜂鸣、灯带信息失败({})", e.getMessage());
		}
		return null;
	}
}
