package com.strongbj.iot.devices.amazonNewReader.request.entity;

import com.strongbj.core.annotation.NotProguard;
/**
 * 接受MQ JSON 的 公共类
 * @author 25969
 *
 */
@NotProguard
public class AmazonNewReaderCommenEntity {

	private String readerCode;
	
	private String readerNewCode;
	
	private String ledAddress;
	
	private int led1;
	
	private int led2;
	
	private int led3;
	
	private int led4;
	
	private int led1State;
	
	private int led2State;
	
	private int led3State;
	
	private int led4State;
	
	private int testBag;
	
	private int hour;
	
	private int minute;
	
	private int second;
	
	private int millisecond;
	
	private String shortAddress;
	
	private int legNum;
	
    private String nativeIPAddress;         //本机IP地址
	
	private String subnetMask;              //子网掩码
	
	private String gateway;                 //网关
	
	private int localPort;                  //本机端口
	
	private String targetIaddressIp;        //目标IP
	
	private int targetPort;                 //目标端口

	public String getReaderCode() {
		return readerCode;
	}

	public void setReaderCode(String readerCode) {
		this.readerCode = readerCode;
	}

	public String getLedAddress() {
		return ledAddress;
	}

	public void setLedAddress(String ledAddress) {
		this.ledAddress = ledAddress;
	}

	public int getLed1() {
		return led1;
	}

	public void setLed1(int led1) {
		this.led1 = led1;
	}

	public int getLed2() {
		return led2;
	}

	public void setLed2(int led2) {
		this.led2 = led2;
	}

	public int getLed3() {
		return led3;
	}

	public void setLed3(int led3) {
		this.led3 = led3;
	}

	public int getLed4() {
		return led4;
	}

	public void setLed4(int led4) {
		this.led4 = led4;
	}

	public int getLed1State() {
		return led1State;
	}

	public void setLed1State(int led1State) {
		this.led1State = led1State;
	}

	public int getLed2State() {
		return led2State;
	}

	public void setLed2State(int led2State) {
		this.led2State = led2State;
	}

	public int getLed3State() {
		return led3State;
	}

	public void setLed3State(int led3State) {
		this.led3State = led3State;
	}

	public int getLed4State() {
		return led4State;
	}

	public void setLed4State(int led4State) {
		this.led4State = led4State;
	}

	public int getTestBag() {
		return testBag;
	}

	public void setTestBag(int testBag) {
		this.testBag = testBag;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second;
	}

	public int getMillisecond() {
		return millisecond;
	}

	public void setMillisecond(int millisecond) {
		this.millisecond = millisecond;
	}

	public String getShortAddress() {
		return shortAddress;
	}

	public void setShortAddress(String shortAddress) {
		this.shortAddress = shortAddress;
	}

	public int getLegNum() {
		return legNum;
	}

	public void setLegNum(int legNum) {
		this.legNum = legNum;
	}

	public String getReaderNewCode() {
		return readerNewCode;
	}

	public void setReaderNewCode(String readerNewCode) {
		this.readerNewCode = readerNewCode;
	}

	public String getNativeIPAddress() {
		return nativeIPAddress;
	}

	public void setNativeIPAddress(String nativeIPAddress) {
		this.nativeIPAddress = nativeIPAddress;
	}

	public String getSubnetMask() {
		return subnetMask;
	}

	public void setSubnetMask(String subnetMask) {
		this.subnetMask = subnetMask;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public int getLocalPort() {
		return localPort;
	}

	public void setLocalPort(int localPort) {
		this.localPort = localPort;
	}

	public String getTargetIaddressIp() {
		return targetIaddressIp;
	}

	public void setTargetIaddressIp(String targetIaddressIp) {
		this.targetIaddressIp = targetIaddressIp;
	}

	public int getTargetPort() {
		return targetPort;
	}

	public void setTargetPort(int targetPort) {
		this.targetPort = targetPort;
	}

	
	
	
	
}
