package com.strongbj.iot.devices.dam.response.entity;
/**
 * 机柜数据
 * @author yuzhantao
 *
 */
public class CabinetEntity {
	/**
	 * 门状态，1为打开，0为关闭
	 */
	private byte doorState;

	public byte getDoorState() {
		return doorState;
	}

	public void setDoorState(byte doorState) {
		this.doorState = doorState;
	}
	
	
}
