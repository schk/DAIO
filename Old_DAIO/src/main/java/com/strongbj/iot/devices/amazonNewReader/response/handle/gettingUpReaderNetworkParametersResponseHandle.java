package com.strongbj.iot.devices.amazonNewReader.response.handle;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.amazonNewReader.message.NewReaderMessage;
import com.strongbj.iot.devices.guis.respnose.common.RedirectOutputStream;

import io.netty.channel.ChannelHandlerContext;
/**
 * 获取读写器网络参数 
 * @author 25969
 *
 */
public class gettingUpReaderNetworkParametersResponseHandle implements IMessageHandle<NewReaderMessage,Object>{
	private static Logger logger = LogManager.getLogger(gettingUpReaderNetworkParametersResponseHandle.class.getName());
	private static SimpleDateFormat df=null;
	@Override
	public boolean isHandle(NewReaderMessage t) {
		if(Integer.valueOf("13", 16).byteValue()==t.getType()){
			return true;
		}else{
			return false;
		}

	}

	@Override
	public Object handle(ChannelHandlerContext ctx, NewReaderMessage t) {
		String hostNumber =  ByteUtil.byteArrToHexString(t.getAddress());   //reader 地址码
		 String timeStamp  =  ByteUtil.byteArrToHexString(t.getTime());   //时间戳
		// 4 字节 IP 地址 + 4 字节子网掩码 + 4 字节网关地址 +
	    //4 字节 目标 IP 地址 + 2 字节目标端口 + 保留字段。
	   int nativeIPAddress = ByteUtil.bytesToInt(t.getBody(),0); //本机IP地址
      int subnetMask = ByteUtil.bytesToInt(t.getBody(), 4);//本机掩码
      int gateway = ByteUtil.bytesToInt(t.getBody(), 8);  //本机网关
      int targetIaddressIp = ByteUtil.bytesToInt(t.getBody(), 12);//目标IP地址
      int targetPort = ByteUtil.byteArrToShort(t.getBody(), 14);//目标端口号
      String log="收到[获取读写器网络参数 ]返回报文: reader 地址码-"+hostNumber+
                "/IP 地址"+ByteUtil.intToIp(nativeIPAddress)+
                "/子网掩码"+ByteUtil.intToIp(subnetMask)+
                "/网关地址"+ByteUtil.intToIp(gateway)+
                "/目标 IP 地址"+ByteUtil.intToIp(targetIaddressIp)+
                "/目标端口号"+String.valueOf(targetPort)+
      		    "/时间戳-"+timeStamp;
      logger.info(log);
      df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
	  RedirectOutputStream.put(df.format(new Date())+ log);
      return null; 
	}

}
