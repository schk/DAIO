package com.strongbj.iot.devices.dam.response.entity;

public class DefaultResultEntity {
	private boolean isSuccess;

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	
}
