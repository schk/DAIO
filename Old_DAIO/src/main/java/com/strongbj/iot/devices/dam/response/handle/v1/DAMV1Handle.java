package com.strongbj.iot.devices.dam.response.handle.v1;

import com.strongbj.core.message.IMessageHandle;
import com.strongbj.iot.devices.dam.message.DAMMessage;

/**
 * DAM设备版本1的处理类
 * @author yuzhantao
 *
 */
public abstract class DAMV1Handle implements IMessageHandle<DAMMessage,Object>{
	/**
	 * 是否处理DAM消息
	 * @param t
	 * @return
	 */
	protected abstract boolean isHandleDAMMessage(DAMMessage t);
	
	@Override
	public boolean isHandle(DAMMessage t) {
		if(t.getVersion()!=1 || !this.isHandleDAMMessage(t)){
			return false;
		}else{
			return true;
		}
	}
}
