package com.strongbj.iot.devices.reader.response.entity;

public class CallLabelEntity {
	/**
	 * 标签编号
	 */
	private String labelId;
	/**
	 * 呼叫状态
	 */
	private ExecutionState callState;
	
	public String getLabelId() {
		return labelId;
	}
	public void setLabelId(String labelId) {
		this.labelId = labelId;
	}
	public ExecutionState getCallState() {
		return callState;
	}
	public void setCallState(ExecutionState callState) {
		this.callState = callState;
	}
}
