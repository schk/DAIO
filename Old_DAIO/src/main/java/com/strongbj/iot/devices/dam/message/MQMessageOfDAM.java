package com.strongbj.iot.devices.dam.message;

import com.alibaba.fastjson.annotation.JSONField;

public class MQMessageOfDAM {
	private String actioncode;
	@JSONField(name="postdata")
	private Object awsPostdata;
	private long timestamp=System.currentTimeMillis();
	private String devType;
	
	public String getActioncode() {
		return actioncode;
	}
	public void setActioncode(String actioncode) {
		this.actioncode = actioncode;
	}
	public Object getAwsPostdata() {
		return awsPostdata;
	}
	public void setAwsPostdata(Object awsPostdata) {
		this.awsPostdata = awsPostdata;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public String getDevType() {
		return devType;
	}
	public void setDevType(String devType) {
		this.devType = devType;
	}
}
