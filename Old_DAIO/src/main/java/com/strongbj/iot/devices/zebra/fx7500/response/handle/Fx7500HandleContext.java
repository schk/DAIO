package com.strongbj.iot.devices.zebra.fx7500.response.handle;

import java.util.ArrayList;
import java.util.List;

import com.mot.rfid.api3.RfidStatusEvents;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.iot.devices.zebra.fx7500.response.entity.TagCache;
import com.strongbj.iot.devices.zebra.fx7500.service.Fx7500Client;

import io.netty.channel.ChannelHandlerContext;

/**
 * Fx7500设备处理策略上下文
 * @author yuzhantao
 *
 */
public class Fx7500HandleContext implements IMessageHandle<RfidStatusEvents,Object> {
	List<IMessageHandle<RfidStatusEvents,Object>> handleList = new ArrayList<IMessageHandle<RfidStatusEvents, Object>>();
	
	public Fx7500HandleContext(Fx7500Client client,String hostIp,String direction,TagCache tagCache){
		handleList.add(new InfraredHandle(tagCache,hostIp,direction));
		handleList.add(new DisconnectionHandle(client));
//		handleList.add(new InventoryStartHandle(tagCache));
//		handleList.add(new InventoryStopHandle(tagCache));
	}
	
	@Override
	public boolean isHandle(RfidStatusEvents t) {
		for(int i = 0;i<handleList.size();i++){
			if(handleList.get(i).isHandle(t)){
				return true;
			}
		}
		return false;
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, RfidStatusEvents t) {
		for(int i = 0;i<handleList.size();i++){
			if(handleList.get(i).isHandle(t)){
				return handleList.get(i).handle(ctx, t);
			}
		}
		return null;
	}

}