package com.strongbj.iot.devices.ru.response.handle;

import io.netty.channel.ChannelHandlerContext;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.core.util.ContextUtils;
import com.strongbj.iot.mq.producer.TopicSender;
import com.strongbj.iot.devices.ru.message.RUMessage;
import com.strongbj.iot.devices.ru.response.entity.DoorInfoEntity;
import com.strongbj.iot.devices.ru.response.message.RUMQMessage;
/**
 * 机柜门开关状态的处理
 * @author yuzhantao
 *
 */
public class CabinetDoorStateHandle implements IMessageHandle<RUMessage,Object> {
	private static Logger logger = LogManager.getLogger(CabinetDoorStateHandle.class.getName());
	private TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");

	@Override
	public boolean isHandle(RUMessage t) {
		if(t.getType()==6){
			return true;
		}else{
			return false;
		}
	}
static int index=0;
	@Override
	public Object handle(ChannelHandlerContext ctx, RUMessage t) {
		String json = this.doorInfoUpload(t);
		logger.info((++index)+json);
		topicSender.send("RU", json);
		return null;
	}

	/**
	 * 开关门信息上传
	 * @param message
	 * @return
	 */
	private String doorInfoUpload(RUMessage message) {
		
		String address = ByteUtil.byteArrToHexString(message.getAddress());
		DoorInfoEntity entity = new DoorInfoEntity();
		entity.setDevAddrCode(address);
		entity.setCdoorstate(String.valueOf(ByteUtil.byteToInt(message.getBody()[0])));
		RUMQMessage mes = new RUMQMessage();
		mes.setActioncode("reader008");
		mes.setGUID(String.valueOf(new Date().getTime()));
		mes.setRfidtype("RU1000");
		mes.setAwsPostdata(entity);
		return JSON.toJSONString(mes); 
	}
}
