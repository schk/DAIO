package com.strongbj.iot.devices.amazonNewReader.message;

public class MQMessageOfAmazonNewReader {

	private String actioncode;
	private Object awsPostdata;
	
	public String getActioncode() {
		return actioncode;
	}
	public void setActioncode(String actioncode) {
		this.actioncode = actioncode;
	}
	public Object getAwsPostdata() {
		return awsPostdata;
	}
	public void setAwsPostdata(Object awsPostdata) {
		this.awsPostdata = awsPostdata;
	}

	
}
