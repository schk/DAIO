package com.strongbj.iot.devices.amazondb.message;

import com.strongbj.core.util.Aes;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.core.util.CRC16;

public class ReaderMessageFactory {
	private final static String BODY_KEY = "1234567890123456"; // 数据区秘钥
	
	/**
	 * 创建基站主动上发标签的协议
	 * @param msg
	 * @return
	 * @throws Exception 
	 */
	public byte[] createReaderMessage(
			String devAddrCode,
			byte type,
			byte[] datas
			) throws Exception{
		// ，数据中心基站采用AES128-CBC-NoPaddi 加密方式（不足16 字节的数据块采用0x00 补足）
//		byte[] bsTemp;
//		int dataLen = datas.length;
//		if(dataLen < 16) {
//			bsTemp = new byte[dataLen+16-dataLen%16];
//			System.arraycopy(datas, 0, bsTemp, 0, dataLen);
//		}
//			else{
//			bsTemp=datas;
//		}
		
//		System.out.println(bsTemp.length+"==================="+ByteUtil.byteArrToHexString(bsTemp));
		
		byte[] body = Aes.Encrypt(datas, BODY_KEY);
		System.out.println(body.length+"*******************"+ByteUtil.byteArrToHexString(body));
		byte[] result = new byte[12+body.length];
		// 设置协议头为5a a5
		result[0]=0x5a;
		result[1]=(byte)165;
		// 设置设备编号
		String[] devCode = devAddrCode.split("\\.");
		result[2]=Integer.valueOf(devCode[0],10).byteValue();
		result[3]=Integer.valueOf(devCode[1],10).byteValue();
		result[4]=Integer.valueOf(devCode[2],10).byteValue();
//		ByteUtil.hexStringToBytes(devAddrCode, result, 2);
		// 编码类型
		result[5]=type;
		// 数据长度
		byte[] bLen = ByteUtil.shortToByteArr((short)body.length);
		System.arraycopy(bLen, 0, result, 6, bLen.length);
		// 总帧数
		result[8]=1;
		// 帧序号
		result[9]=1;
		// 设置数据区
		System.arraycopy(body, 0, result, 10, body.length);
		// 设置CRC校验位
		byte[] crc = ByteUtil.shortToByteArr((short)CRC16.Table_Crc(result, result.length-2));
		System.arraycopy(crc, 0, result, 10+body.length, crc.length);
		return result;
	}
}
