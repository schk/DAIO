package com.strongbj.iot.devices.guis.respnose.handle;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.core.util.ContextUtils;
import com.strongbj.iot.devices.guis.message.GUISMessage;
import com.strongbj.iot.devices.guis.respnose.common.CommonSendToCustomer;
import com.strongbj.iot.devices.guis.respnose.entity.UInfoEntity;
import com.strongbj.iot.devices.guis.respnose.entity.UInfoEntity.UdevInfo;
import com.strongbj.iot.devices.guis.respnose.message.GUISMQMessage;
import com.strongbj.iot.mq.producer.TopicSender;

import io.netty.channel.ChannelHandlerContext;

/**
 * 处理下位机定时上传U位扫描后的信息的类
 * 
 * @author 25969
 *
 */
public class GUISTimeUploadUScanLaterHandle implements IMessageHandle<GUISMessage, Object> {
	private static Logger logger = LogManager.getLogger(GUISTimeUploadUScanLaterHandle.class.getName());
	protected final static String EXCEPTION_TAG_ID = "FFFFFFFF"; // 异常标签ID
	private static TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");

//	private SimpleDateFormat df = null;

	private static byte ACTION_CODE = Integer.valueOf("80", 16).byteValue();

	private UInfoEntity devs = null;

	private List<UInfoEntity.UdevInfo> list = null;

	// private List<UInfoEntity.DevAddr> devList=null;

//	private GUISMQMessage mes = null;

	private static final String TAG_NULL = "00000000";
	
	// 最大标签数量
	private final static int MAX_TAG_COUNT=54;

	@Override
	public boolean isHandle(GUISMessage t) {
		if (ACTION_CODE == t.getCommand()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, GUISMessage t) {
		try {
//			System.out.println("盘点数据16进制完整报文=" + ByteUtil.byteArrToHexString(t.getData()));
			
			// TODO 因硬件原因，软件端需要屏蔽超过最大U位数量的数据
			if ((t.getData().length/5)>MAX_TAG_COUNT) {
				logger.error("获取的标签数量超过{}个",MAX_TAG_COUNT);
				return null;
			}
			
			if ((t.getData().length) % 5 == 0) { // 因“N个标签数据，每个标签=1byte（U位）+4 byte（UID）” ，此处判断
													// 数据的长度是不是5的倍数，如果不是暂时认为时客户端传输有误
				String hostNumber = ByteUtil.byteArrToHexString(t.getHostNumber()); // 主机编号
				int iCurIdx = 0; // 指针
				devs = new UInfoEntity();
				list = new ArrayList<>();
				UInfoEntity.UdevInfo u = null;
				long dataLen = t.getData().length;
				while (iCurIdx < dataLen) {
					u = devs.new UdevInfo();
					String uPosition = ByteUtil.byteArrToHexString(t.getData(), iCurIdx, 1); // U位
					String labCode = ByteUtil.byteArrToHexString(t.getData(), (iCurIdx + 1), 4).toUpperCase(); // 标签编号
					iCurIdx += 5;
					u.setRfid(labCode.toLowerCase());
					u.setU(ByteUtil.hexStringToInt(uPosition));
					
					if(EXCEPTION_TAG_ID.equals(labCode)) { // 如果标签有FFFFFFFF的，认为扫描杆程序异常，将不处理，并且不返回信息。
						logger.info("有标签编号为[{}],因此本次标签数据不处理，源数据:{}",EXCEPTION_TAG_ID,JSON.toJSONString(t));
						return null;
					} else if (!TAG_NULL.equals(labCode)) {
						list.add(u);
					}
					u = null;
				}
				// logger.info(myStringBuilder.toString()); //打印收到的数据
				// df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
				// RedirectOutputStream.put(df.format(new Date())+"收到读卡返回报文
				// "+ByteUtil.byteArrToHexString(t.getData()));
				// System.out.println(df.format(new Date())+"收到读卡返回报文
				// "+ByteUtil.byteArrToHexString(t.getData()));
				CommonSendToCustomer.commonSendMessageToCustomer(ctx, t, (byte) 0, 1); // 上位机回复信息
				pushToMq(list, hostNumber.toLowerCase()); // 向MQ推送固定格式的消息
			} else {
				// logger.info("GUIS 12号指令返回的正文不是5的倍数！"); //根据协议，此处应该给下位机返回接受失败的应答
				CommonSendToCustomer.commonSendMessageToCustomer(ctx, t, (byte) 1, 1); // 上位机回复信息
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			devs = null;
			list.clear();
			list = null;
		}
		return null;
	}

	/**
	 * 向MQ推送固定格式的消息
	 * 
	 * @param list
	 *            U位标签集合
	 * @param hostNumber
	 *            主机名称
	 */
	private static void pushToMq(List<UdevInfo> list, String hostNumber) {
		UInfoEntity devs = new UInfoEntity();
		List<UInfoEntity.DevAddr> devList = new ArrayList<>();
		UInfoEntity.DevAddr dev = devs.new DevAddr();
		dev.setDevAddrCode(hostNumber);

		dev.setUdevInfo(list);
		devList.add(dev);
		devs.setDevAddrList(devList);

		GUISMQMessage mes = new GUISMQMessage();
		mes.setActioncode("reader003");
		mes.setRfidtype("smarrack");
		mes.setAwsPostdata(devs);
		mes.setTimeStamp(String.valueOf(System.currentTimeMillis()));
		logger.info(JSON.toJSONString(mes));
		topicSender.send("daioReader", JSON.toJSONString(mes)); // 一会放到 mes上边
		devList.clear();
		devList = null;
		mes = null;
		devs = null;
		dev = null;
		mes = null;

	}

}
