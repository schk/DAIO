package com.strongbj.iot.devices.amazonNewReader.response.handle;
/**
 * 标签绑定请求  解码
 * @author 25969
 *
 */

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.amazonNewReader.message.NewReaderMessage;
import com.strongbj.iot.devices.guis.respnose.common.RedirectOutputStream;
import com.strongbj.iot.devices.guis.respnose.handle.GUISGetHostBasicInfoHandle;

import io.netty.channel.ChannelHandlerContext;

public class LabelBindingRequestResponseHandle implements IMessageHandle<NewReaderMessage,Object>{
	private static Logger logger = LogManager.getLogger(GUISGetHostBasicInfoHandle.class.getName());
	private static SimpleDateFormat df=null;
	@Override
	public boolean isHandle(NewReaderMessage t) {
		if(Integer.valueOf("1", 16).byteValue()==t.getType()){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, NewReaderMessage t) {
		 String hostNumber =  ByteUtil.byteArrToHexString(t.getAddress());   //reader 地址码
		 String timeStamp  =  ByteUtil.byteArrToHexString(t.getTime());   //时间戳
		 //返回正文：4 字节 LED 标签地址 + 1 字节接收成功的测试包数量 + 保留字段。 
		 String ledTag = ByteUtil.byteArrToHexString(t.getBody(), 0, 4);  // LED 标签地址
         String testPack  = ByteUtil.byteArrToHexString(t.getBody(), 4, 1); //1 字节接收成功的测试包数量
         String log ="收到标签绑定请求返回报文: reader 地址码-"+hostNumber+"  LED 标签地址-"+ledTag +" 测试包数量-"+testPack+" 时间戳-"+timeStamp;
         logger.info(log);
         df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
   	     RedirectOutputStream.put(df.format(new Date())+ log);
		return null;
	}

}
