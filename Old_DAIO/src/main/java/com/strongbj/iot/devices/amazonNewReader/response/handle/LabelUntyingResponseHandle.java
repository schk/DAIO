package com.strongbj.iot.devices.amazonNewReader.response.handle;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.amazonNewReader.message.NewReaderMessage;
import com.strongbj.iot.devices.guis.respnose.common.RedirectOutputStream;

import io.netty.channel.ChannelHandlerContext;

/**
 * 标签解绑  Handle
 * @author 25969
 *
 */
public class LabelUntyingResponseHandle implements IMessageHandle<NewReaderMessage,Object>{
	private static Logger logger = LogManager.getLogger(LabelUntyingResponseHandle.class.getName());
	private static SimpleDateFormat df=null;
	@Override
	public boolean isHandle(NewReaderMessage t) {
		if(Integer.valueOf("4", 16).byteValue()==t.getType()){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, NewReaderMessage t) {
		String hostNumber =  ByteUtil.byteArrToHexString(t.getAddress());   //reader 地址码
		 String timeStamp  =  ByteUtil.byteArrToHexString(t.getTime());   //时间戳
		 //4 字节 LED 标签地址 + 1 字节解绑返回码 + 保留字段。0x01，解绑完成；0x02 解绑失败
		 String ledTag = ByteUtil.byteArrToHexString(t.getBody(), 0, 4);  // LED 标签地址
        int unboundReturnCode  = ByteUtil.bytesToUbyte(t.getBody(), 4); //解绑返回码
        String log ="收到标签解绑请求返回报文: reader 地址码-"+hostNumber+"  LED 标签地址-"+ledTag +
                "解绑返回码"+unboundReturnCode
        		+" 时间戳-"+timeStamp;
        logger.info(log);
        df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
	     RedirectOutputStream.put(df.format(new Date())+ log);
		return null;
	}

}
