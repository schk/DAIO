package com.strongbj.iot.devices.dam.sub.dacai.message;
/**
 * 大彩消息类型
 * @author yuzhantao
 *
 */
public enum DaCaiMessageType {
	/**
	 * 系统消息
	 */
	System,
	/**
	 * 自定义消息
	 */
	Self
}
