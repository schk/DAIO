package com.strongbj.iot.devices.reader.response.handle;

import io.netty.channel.ChannelHandlerContext;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.core.util.ContextUtils;
import com.strongbj.iot.mq.producer.TopicSender;
import com.strongbj.iot.devices.reader.request.message.MQMessage;
import com.strongbj.iot.devices.reader.response.entity.BaseLabelEntity;
import com.strongbj.iot.devices.reader.response.entity.IEntityFactory;
import com.strongbj.iot.devices.reader.response.entity.LabelEntityFactory;
import com.strongbj.iot.devices.reader.response.entity.RealtimeLabelInfoListOfReaderEntity;
import com.strongbj.iot.devices.reader.response.entity.RealtimeLabelInfoOfReaderEntity;
import com.strongbj.iot.devices.reader.message.ReaderMessage;

/**
 * 主动上传的标签信息的应答处理
 * @author yuzhantao
 *
 */
class ActivelyUploadedLabelInfoResponseHandle implements IMessageHandle<ReaderMessage,Object>{
	private final static String ACTION_CODE="collect002";
	private final static String MQ_TOPIC = "daioReader";
	private TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");
	private static Logger logger = LogManager.getLogger(ActivelyUploadedLabelInfoResponseHandle.class.getName());
	
	IEntityFactory<List<BaseLabelEntity>> labelFactory;
	StringBuffer stringBuffer = new StringBuffer();
	
	public ActivelyUploadedLabelInfoResponseHandle(){
		this.labelFactory = new LabelEntityFactory();
	}
	
	@Override
	public boolean isHandle(ReaderMessage t) {
		if(t.getType()==2){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, ReaderMessage t) {
		byte[] datas = t.getBody();				// 获取从Reader接收到的数据段信息
//		logger.debug("收到Reader主动上传的标签信息数据："+ByteUtil.byteArrToHexString(datas));
		List<BaseLabelEntity> labs = labelFactory.parse(datas);	// 通过简单工厂模式将byte数组转为对象
		MQMessage<RealtimeLabelInfoListOfReaderEntity> msg = this.baseLabel2MQMessage(
				ByteUtil.byteArrToHexString(t.getAddress()), labs);// 将Reader对象信息转为MQ格式的信息
		
		stringBuffer.append(JSON.toJSONString(msg));	// 转为Json字符串
		topicSender.send(MQ_TOPIC, stringBuffer.toString());		// 发送标签信息到MQ
		logger.info(stringBuffer.toString());
		stringBuffer.setLength(0);
		return null;
	}

	/**
	 * 将从Reader获取的标签信息转成MQ格式的信息
	 * @param readerCode
	 * @param labs
	 * @return
	 */
	private MQMessage<RealtimeLabelInfoListOfReaderEntity> baseLabel2MQMessage(String readerCode, List<BaseLabelEntity> labs){
		MQMessage<RealtimeLabelInfoListOfReaderEntity> msg = new MQMessage<>();
		msg.setActionCode(ACTION_CODE);
		List<RealtimeLabelInfoOfReaderEntity> list = new ArrayList<>();
		RealtimeLabelInfoOfReaderEntity rl = new RealtimeLabelInfoOfReaderEntity();
		rl.setDevAddrCode(readerCode);
		
		List<String> labList = new ArrayList<>();
		for(BaseLabelEntity lab : labs){
			labList.add(lab.getId());
		}
		rl.setDevInfo(labList);
		list.add(rl);
		RealtimeLabelInfoListOfReaderEntity labelListEntity = new RealtimeLabelInfoListOfReaderEntity();
		labelListEntity.setDevAddrList(list);
		msg.setAwsPostData(labelListEntity);
		return msg;
	}
}
