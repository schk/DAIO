package com.strongbj.iot.devices.dam.response.handle.v1;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.netty.channel.ChannelHandlerContext;

import com.alibaba.fastjson.JSON;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.core.util.ContextUtils;
import com.strongbj.iot.devices.dam.message.DAMMessage;
import com.strongbj.iot.devices.dam.message.MQMessageOfDAM;
import com.strongbj.iot.devices.dam.response.entity.SensorEntity;
import com.strongbj.iot.devices.dam.response.entity.SensorFactoryContext;
import com.strongbj.iot.devices.dam.response.entity.SensorOfAwsPostdataEntity;
import com.strongbj.iot.devices.dam.response.entity.SensorParamesEntity;
import com.strongbj.iot.devices.dam.response.entity.TemperatureAndHumiditySensorEntity;
import com.strongbj.iot.devices.dam.response.entity.TemperatureSensorEntity;
import com.strongbj.iot.mq.producer.TopicSender;
/**
 * 温湿度信息处理
 * @author yuzhantao
 *
 */
public class TemperatureAndHumidityHandle extends DAMV1Handle {
	private static Logger logger = LogManager.getLogger(TemperatureAndHumidityHandle.class.getName());
	private TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");
	private static final String TOPIC="damRealTimeInfoToService";
	private static final String ACTION_CODE = "realTimeDatas";
	private static final String DEV_TYPE="damDc";
	private final static int SENSOR_DATA_SIZE=5;	// 一个传感器在协议里的字节长度
	private SensorFactoryContext sensorFactoryContext=new SensorFactoryContext();	// 传感器工厂类
	
	@Override
	protected boolean isHandleDAMMessage(DAMMessage t) {
		if(t.getCommand()==0x32){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, DAMMessage t) {
		int dataLen = t.getDatas().length;
		List<SensorParamesEntity> sensorList = new ArrayList<>();
		for(int i=0;i<=dataLen-SENSOR_DATA_SIZE;i+=SENSOR_DATA_SIZE){
			SensorEntity sensor = sensorFactoryContext.createSensor(t.getDatas(), i);
			int sensorPosition = i/SENSOR_DATA_SIZE+1;		// 计算传感器位置
			if(sensor instanceof TemperatureSensorEntity){	// 如果是温度传感器
				SensorParamesEntity spe = new SensorParamesEntity();
				spe.setSensorPosition(sensorPosition);
				spe.setSensorType(1);
				int tempTemperature = ((TemperatureSensorEntity) sensor).getTemperature();
				// TODO 由于硬件温度有异常数据，所以过滤
				if(tempTemperature<1 || tempTemperature>100) {
//					spe.setSensorDatas(0);
					continue;
				}else {
					spe.setSensorDatas(tempTemperature);
				}
				sensorList.add(spe);
			}else if(sensor instanceof TemperatureAndHumiditySensorEntity) { // 如果是温湿度传感器
				SensorParamesEntity spe = new SensorParamesEntity();
				spe.setSensorPosition(sensorPosition);
				spe.setSensorType(1);
				int tempTemperature = ((TemperatureAndHumiditySensorEntity)sensor).getTemperature();
				// TODO 由于硬件温度有异常数据，所以过滤
				if(tempTemperature<1 || tempTemperature>100) {
//					spe.setSensorDatas(0);
					continue;
				}else {
					spe.setSensorDatas(tempTemperature);
				}
				sensorList.add(spe);
				
				SensorParamesEntity spe2 = new SensorParamesEntity();
				spe2.setSensorPosition(sensorPosition);
				spe2.setSensorType(2);
				spe2.setSensorDatas(((TemperatureAndHumiditySensorEntity)sensor).getHumidity());
				sensorList.add(spe2);
			}
			
		}
		
		MQMessageOfDAM msg = new MQMessageOfDAM();
		msg.setActioncode(ACTION_CODE);
		
		SensorOfAwsPostdataEntity sensorOfAwsPostdataEntity=new SensorOfAwsPostdataEntity();
		sensorOfAwsPostdataEntity.setSensorList(sensorList);
		sensorOfAwsPostdataEntity.setRackConverCode(ByteUtil.byteArrToHexString(t.getAddressCode()));
		msg.setAwsPostdata(sensorOfAwsPostdataEntity);
		msg.setDevType(DEV_TYPE);
		msg.setTimestamp(ByteUtil.bytesToLong(t.getSn()));
		String json = JSON.toJSONString(msg);
		
		topicSender.send(TOPIC, json);
		logger.debug("DAM设备 向MQ发送温湿度数据	 JSON={}",json);
		return null;
	}
}
