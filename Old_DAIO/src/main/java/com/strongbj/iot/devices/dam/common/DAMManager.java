package com.strongbj.iot.devices.dam.common;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * DAM设备管理
 * @author yuzhantao
 *
 */
public class DAMManager {
	private static Map<String, ITaskManager> taskMap = new ConcurrentHashMap<String, ITaskManager>();
	public static ITaskManager getTaskManager(String deviceCode) {
		if(!taskMap.containsKey(deviceCode)) {
			taskMap.put(deviceCode, new QueueTaskManager());
		}
		return taskMap.get(deviceCode);
	}
	
	public static void removeTaskManager(String deviceCode) {
		if(taskMap.containsKey(deviceCode)) {
			ITaskManager taskManager = taskMap.remove(deviceCode);
			if (taskManager != null) {
				taskManager.destory();
			}
		}
	}
}
