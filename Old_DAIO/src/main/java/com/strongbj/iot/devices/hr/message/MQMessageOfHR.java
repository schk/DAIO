package com.strongbj.iot.devices.hr.message;

import com.alibaba.fastjson.annotation.JSONField;

public class MQMessageOfHR {
	private String actioncode;
	@JSONField(name="postdata")
	private Object awsPostdata;
	

	public String getActioncode() {
		return actioncode;
	}
	public void setActioncode(String actioncode) {
		this.actioncode = actioncode;
	}
	public Object getAwsPostdata() {
		return awsPostdata;
	}
	public void setAwsPostdata(Object awsPostdata) {
		this.awsPostdata = awsPostdata;
	}
}
