package com.strongbj.iot.devices.zebra.fx7500.response;

import com.mot.rfid.api3.RfidEventsListener;
import com.mot.rfid.api3.RfidReadEvents;
import com.mot.rfid.api3.RfidStatusEvents;
import com.mot.rfid.api3.TagData;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.iot.devices.zebra.fx7500.response.entity.TagCache;
import com.strongbj.iot.devices.zebra.fx7500.response.handle.Fx7500HandleContext;
import com.strongbj.iot.devices.zebra.fx7500.service.Fx7500Client;

/**
 * Fx7500监听类
 * @author jeff
 *
 */
public class Fx7500Listener implements RfidEventsListener {
	private Fx7500Client client;
	private IMessageHandle<RfidStatusEvents,Object> messageHandle;
	private TagCache tagCache;
	
	public Fx7500Listener(Fx7500Client client,String hostIp,String direction){
		this.client=client;
		this.tagCache = new TagCache();
		this.messageHandle=new Fx7500HandleContext(client,hostIp,direction,this.tagCache);
	}
	
	/**
	 * 当有标签数据时触发此函数
	 */
	@Override
	public void eventReadNotify(RfidReadEvents rre) {
		TagData[] tags = client.getReader().Actions.getReadTags(100);
		if(tags!=null){
			this.tagCache.addTags(tags); // 将设备扫描到的标签报错的标签缓存类中
		}
//		for(TagData td : tagCache.getTags()) {
//			System.out.println("缓存中的标签：" + td.getTagID());
//		}
	}

	/**
	 * 监听fx7500相关状态
	 */
	@Override
	public void eventStatusNotify(RfidStatusEvents rse) {
//		STATUS_EVENT_TYPE statusType = rse.StatusEventData.getStatusEventType();
//		System.out.println(statusType.toString());
		this.messageHandle.handle(null, rse); // 处理获取到的状态信息
	}
}