package com.strongbj.iot.devices.dam.sub.dacai.response.entity;

/**
 * 屏幕返回的信息工厂
 * 
 * @author yuzhantao
 *
 */
public class ScreenReturnFactory {
	public static ScreenReturnEntity create(String msg) {
		if (msg.length() < 10)
			return null;
		ScreenReturnEntity sr = new ScreenReturnEntity();
		sr.setScreenId(Integer.valueOf(msg.substring(0, 4), 16).shortValue());
		sr.setControlId(Integer.valueOf(msg.substring(4, 8), 16).shortValue());
		sr.setControlType(Integer.valueOf(msg.substring(8, 10), 16).byteValue());
//		sr.setSubtype(Integer.valueOf(msg.substring(10,12),16).byteValue());
//		sr.setStatus(Integer.valueOf(msg.substring(12,14),16).byteValue());
		return sr;
	}
}
