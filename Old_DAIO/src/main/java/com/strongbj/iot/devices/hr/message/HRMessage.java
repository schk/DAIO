package com.strongbj.iot.devices.hr.message;
/**
 * HR3.15版本的消息结构体
 * @author yuzhantao
 *
 */
public class HRMessage {
	/**
	 * 用于区分各个操作的指令
	 */
	private byte command;
	/**
	 * 指令序列号(PC->读写器 0x00,读写器->PC 0x01)
	 */
	private byte sequence;
	/**
	 * 指令操作码 0xXX 协议预留字节(在 485 通讯中可当作设备地址使用)
	 */
	private byte opcode;
	/**
	 * 数据
	 */
	private byte[] data;
	
	public HRMessage(){}
	
	public HRMessage(byte command,byte sequence,byte opcode,byte[] data){
		this.command=command;
		this.sequence=sequence;
		this.opcode=opcode;
		this.data=data;
	}
	
	public byte getCommand() {
		return command;
	}
	public void setCommand(byte command) {
		this.command = command;
	}
	public byte getSequence() {
		return sequence;
	}
	public void setSequence(byte sequence) {
		this.sequence = sequence;
	}
	public byte getOpcode() {
		return opcode;
	}
	public void setOpcode(byte opcode) {
		this.opcode = opcode;
	}
	public byte[] getData() {
		return data;
	}
	public void setData(byte[] data) {
		this.data = data;
	}
	
	
}
