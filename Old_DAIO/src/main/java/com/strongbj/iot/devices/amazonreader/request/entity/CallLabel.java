package com.strongbj.iot.devices.amazonreader.request.entity;
/**
 * 点亮标签的结构体
 * @author yuzhantao
 *
 */
public class CallLabel {
	private String card;
	private String lightcolor;
	public String getCard() {
		return card;
	}
	public void setCard(String card) {
		this.card = card;
	}
	public String getLightcolor() {
		return lightcolor;
	}
	public void setLightcolor(String lightcolor) {
		this.lightcolor = lightcolor;
	}

}
