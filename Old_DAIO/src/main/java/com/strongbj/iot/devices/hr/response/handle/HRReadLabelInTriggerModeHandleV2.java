package com.strongbj.iot.devices.hr.response.handle;

import java.util.HashMap;
import java.util.Map;

import io.netty.channel.ChannelHandlerContext;

/**
 * 在触发模式下读取标签的处理2.0版
 * 此次更新内容：当标签在一次扫描中重复出现，程序在天线关闭前只向服务器发送一个相同标签。
 * @author yuzhantao
 *
 */
public class HRReadLabelInTriggerModeHandleV2 extends HRReadLabelInTriggerModeHandle {
	protected static int MAX_SEND_COUNT = 5; // 最大发送失败次数
	protected Map<String, Integer> scanedLabelMap = new HashMap<>(); // 在天线关闭前，已扫描的标签及发送次数的MAP

	public HRReadLabelInTriggerModeHandleV2(String doorDirection, Integer labelCount) {
		super(doorDirection, labelCount);
	}

	/**
	 * 当标签在一次扫描中多次被扫描到，只向mq传输一次
	 */
	@Override
	protected void labHandle(ChannelHandlerContext ctx, byte rSSI, byte antennaNum, String labCode) {
		if (!scanedLabelMap.containsKey(labCode)) {		// 如果标签没有扫描过，则记录到缓存
			scanedLabelMap.put(labCode, 0);
		} else if (scanedLabelMap.get(labCode) >= MAX_SEND_COUNT) { // 如果标签扫描超过最大次数，就过滤调
			return;
		}
		try {
			super.labHandle(ctx, rSSI, antennaNum, labCode);		// 处理或发送标签
			scanedLabelMap.put(labCode, MAX_SEND_COUNT);			// 如果标签处理发送成功，则将发送次数记录最大，禁止本次扫描重复发送
		} catch (Exception e) {
			// 当标签发送异常时，累加发送次数
			int sendCount = scanedLabelMap.get(labCode);			// 获取发送次数
			scanedLabelMap.put(labCode, Math.min(++sendCount,MAX_SEND_COUNT));// 记录标签发送次数
			e.printStackTrace();
		}
	}

	@Override
	protected void startAntennaScanHandle(ChannelHandlerContext ctx, byte triggerInfraredNum) {
		scanedLabelMap.clear(); // 清楚标签缓存
		super.startAntennaScanHandle(ctx, triggerInfraredNum);
	}

	@Override
	protected void endAllAntennaScanHandle(ChannelHandlerContext ctx, byte antennaNum) {
		scanedLabelMap.clear(); // 清楚标签缓存
		super.endAllAntennaScanHandle(ctx, antennaNum);
	}

	@Override
	protected void endCurrentAntennaScanHandle(ChannelHandlerContext ctx, byte antennaNum, int labCount) {
		// TODO Auto-generated method stub
		super.endCurrentAntennaScanHandle(ctx, antennaNum, labCount);
	}

}
