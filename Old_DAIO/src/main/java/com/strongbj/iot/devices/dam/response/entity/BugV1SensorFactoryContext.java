package com.strongbj.iot.devices.dam.response.entity;
/**
 * 因目前硬件传感器实时显示类型不对，所以暂时中行都默认只有温湿度
 * @author yuzhantao
 *
 */
public class BugV1SensorFactoryContext implements ISensorFactory {
	// 湿度传感器工厂
	private TemperatureAndHumiditySensorFactory temperatureAndHumiditySensorFactory = new TemperatureAndHumiditySensorFactory();
	@Override
	public SensorEntity createSensor(byte[] src, int offset) {
		return temperatureAndHumiditySensorFactory.createSensor(src, offset);
	}
}

