package com.strongbj.iot.devices.guis.respnose.handle;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.guis.message.GUISMessage;
import com.strongbj.iot.devices.guis.respnose.common.RedirectOutputStream;

import io.netty.channel.ChannelHandlerContext;

public class GUISGetHostBasicInfoHandle implements IMessageHandle<GUISMessage, Object> {
	private static Logger logger = LogManager.getLogger(GUISGetHostBasicInfoHandle.class.getName());
	private static SimpleDateFormat df=null;
	@Override
	public boolean isHandle(GUISMessage t) {
		if(Integer.valueOf("21", 16).byteValue()==t.getCommand()){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, GUISMessage t) {
//		   String hostNumber =  ByteUtil.byteArrToHexString(t.getHostNumber());   //主机编号
		   //下位机返回数据：3byte(硬件流水号HID)+1byte(硬件主版本)+1byte(硬件副版本)+1byte(软件主版本号)+1byte(软件副版本号)
		   String devAddrCode = ByteUtil.byteArrToHexString(t.getData(), 0, 3);  // HID
           String hardwareMainVersionNumber = ByteUtil.byteArrToHexString(t.getData(), 3, 1); //硬件主版本
           String hardwareMinorVersionNumber = ByteUtil.byteArrToHexString(t.getData(), 4, 1);//硬件副版本号
           String softwareMainVersionNumbe = ByteUtil.byteArrToHexString(t.getData(), 5, 1);  //软件主版本号
           String softwareMinorVersionNumber = ByteUtil.byteArrToHexString(t.getData(), 6, 1);//软件副版本号
		   logger.info("收到上位机获取主机基本信息 回复报文   HID："+devAddrCode+" 硬件主版本:"+hardwareMainVersionNumber+" 硬件副版本号:" +hardwareMinorVersionNumber+" 软件主版本号:"+softwareMainVersionNumbe+" 软件副版本号:"+softwareMinorVersionNumber); 
		   df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		   RedirectOutputStream.put(df.format(new Date())+" 收到上位机获取主机基本信息 回复报文 "+ ByteUtil.byteArrToHexString(t.getData()));
		   return null;
	}

}
