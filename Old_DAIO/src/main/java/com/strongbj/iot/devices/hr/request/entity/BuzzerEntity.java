package com.strongbj.iot.devices.hr.request.entity;

/**
 * 蜂鸣器
 * @author yuzhantao
 *
 */
public class BuzzerEntity {
	/**
	 * 门禁设备的IP地址
	 */
	private String devIp;
	/**
	 * 是否报警，true为报警，false为取消报警
	 */
	private boolean isAlarm;
	/**
	 * 报警时长
	 */
	private long alarmTime;
	
	public String getDevIp() {
		return devIp;
	}
	public void setDevIp(String devIp) {
		this.devIp = devIp;
	}
	public boolean isAlarm() {
		return isAlarm;
	}
	public void setAlarm(boolean isAlarm) {
		this.isAlarm = isAlarm;
	}
	public long getAlarmTime() {
		return alarmTime;
	}
	public void setAlarmTime(long alarmTime) {
		this.alarmTime = alarmTime;
	}
	
	
}
