package com.strongbj.iot.devices.zebra.fx7500.task;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.strongbj.iot.devices.zebra.fx7500.service.DoorConfig;
import com.strongbj.iot.devices.zebra.fx7500.service.Fx7500Client;
import com.strongbj.iot.devices.zebra.fx7500.service.Fx7500ClientService;

/**
 * Fx7500设备定时检测连接状态
 * @author jeff
 *
 */
@Service
public class Fx7500ClientConnectionCheck {
	private static Logger logger = LogManager.getLogger(Fx7500ClientConnectionCheck.class.getName());
	
	/**
	 * 每30秒检测一下Fx7500设备是否连接，如果没有连接，就重新实例化一个类来连接它
	 */
	@Scheduled(cron = "0/30 * * * * ?")
	public void connectionCheck(){
		List<DoorConfig> configList = Fx7500ClientService.getConfig();
		for(int i=0;i<configList.size();i++){
			Fx7500Client client = Fx7500ClientService.getClient(configList.get(i).getIp());
			if(client==null){
				logger.info("检测到Fx7500设备({})已断开，采集系统将自动连接", configList.get(i).getIp());
				Fx7500ClientService.runNewClient(configList.get(i));
			}else if(!client.isConnected){
				logger.info("检测到Fx7500设备({})已断开，采集系统将自动连接", configList.get(i).getIp());
				Fx7500ClientService.getClients().remove(configList.get(i).getIp());
				Fx7500ClientService.runNewClient(configList.get(i));
			}
		}
	}
	
}
