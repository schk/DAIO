package com.strongbj.iot.devices.dam.request.handle.dam;

/**
 * 获取传感器类型
 * 
 * @author yuzhantao
 *
 */
public class GetSensorTypeHandle extends GetParamsHandle {
	public GetSensorTypeHandle() {
		super("getSensorType", (byte) 0x17);
	}
}