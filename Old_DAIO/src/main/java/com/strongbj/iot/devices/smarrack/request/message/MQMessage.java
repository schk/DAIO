package com.strongbj.iot.devices.smarrack.request.message;

public class MQMessage<T>{
	private String actionCode;
	private T awsPostData;
	
	public String getActionCode() {
		return actionCode;
	}
	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}
	public T getAwsPostData() {
		return awsPostData;
	}
	public void setAwsPostData(T awsPostData) {
		this.awsPostData = awsPostData;
	}
}
