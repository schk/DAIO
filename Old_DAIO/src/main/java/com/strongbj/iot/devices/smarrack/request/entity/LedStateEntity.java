package com.strongbj.iot.devices.smarrack.request.entity;
/**
 * 灯的状态
 * @author yuzhantao
 *
 */
public class LedStateEntity {
	private int u;
	private byte state;
	public int getU() {
		return u;
	}
	public void setU(int u) {
		this.u = u;
	}
	public byte getState() {
		return state;
	}
	public void setState(byte state) {
		this.state = state;
	}
	
	
}
