package com.strongbj.iot.devices.dam.sub.dacai.response.entity;
/**
 * 屏幕返回的表格控件实体
 * @author yuzhantao
 *
 */
public class ScreenReturnTableControlEntity {
	/**
	 * 屏幕Id
	 */
	private short screenId;
	/**
	 * 控件id
	 */
	private short controlId;
	/**
	 * 控件类型
	 */
	private byte controlType;
	/**
	 * 选择的索引
	 */
	private short selectIndex;
	public short getScreenId() {
		return screenId;
	}
	public void setScreenId(short screenId) {
		this.screenId = screenId;
	}
	public short getControlId() {
		return controlId;
	}
	public void setControlId(short controlId) {
		this.controlId = controlId;
	}
	public byte getControlType() {
		return controlType;
	}
	public void setControlType(byte controlType) {
		this.controlType = controlType;
	}
	public short getSelectIndex() {
		return selectIndex;
	}
	public void setSelectIndex(short selectIndex) {
		this.selectIndex = selectIndex;
	}
	
	
}
