package com.strongbj.iot.devices.dam.request.entity;
/**
 * 传感器类型
 * @author yuzhantao
 *
 */
public class SensorTypeEntity extends RackCodeEntity {
	private int[] sensorTypes;

	public int[] getSensorTypes() {
		return sensorTypes;
	}

	public void setSensorTypes(int[] sensorTypes) {
		this.sensorTypes = sensorTypes;
	}
	
}
