package com.strongbj.iot.devices.reader.response.entity;

import java.util.List;

public class RealtimeLabelInfoListOfReaderEntity {
	private List<RealtimeLabelInfoOfReaderEntity> devAddrList;

	public List<RealtimeLabelInfoOfReaderEntity> getDevAddrList() {
		return devAddrList;
	}

	public void setDevAddrList(List<RealtimeLabelInfoOfReaderEntity> devAddrList) {
		this.devAddrList = devAddrList;
	}
	
	
}
