package com.strongbj.iot.devices.smarrack.response.handle;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.core.util.ContextUtils;
import com.strongbj.iot.devices.smarrack.common.DeviceChannelMap;
import com.strongbj.iot.devices.smarrack.message.SmarRackMQMessage;
import com.strongbj.iot.devices.smarrack.message.SmarRackMessage;
import com.strongbj.iot.devices.smarrack.response.entity.UInfoEntity;
import com.strongbj.iot.mq.producer.TopicSender;

import io.netty.channel.ChannelHandlerContext;
/**
 * 处理上报U位信息
 * @author yuzhantao
 *
 */
public class UploadUInfoHandle extends WithResponseHandle{
	private static Logger logger = LogManager.getLogger(UploadUInfoHandle.class.getName());
	private final static byte COMMAND_ID = (byte)0xA1;
	private final static String ACTION_CODE="reader003";
	private final static String RFID_TYPE="smarrack"; 
	private TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");
//	private RedisUtil<Object> redis = (RedisUtil<Object>) ContextUtils.getBean("redisUtil");
	
	@Override
	public boolean isHandle(SmarRackMessage t) {
		if(t.getCommand()==COMMAND_ID){
			return true;
		}else{
			return false;
		}
	}

	@Override
	void messageHandle(ChannelHandlerContext ctx, SmarRackMessage t) {
		final int LABEL_DATA_SIZE=34;	// 一个标签在数组中的长度
		byte[] datas=t.getBody();
		String deviceCode = ByteUtil.byteArrToHexString(t.getDevId());
		// int modelCount=datas[0];		// 获取模块数据
		// int uTotal = datas[1];			// u位总数量
		int uCount = Math.max(0, (datas.length-2)/LABEL_DATA_SIZE);// 读到的标签数量
//		logger.info("==========数据段："+ByteUtil.byteArrToHexString(t.getBody()));
		
		UInfoEntity devs = new UInfoEntity();
		List<UInfoEntity.DevAddr> devList = new ArrayList<>();
		UInfoEntity.DevAddr dev = devs.new DevAddr();
		dev.setDevAddrCode(deviceCode);
		List<UInfoEntity.UdevInfo> list = new ArrayList<>();
		// 遍历获取标签数据
		for(int i=0;i<uCount;i++){
			UInfoEntity.UdevInfo u = devs.new UdevInfo();
			u.setHeight(ByteUtil.byteToInt(datas[i*LABEL_DATA_SIZE+2]));
			u.setU(ByteUtil.byteToInt(datas[i*LABEL_DATA_SIZE+3]));
//			byte[] labId=new byte[LABEL_DATA_SIZE-2];
//			System.arraycopy(datas, i*LABEL_DATA_SIZE+4, labId, 0, labId.length);
//			u.setRfid(ByteUtil.byteArrToHexString(labId));
			String labId = ByteUtil.getString(datas,i*LABEL_DATA_SIZE+4,LABEL_DATA_SIZE-2, "ascii"); // 将32字节数据转为字符
			labId=labId.replace("\u0000", "");		// 删除无用数据
			String[] labDatas = labId.split("@");
			u.setRfid(labDatas[0]);
			list.add(u);
		}
		dev.setUdevInfo(list);
		devList.add(dev);
		
		devs.setDevAddrList(devList);
//		logger.info("之后 devs.setDevAddrList(devList);");
		SmarRackMQMessage mqMessage = new SmarRackMQMessage();
		mqMessage.setActioncode(ACTION_CODE);
		mqMessage.setRfidtype(RFID_TYPE);
		mqMessage.setAwsPostdata(devs);
		String json = JSON.toJSONString(mqMessage);
//		logger.info("之前 topicSender.send(\"smarRackToService\", json);");
		topicSender.send("smarRackToService", json);
//		logger.info("扫描到的U位长度="+uCount);
		logger.info("上传U位信息="+json);
		
		DeviceChannelMap.getInstance().put(deviceCode, ctx.channel());
//		redis.setCacheObject("test", JSON.toJSONString(mqMessage));
//		String msg = redis.getCacheObject("test");
//		logger.info("!!!!!!!!!!!================"+msg);
	}
}
