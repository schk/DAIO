package com.strongbj.iot.devices.dam.sub.dacai.response.entity;

public class ScreenReturnTextControlEntity {
		/**
		 * 屏幕Id
		 */
		private short screenId;
		/**
		 * 控件id
		 */
		private short controlId;
		/**
		 * 控件类型
		 */
		private byte controlType;
		/**
		 * 文本控件的内容
		 */
		private String content;
		public short getScreenId() {
			return screenId;
		}
		public void setScreenId(short screenId) {
			this.screenId = screenId;
		}
		public short getControlId() {
			return controlId;
		}
		public void setControlId(short controlId) {
			this.controlId = controlId;
		}
		public byte getControlType() {
			return controlType;
		}
		public void setControlType(byte controlType) {
			this.controlType = controlType;
		}
		public String getContent() {
			return content;
		}
		public void setContent(String content) {
			this.content = content;
		}
}
