package com.strongbj.iot.devices.dam.request.listener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.listener.adapter.MessageListenerAdapter;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.google.gson.reflect.TypeToken;
import com.strongbj.core.message.MessageHandleContext;
import com.strongbj.iot.devices.dam.common.ITaskManager;
import com.strongbj.iot.devices.dam.common.QueueTaskManager;
import com.strongbj.iot.devices.dam.message.MQMessageOfDAM;
import com.strongbj.iot.devices.dam.request.handle.dam.GetDNSParamesHandle;
import com.strongbj.iot.devices.dam.request.handle.dam.GetDestServerParamsHandle;
import com.strongbj.iot.devices.dam.request.handle.dam.GetDeviceParamsHandle;
import com.strongbj.iot.devices.dam.request.handle.dam.GetNetParamsHandle;
import com.strongbj.iot.devices.dam.request.handle.dam.GetSensorTypeHandle;
import com.strongbj.iot.devices.dam.request.handle.dam.SetDNSParamesHandle;
import com.strongbj.iot.devices.dam.request.handle.dam.SetDestServerParamesHandle;
import com.strongbj.iot.devices.dam.request.handle.dam.SetDeviceCodeHandle;
import com.strongbj.iot.devices.dam.request.handle.dam.SetNetParamesHandle;
import com.strongbj.iot.devices.dam.request.handle.dam.SetSensorTypeHandle;
import com.strongbj.iot.devices.dam.request.handle.damtoscreen.CommandToScreenHandle;
import com.strongbj.iot.devices.dam.request.handle.damtoscreen.UpdateBindRFIDMenuHandle;

@Component
public class MQMessageOfDAMListener extends MessageListenerAdapter {
	private static Logger logger = LogManager.getLogger(MQMessageOfDAMListener.class.getName());
	ITaskManager taskManager = new QueueTaskManager();
	// 通过策略模式处理不同的信息
	private MessageHandleContext<MQMessageOfDAM, Object> messageHandleContent;

	public MQMessageOfDAMListener() {
		super();
		this.messageHandleContent = new MessageHandleContext<>();
		// 添加消息处理类 ----（将命令转到屏幕的处理类）
		this.messageHandleContent.addHandleClass(new CommandToScreenHandle());
		this.messageHandleContent.addHandleClass(new SetNetParamesHandle());
		this.messageHandleContent.addHandleClass(new SetDNSParamesHandle());
		this.messageHandleContent.addHandleClass(new SetDeviceCodeHandle());
		this.messageHandleContent.addHandleClass(new SetDestServerParamesHandle());
		this.messageHandleContent.addHandleClass(new GetSensorTypeHandle());
		this.messageHandleContent.addHandleClass(new GetNetParamsHandle());
		this.messageHandleContent.addHandleClass(new GetDNSParamesHandle());
		this.messageHandleContent.addHandleClass(new GetDeviceParamsHandle());
		this.messageHandleContent.addHandleClass(new GetDestServerParamsHandle());
		this.messageHandleContent.addHandleClass(new SetSensorTypeHandle());
		
		this.messageHandleContent.addHandleClass(new UpdateBindRFIDMenuHandle());
	}

	@JmsListener(destination = "serviceToDam", concurrency = "1")
	public void onMessage(Message message, Session session) throws JMSException {
		try {
			if (message instanceof TextMessage) {
				TextMessage tm = (TextMessage) message;
				String json = tm.getText();
				logger.info("MQ接收到json数据:" + json);
				MQMessageOfDAM rm = JSON.parseObject(json, new TypeToken<MQMessageOfDAM>() {
				}.getType());
				this.messageHandleContent.handle(null, rm);
			} else {
				logger.info("无法解析的mq对象消息:" + message.getClass().getName());
			}
		} catch (Exception e) {
			logger.error("",e);
		}
	}
}
