package com.strongbj.iot.devices.dam.request.entity;
/**
 * 目标服务器
 * @author yuzhantao
 *
 */
public class DestServerEntity extends RackCodeEntity {
	
	private String protocol;
	private String destIp;
	private short destPort;
	
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public String getDestIp() {
		return destIp;
	}
	public void setDestIp(String destIp) {
		this.destIp = destIp;
	}
	public short getDestPort() {
		return destPort;
	}
	public void setDestPort(short destPort) {
		this.destPort = destPort;
	}
	
}
