package com.strongbj.iot.devices.reader.response.entity;
/**
 * 标签基础实例
 * @author yuzhantao
 *
 */
public abstract class BaseLabelEntity {
	/**
	 * 标签类型
	 */
	private byte type;
	/**
	 * 身份Id
	 */
	private String id;
	
	public byte getType() {
		return type;
	}
	public void setType(byte type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public abstract int getLength();
}
