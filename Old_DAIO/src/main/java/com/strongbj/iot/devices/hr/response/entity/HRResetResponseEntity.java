package com.strongbj.iot.devices.hr.response.entity;

/**
 * 门禁重启反馈实体类
 * 
 * @author Pingfan
 *
 */
public class HRResetResponseEntity {
	/**
	 * 门禁设备的IP地址
	 */
	private String devIp;
	/**
	 * 用于区分各个操作的指令
	 */
	private byte command;
	/**
	 * 是否重启成功(data为0x00成功, 0x80失败)
	 */
	private boolean isResetSuccess;
	
	public String getDevIp() {
		return devIp;
	}
	public void setDevIp(String devIp) {
		this.devIp = devIp;
	}
	public byte getCommand() {
		return command;
	}
	public void setCommand(byte command) {
		this.command = command;
	}
	public boolean isResetSuccess() {
		return isResetSuccess;
	}
	public void setResetSuccess(boolean isResetSuccess) {
		this.isResetSuccess = isResetSuccess;
	}
}
