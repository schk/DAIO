package com.strongbj.iot.devices.dam.response.entity;
/**
 * 温度传感器
 * @author yuzhantao
 *
 */
public class TemperatureSensorEntity extends SensorEntity {
	/**
	 * 温度
	 */
	private short temperature;

	public short getTemperature() {
		return temperature;
	}

	public void setTemperature(short temperature) {
		this.temperature = temperature;
	}
	
	
}
