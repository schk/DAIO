package com.strongbj.iot.devices.ru.response.entity;

import com.strongbj.core.annotation.NotProguard;

@NotProguard
public class DoorInfoEntity {
	
	private String devAddrCode;
	private String cdoorstate;
	
	public String getDevAddrCode() {
		return devAddrCode;
	}
	public void setDevAddrCode(String devAddrCode) {
		this.devAddrCode = devAddrCode;
	}
	public String getCdoorstate() {
		return cdoorstate;
	}
	public void setCdoorstate(String cdoorstate) {
		this.cdoorstate = cdoorstate;
	}


}
