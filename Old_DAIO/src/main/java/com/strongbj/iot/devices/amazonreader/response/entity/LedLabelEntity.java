package com.strongbj.iot.devices.amazonreader.response.entity;
/**
 * Led标签
 * @author yuzhantao
 *
 */
public class LedLabelEntity extends BaseLabelEntity {
	/**
	 * 电池电压
	 */
	private LedLabelVoltageState voltage;
	/**
	 * 按键状态
	 */
	private LedLabelKeyState keyState;
	/**
	 * 红灯
	 */
	private LedLabelLightState redLed;
	/**
	 * 绿灯
	 */
	private LedLabelLightState greenLed;
	/**
	 * 蓝灯
	 */
	private LedLabelLightState blueLed;
	/**
	 * 白灯
	 */
	private LedLabelLightState whiteLed;
	/**
	 * 记录的8个基站短地址
	 */
	private byte[] baseStationShortAddress;
	
	public LedLabelVoltageState getVoltage() {
		return voltage;
	}
	public void setVoltage(LedLabelVoltageState voltage) {
		this.voltage = voltage;
	}
	public LedLabelKeyState getKeyState() {
		return keyState;
	}
	public void setKeyState(LedLabelKeyState keyState) {
		this.keyState = keyState;
	}
	public LedLabelLightState getRedLed() {
		return redLed;
	}
	public void setRedLed(LedLabelLightState redLed) {
		this.redLed = redLed;
	}
	public LedLabelLightState getGreenLed() {
		return greenLed;
	}
	public void setGreenLed(LedLabelLightState greenLed) {
		this.greenLed = greenLed;
	}
	public LedLabelLightState getBlueLed() {
		return blueLed;
	}
	public void setBlueLed(LedLabelLightState blueLed) {
		this.blueLed = blueLed;
	}
	public LedLabelLightState getWhiteLed() {
		return whiteLed;
	}
	public void setWhiteLed(LedLabelLightState whiteLed) {
		this.whiteLed = whiteLed;
	}
	public byte[] getBaseStationShortAddress() {
		return baseStationShortAddress;
	}
	public void setBaseStationShortAddress(byte[] baseStationShortAddress) {
		this.baseStationShortAddress = baseStationShortAddress;
	}
	@Override
	public int getLength() {
		return 14;
	}
	
	
}
