package com.strongbj.iot.devices.hr.response.entity;

/**
 * 开机信息实体类
 * 
 * @author Pingfan
 *
 */
public class BootInfoEntity {
	/**
	 * 门禁设备ip
	 */
	private String devIp;
	/**
	 * 用于区分各个操作的指令
	 */
	private byte command;
	/**
	 * 当前时间
	 */
	private String currentTime;
	/**
	 * 设备地址
	 */
	private int devCode;
	/**
	 * 工作模式
	 * 0x00定时模式，设备主动寻卡;
	 * 0x01主丛模式，设备接收上位机发来的指令寻卡;
	 * 0x02触发模式，设备检测到外部第 1 或第 2 路触发脚被拉低开始寻卡
	 */
	private byte workMode;
	
	public String getDevIp() {
		return devIp;
	}
	public void setDevIp(String devIp) {
		this.devIp = devIp;
	}
	public byte getCommand() {
		return command;
	}
	public void setCommand(byte command) {
		this.command = command;
	}
	public String getCurrentTime() {
		return currentTime;
	}
	public void setCurrentTime(String currentTime) {
		this.currentTime = currentTime;
	}
	public int getDevCode() {
		return devCode;
	}
	public void setDevCode(int devCode) {
		this.devCode = devCode;
	}
	public byte getWorkMode() {
		return workMode;
	}
	public void setWorkMode(byte workMode) {
		this.workMode = workMode;
	}
}
