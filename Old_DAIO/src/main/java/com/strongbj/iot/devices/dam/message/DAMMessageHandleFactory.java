package com.strongbj.iot.devices.dam.message;

import com.strongbj.core.message.IMessageHandleFactory;
import com.strongbj.iot.devices.dam.DAMResponseHandleContext;
import com.strongbj.iot.devices.dam.response.coder.DAMDecoder;

import io.netty.channel.ChannelHandler;

public class DAMMessageHandleFactory implements IMessageHandleFactory {

	@Override
	public ChannelHandler createMessageDecoder() {
		return new DAMDecoder(1024);
	}

	@Override
	public ChannelHandler createMessageHandle() {
		return new DAMResponseHandleContext();
	}
}
