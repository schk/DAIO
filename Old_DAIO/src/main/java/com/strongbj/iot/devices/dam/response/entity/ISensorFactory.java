package com.strongbj.iot.devices.dam.response.entity;

/**
 * 传感器工厂类，用与将协议中的字节转为实体类
 * @author yuzhantao
 *
 */
public interface ISensorFactory {
	SensorEntity createSensor(byte[] src,int offset);
}
