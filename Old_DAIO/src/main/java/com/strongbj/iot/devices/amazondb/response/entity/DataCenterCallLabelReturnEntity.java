package com.strongbj.iot.devices.amazondb.response.entity;

import java.util.List;

public class DataCenterCallLabelReturnEntity {
	private String devAddrCode;
	private List<CallLabelEntity> labs;
	public String getDevAddrCode() {
		return devAddrCode;
	}
	public void setDevAddrCode(String devAddrCode) {
		this.devAddrCode = devAddrCode;
	}
	public List<CallLabelEntity> getLabs() {
		return labs;
	}
	public void setLabs(List<CallLabelEntity> labs) {
		this.labs = labs;
	}
}
