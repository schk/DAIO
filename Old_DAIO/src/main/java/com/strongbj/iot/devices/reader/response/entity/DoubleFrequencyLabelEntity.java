package com.strongbj.iot.devices.reader.response.entity;
/**
 * 双频2.4G+125KHZ定位标签
 * @author yuzhantao
 *
 */
public class DoubleFrequencyLabelEntity extends BaseLabelEntity {

	/**
	 * 位置ID
	 */
	private short postionId;
	/**
	 * 电压状态
	 */
	private byte voltageState;
	/**
	 * 标签发射信息的次数
	 */
	private int sendCount;

	public short getPostionId() {
		return postionId;
	}
	public void setPostionId(short postionId) {
		this.postionId = postionId;
	}
	public byte getVoltageState() {
		return voltageState;
	}
	public void setVoltageState(byte voltageState) {
		this.voltageState = voltageState;
	}
	public int getSendCount() {
		return sendCount;
	}
	public void setSendCount(int sendCount) {
		this.sendCount = sendCount;
	}
	@Override
	public int getLength() {
		return 12;
	}
	
	
}
