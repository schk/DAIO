package com.strongbj.iot.devices.dam.sub.dacai.request.handle;

import java.io.UnsupportedEncodingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.dam.common.AbstractTaskDelayedRunnable;
import com.strongbj.iot.devices.dam.common.DAMManager;
import com.strongbj.iot.devices.dam.common.ITaskManager;
import com.strongbj.iot.devices.dam.message.DAMMessage;
import com.strongbj.iot.devices.dam.message.DAMMessageFactory;
import com.strongbj.iot.devices.dam.response.handle.v1.DAMV1Handle;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
/**
 * 显示设置传感器的返回信息到屏幕
 * @author yuzhantao
 *
 */
public class ShowSensorParamsReturnHandle extends DAMV1Handle {
	private static Logger logger = LogManager.getLogger(ShowSensorParamsReturnHandle.class.getName());
//	private static final byte SCREEN_ID = (byte) 6; // 传感器屏幕ID
//	private static final byte DIALOG_ID = (byte)0xFA; // IP控件ID（文本框在屏幕中的ID）
//	private static final String STRING_CODE = "GBK";
	private static final byte[] SHOW_CONTROL_DATAS = {(byte)0xEE, (byte)0xB1 , (byte)0x00 , (byte)0x00 , (byte)0x10, (byte)0xFF, (byte)0xFC, (byte)0xFF, (byte)0xFF};
	private static final DAMMessageFactory damMessageFactory = new DAMMessageFactory(); // DAM消息工厂

	@Override
	protected boolean isHandleDAMMessage(DAMMessage t) {
		if (t.getCommand() == 0x16) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, DAMMessage t) {
//		String dialogMsg = t.getDatas()[0]==1?"设置成功":"设置失败";
		// TODO 如果成功在屏幕上显示对话框，否则暂时不显示
		if(t.getDatas()[0]!=1) return null;
		ITaskManager taskManager = DAMManager.getTaskManager(ByteUtil.byteArrToHexString(t.getAddressCode()));
		taskManager.addSendTask(new AbstractTaskDelayedRunnable() {
			@Override
			protected void taskRun() {
				try {
					sendMessage(ctx, t.getAddressCode());
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
		});
		
		return null;
	}

	private void sendMessage(ChannelHandlerContext ctx, byte[] devAddress)
			throws UnsupportedEncodingException {
		byte[] datas = createDialogSuccessMessage(devAddress);
		ChannelFuture cf = ctx.channel().writeAndFlush(Unpooled.copiedBuffer(datas));
		cf.addListener(new ChannelFutureListener() {
			@Override
			public void operationComplete(ChannelFuture future) throws Exception {
				if (future.isSuccess()) {
					logger.info("向屏幕下发显示传感器消息框命令成功 下发命令={}  ", ByteUtil.byteArrToHexString(datas, true));
				} else {
					logger.error("向屏幕下发显示传感器消息框命令失败 下发命令={}  ", ByteUtil.byteArrToHexString(datas, true));
				}
			}
		});
	}

	private byte[] createDialogSuccessMessage(byte[] devAddress)
			throws UnsupportedEncodingException {
		return damMessageFactory.createDAMMessage(ByteUtil.longToBytes(System.currentTimeMillis()), devAddress, // 设备编号
				(byte) 0, // 设备类型，默认填0
				(byte) 0, // 版本号，默认填0
				(byte) 0xFF, // 设置设备编号命令
				SHOW_CONTROL_DATAS // 设置设备编号
		);
	}
}

