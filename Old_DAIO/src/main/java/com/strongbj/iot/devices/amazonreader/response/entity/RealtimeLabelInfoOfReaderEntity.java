package com.strongbj.iot.devices.amazonreader.response.entity;

/**
 * 实时在Reader中采集的标签信息
 * @author yuzhantao
 *
 */
public class RealtimeLabelInfoOfReaderEntity {
	private String card;
	private String batterState;
	private String shortAddr;
	private String wlightstate;
	private String blightstate;
	private String gightstate;
	private String rlightstate;
	private String btnstate;
	public String getCard() {
		return card;
	}
	public void setCard(String card) {
		this.card = card;
	}
	public String getBatterState() {
		return batterState;
	}
	public void setBatterState(String batterState) {
		this.batterState = batterState;
	}
	
	public String getShortAddr() {
		return shortAddr;
	}
	public void setShortAddr(String shortAddr) {
		this.shortAddr = shortAddr;
	}
	public String getWlightstate() {
		return wlightstate;
	}
	public void setWlightstate(String wlightstate) {
		this.wlightstate = wlightstate;
	}
	public String getBlightstate() {
		return blightstate;
	}
	public void setBlightstate(String blightstate) {
		this.blightstate = blightstate;
	}
	public String getGightstate() {
		return gightstate;
	}
	public void setGightstate(String gightstate) {
		this.gightstate = gightstate;
	}
	public String getRlightstate() {
		return rlightstate;
	}
	public void setRlightstate(String rlightstate) {
		this.rlightstate = rlightstate;
	}
	public String getBtnstate() {
		return btnstate;
	}
	public void setBtnstate(String btnstate) {
		this.btnstate = btnstate;
	}

}
