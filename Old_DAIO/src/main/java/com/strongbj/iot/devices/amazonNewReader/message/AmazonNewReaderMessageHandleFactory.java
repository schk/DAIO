package com.strongbj.iot.devices.amazonNewReader.message;

import com.strongbj.core.message.IMessageHandleFactory;
import com.strongbj.iot.devices.amazonNewReader.response.coder.NewReaderDecoder;
import com.strongbj.iot.devices.amazonNewReader.response.handle.NewReaderResponseHandleContext;

import io.netty.channel.ChannelHandler;

public class AmazonNewReaderMessageHandleFactory implements IMessageHandleFactory{

	@Override
	public ChannelHandler createMessageDecoder() {
		return new NewReaderDecoder(1024);
	}

	@Override
	public ChannelHandler createMessageHandle() {
		return new NewReaderResponseHandleContext();
	}

}
