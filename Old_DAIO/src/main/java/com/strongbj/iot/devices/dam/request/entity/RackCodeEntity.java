package com.strongbj.iot.devices.dam.request.entity;

/**
 * 机柜编号
 * @author yuzhantao
 *
 */
public class RackCodeEntity {
	/**
	 * 机柜编号
	 */
	private String rackConverCode;

	public String getRackConverCode() {
		return rackConverCode;
	}

	public void setRackConverCode(String rackConverCode) {
		this.rackConverCode = rackConverCode;
	}
	
}
