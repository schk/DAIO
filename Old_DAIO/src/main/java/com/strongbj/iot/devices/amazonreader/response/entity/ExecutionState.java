package com.strongbj.iot.devices.amazonreader.response.entity;
/**
 * 执行状态
 * @author yuzhantao
 *
 */
public enum ExecutionState {
	/**
	 * 成功
	 */
	Success,
	/**
	 * 失败
	 */
	Fail,
	/**
	 * 完成
	 */
	Complete,
	/**
	 * 未知
	 */
	Unknown
}
