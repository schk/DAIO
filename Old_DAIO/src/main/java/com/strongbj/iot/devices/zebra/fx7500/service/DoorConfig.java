package com.strongbj.iot.devices.zebra.fx7500.service;

public class DoorConfig {
	private String ip;
	private String direction;
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
}
