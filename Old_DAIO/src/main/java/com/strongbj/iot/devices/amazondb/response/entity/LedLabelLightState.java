package com.strongbj.iot.devices.amazondb.response.entity;

public enum LedLabelLightState {
	/**
	 * 开
	 */
	On,
	/**
	 * 关
	 */
	Off
}
