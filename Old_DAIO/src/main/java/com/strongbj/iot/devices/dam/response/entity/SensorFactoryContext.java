package com.strongbj.iot.devices.dam.response.entity;

/**
 * 传感器工厂上下文
 * @author yuzhantao
 *
 */
public class SensorFactoryContext implements ISensorFactory {
	// 温度传感器工厂
	private TemperatureSensorFactory temperatureSensorFactory = new TemperatureSensorFactory();
	// 湿度传感器工厂
	private TemperatureAndHumiditySensorFactory temperatureAndHumiditySensorFactory = new TemperatureAndHumiditySensorFactory();
	@Override
	public SensorEntity createSensor(byte[] src, int offset) {
		byte sensorType = src[offset];
		switch(sensorType){
		case 0:		// 温度传感器
			return temperatureSensorFactory.createSensor(src, offset);
		case 1:		// 温湿度传感器
			return temperatureAndHumiditySensorFactory.createSensor(src, offset);
		default:
				return null;
		}
	}
}
