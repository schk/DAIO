package com.strongbj.iot.devices.amazonNewReader.response.handle;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.amazonNewReader.message.NewReaderMessage;
import com.strongbj.iot.devices.amazonNewReader.request.entity.T;
import com.strongbj.iot.devices.amazonNewReader.response.common.HandleTimeStamp;
import com.strongbj.iot.devices.guis.respnose.common.RedirectOutputStream;

import io.netty.channel.ChannelHandlerContext;
/**
 * 标签绑定  Handle
 * @author 25969
 *
 */
public class LabelBindingResponseHandle implements IMessageHandle<NewReaderMessage,Object>{
	private static Logger logger = LogManager.getLogger(LabelBindingResponseHandle.class.getName());
	private static SimpleDateFormat df=null;
	@Override
	public boolean isHandle(NewReaderMessage t) {
		if(Integer.valueOf("2", 16).byteValue()==t.getType()){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, NewReaderMessage t) {
		System.out.println("返回报文--"+ByteUtil.byteArrToHexString(t.getBody()));
		String hostNumber =  ByteUtil.byteArrToHexString(t.getAddress());   //reader 地址码
		 String timeStamp  =  ByteUtil.byteArrToHexString(t.getTime());   //时间戳
		 //4 字节标签地址 + 2 字节标签短地址 + 2 字节标签信息（参考 LED 标签信息格 式，下同） + 保留字段。
		 String ledTag = ByteUtil.byteArrToHexString(t.getBody(), 0, 4);  // LED 标签地址
		 String shortAddress  = ByteUtil.byteArrToHexString(t.getBody(), 4, 2);  //签短地址 
		 String led = T.byteToBit(t.getBody()[6]);  //LED  字节
        //LED 状态 ： 0 ：灭   1： 慢闪   2：快闪  3：长亮
        int led4 = Integer.parseInt(led.substring(0, 2),2);  //LED4 状态 
        int led3 = Integer.parseInt(led.substring(2, 4),2);  //LED3 状态
        int led2 = Integer.parseInt(led.substring(4, 6),2);  //LED2 状态
        int led1 = Integer.parseInt(led.substring(led.length()-2),2);  //LED1 状态
        String state  = T.byteToBit(t.getBody()[7]); //LED
        int electricityThreshold=Integer.parseInt(state.substring(0, 3),2);  //电量阈值，0~6 
        int electricityQuantity =Integer.parseInt(state.substring(3, 6),2);  //当前电量,0~6 
        int keyState = Integer.parseInt(state.substring(state.length()-2),2); //按键状态   
        String log ="收到标签绑定请求返回报文: reader 地址码： "+hostNumber+"  LED 标签地址： "+ledTag +
     		   " 短地址 ： "+shortAddress+
     		   "[LED 状态 ： 0 ：灭   1： 慢闪   2：快闪  3：长亮]"+
         		"/ LED4 状态 ： "+led4+
         		"/ LED3 状态 ： "+led3+
         		"/ LED2 状态 ： "+led2+
         		"/ LED1 状态 ： "+led1+
         		"/ 电量阈值 ：  "+electricityThreshold+
         		" 当前电量 ：   "+electricityQuantity+
         		" 按键状态：  "+keyState
         		+" 时间戳：   "+HandleTimeStamp.handle(timeStamp);
        logger.info(log);
        df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
  	     RedirectOutputStream.put(df.format(new Date())+ log);
		return null;
	}

}
