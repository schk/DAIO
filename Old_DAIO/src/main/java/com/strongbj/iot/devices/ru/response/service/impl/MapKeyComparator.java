package com.strongbj.iot.devices.ru.response.service.impl;

import java.util.Comparator;

class MapKeyComparator implements Comparator<Byte>{

    @Override
    public int compare(Byte b1, Byte b2) {
        
        return b1.compareTo(b2);
    }
}
