package com.strongbj.iot.devices.dam.message;

public class DAMMessage {
	/**
	 * 地址吗
	 */
	private byte[] addressCode;
	/**
	 * 消息流水号
	 */
	private byte[] sn;
	/**
	 * 设备类型
	 */
	private byte deviceType;
	/**
	 * 版本
	 */
	private byte version;
	/**
	 * 命令码
	 */
	private byte command;
	/**
	 * 总帧数
	 */
	private byte frameTotal;
	/**
	 * 当前帧索引
	 */
	private byte currentFrameIndex;
	/**
	 * 数据段
	 */
	private byte[] datas;

	public DAMMessage() {}
	
	public DAMMessage(
			byte[] sn,
			byte[] addressCode,
			byte deviceType,
			byte version,
			byte command,
			byte frameTotal,
			byte currentFrameIndex,
			byte[] datas){
		this.sn=sn;
		this.addressCode=addressCode;
		this.deviceType=deviceType;
		this.version=version;
		this.command=command;
		this.frameTotal=frameTotal;
		this.currentFrameIndex=currentFrameIndex;
		this.datas=datas;
	}
	
	public byte[] getSn() {
		return sn;
	}

	public void setSn(byte[] sn) {
		this.sn = sn;
	}

	public byte[] getAddressCode() {
		return addressCode;
	}

	public void setAddressCode(byte[] addressCode) {
		this.addressCode = addressCode;
	}

	public byte getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(byte deviceType) {
		this.deviceType = deviceType;
	}

	public byte getVersion() {
		return version;
	}

	public void setVersion(byte version) {
		this.version = version;
	}

	public byte getCommand() {
		return command;
	}

	public void setCommand(byte command) {
		this.command = command;
	}

	public byte getFrameTotal() {
		return frameTotal;
	}

	public void setFrameTotal(byte frameTotal) {
		this.frameTotal = frameTotal;
	}

	public byte getCurrentFrameIndex() {
		return currentFrameIndex;
	}

	public void setCurrentFrameIndex(byte currentFrameIndex) {
		this.currentFrameIndex = currentFrameIndex;
	}

	public byte[] getDatas() {
		return datas;
	}

	public void setDatas(byte[] datas) {
		this.datas = datas;
	}
	
	
}
