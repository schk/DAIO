package com.strongbj.iot.devices.dam.sub.dacai.response.handle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.dam.common.AbstractTaskDelayedRunnable;
import com.strongbj.iot.devices.dam.common.DAMManager;
import com.strongbj.iot.devices.dam.common.ITaskManager;
import com.strongbj.iot.devices.dam.message.DAMMessageFactory;
import com.strongbj.iot.devices.dam.sub.dacai.message.DaCaiMessage;
import com.strongbj.iot.devices.dam.sub.dacai.message.DaCaiMessageType;
import com.strongbj.iot.devices.dam.sub.dacai.response.entity.ScreenReturnTextControlEntity;
import com.strongbj.iot.devices.dam.sub.dacai.response.entity.ScreenReturnTextControlFactory;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.HashedWheelTimer;
import io.netty.util.Timer;

/**
 * 设置目标服务器网络参数
 * @author yuzhantao
 *
 */
public class SetDestServerNetParamsHandle implements IMessageHandle<DaCaiMessage, Object> {
	private static Logger logger = LogManager.getLogger(OpenNetParamsPageHandle.class.getName());
	protected final static String COMMAND_CODE = "B111"; // 打开页面命令的编号
	protected final static byte[] DEFAULT_DEVICE_CODE = {00,00,00}; // 设备默认编号
	protected final static byte[] EMPTY_DATAS = {}; // 空参数
	protected final static long MAX_SET_AND_WAIT_MILLISECOND = 2000; // 最大等待并设置参数的毫秒数
	protected DAMMessageFactory damMessageFactory = new DAMMessageFactory();
	protected final Timer taskTimer = new HashedWheelTimer();
	private String ip, port; // 从屏幕获取的数据
	private long prevGetTime; // 上一次获取数据的时间
	private ScreenReturnTextControlEntity screenReturnTextControlEntity;

	@Override
	public boolean isHandle(DaCaiMessage t) {
		if (t.getMessageType() == DaCaiMessageType.System && t.getCommand().equals(COMMAND_CODE)) {
			ScreenReturnTextControlEntity sr = ScreenReturnTextControlFactory.create(t.getDatas());

			// 如果控件类型为按钮，屏幕id为4，按钮id为4、5，就认为是打开网络界面
			if (sr.getControlType() == 0x11 && sr.getScreenId() == 4
					&& (sr.getControlId() == 4 || sr.getControlId() == 5)) {
				this.screenReturnTextControlEntity = sr;
				return true;
			}
		}
		return false;
	}

	private void initScreenParams() {
		this.ip = "";
		this.port = "";
	}
	@Override
	public Object handle(ChannelHandlerContext ctx, DaCaiMessage t) {
		if (System.currentTimeMillis() - prevGetTime > MAX_SET_AND_WAIT_MILLISECOND) {
			initScreenParams();
		}
		switch (this.screenReturnTextControlEntity.getControlId()) {
		case 4:
			this.ip = new String(ByteUtil.hexStringToBytes(this.screenReturnTextControlEntity.getContent()));
			this.ip = this.ip.replace("\0", "").replace("\r", "").replace("\n", "");
			break;
		case 5:
			this.port = new String(ByteUtil.hexStringToBytes(this.screenReturnTextControlEntity.getContent()));
			this.port = this.port.replace("\0", "").replace("\r", "").replace("\n", "");
			break;
		default:
			return null;
		}
		this.prevGetTime = System.currentTimeMillis();
		
		if(this.ip=="" || this.port=="") return null;

		byte[] datas = this.NetParamesEntity2Bytes(DEFAULT_DEVICE_CODE,this.ip,Short.parseShort(this.port));
		
		ITaskManager taskManager = DAMManager.getTaskManager(t.getDamCode());
		// 设定500毫秒后在设置目标服务器（由于此处理信息会和设置本地网络参数同时向硬件发送命令，但硬件无法处理在短时间处理，所以延时）
		taskManager.addSendTask(new AbstractTaskDelayedRunnable() {
			@Override
			protected void taskRun() {
				SetDestServerNetParamsHandle.this.sendMessageToDAM(ctx.channel(), datas);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		initScreenParams();  // 发送后初始化屏幕参数
		return null;
	}

	/**
	 * 发送获取本地网络的消息
	 * 
	 * @param channel
	 */
	private void sendMessageToDAM(Channel channel,byte[] datas) {
		ByteBuf bs = Unpooled.copiedBuffer(datas);
		ChannelFuture cf = channel.writeAndFlush(bs);
		cf.addListener(new ChannelFutureListener() {

			@Override
			public void operationComplete(ChannelFuture future) throws Exception {
				if (future.isSuccess()) {
					logger.info("DAM自动下发设置目标服务器网络参数命令成功 下发命令={}  ", ByteUtil.byteArrToHexString(datas, true));
				} else {
					logger.error("DAM自动下发设置目标服务器网络参数命令失败 下发命令={}  ", ByteUtil.byteArrToHexString(datas, true));
				}
			}
		});
	}

	/**
	 * 网络参数对象转byte数组
	 * @param np
	 * @return
	 */
	private byte[] NetParamesEntity2Bytes(byte[] devAddress,String ip,short port) {
		// 创建
		byte[] netParamsDatas = new byte[6];
		// 将ip赋值到4byte数组中
		this.setParamsToBytes(ip,netParamsDatas,0);
		// 将端口赋值到2byte数组中
		System.arraycopy(ByteUtil.shortToByteArr(port), 0, netParamsDatas, 4, 2);
		
		// 将命令发送到屏幕硬件
		byte[] datas = damMessageFactory.createDAMMessage(
				ByteUtil.longToBytes(System.currentTimeMillis()),
				devAddress, // 设备编号
				(byte)0,		// 设备类型，默认填0
				(byte)0, 		// 版本号，默认填0
				(byte)0x12, 	// 设置网络参数命令
				netParamsDatas	// 设置的网络参数数据
			);  // 通过dam消息工厂获取设置网络参数的byte命令数组
		return datas;
	}
	
	/**
	 * 设置指定*.*.*.*格式的数据转为4byte数组
	 * @param srcString		源数据
	 * @param destBytes		目标数组
	 * @param destOffset	赋值的目标数组偏移量
	 */
	private void setParamsToBytes(String srcString,byte[] destBytes,int destOffset) {
		String[] strIps = srcString.split("[.]");
		for(int i=0;i<strIps.length;i++) {
			String sNum = strIps[i].replace("\0", "").replace("\r", "").replace("\n", "");
			destBytes[i+destOffset]=Integer.valueOf(sNum, 10).byteValue();
		}
	}
}
