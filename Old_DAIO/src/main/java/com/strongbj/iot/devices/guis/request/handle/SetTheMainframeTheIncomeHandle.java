package com.strongbj.iot.devices.guis.request.handle;

import java.net.InetSocketAddress;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.strongbj.core.message.IMessageHandle;
import com.strongbj.core.util.ByteUtil;
import com.strongbj.iot.devices.guis.message.GUISOSMessageFactory;
import com.strongbj.iot.devices.guis.message.MQMessageOfGUIS;
import com.strongbj.iot.devices.guis.request.entity.SetTheMainframeTheIncomeEntity;
import com.strongbj.iot.devices.guis.respnose.common.RedirectOutputStream;
import com.strongbj.iot.devices.guis.respnose.handle.GUISResponseHandleContext;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.group.ChannelMatcher;
/**
 * 上位机设置主机流水号  handle
 * @author 25969
 *
 */
public class SetTheMainframeTheIncomeHandle implements IMessageHandle<MQMessageOfGUIS, Object> {
	private static Logger logger = LogManager.getLogger(SetTheMainframeTheIncomeHandle.class.getName());
	private final static String ACTION_CODE = "reader021";   //MQ中判断消息类型的标识符
	private final static byte command = (byte)0x01;
	private GUISOSMessageFactory gUISOSMessageFactory = new GUISOSMessageFactory();
//	private TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");
	private static SimpleDateFormat df=null;
	@Override
	public boolean isHandle(MQMessageOfGUIS t) {
		if(t.getActioncode().equals(ACTION_CODE)){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, MQMessageOfGUIS t) {
		logger.info("GUIS 收到MQ发来的“上位机设置主机流水号” json={}",JSON.toJSONString(t));
		SetTheMainframeTheIncomeEntity setTheMainframeTheIncome = ((JSONObject)t.getAwsPostdata()).toJavaObject(SetTheMainframeTheIncomeEntity.class);
		 this.messageDataToBytes(setTheMainframeTheIncome);
		return null;
	}
    
	/**
	 * 解析并下发数据给下位机
	 * @param setTheMainframeTheIncome
	 */
	private void messageDataToBytes(SetTheMainframeTheIncomeEntity setTheMainframeTheIncome) {
		String devAddrCode= setTheMainframeTheIncome.getDevAddrCode();  //机柜编号
//		String devIp = setTheMainframeTheIncome.getDevIp();   //  机柜IP
		byte[] lightComByte = new byte[3];   //装数据的byte数组
		byte[] dest = new byte[3];   //接受主机编号的临时byte数组，因为上位机设置主机流水号这个指令，报文中HID默认是00 00 00,此处直接创建一个默认的字节数组作为HID
		dest[0]=(byte)0x00;
		dest[0]=(byte)0x00;
		dest[0]=(byte)0x00;
		byte[] datas = gUISOSMessageFactory.createGUISOSMessage(dest, command, ByteUtil.hexStringToBytes(devAddrCode,lightComByte,0));
		//打印
		System.out.println("ByteUtil.byteArrToHexString(datas)  " + ByteUtil.byteArrToHexString(datas));
		ByteBuf bs  = Unpooled.copiedBuffer(datas);
		//HRResponseHandleContext.channels.writeAndFlush(bs,new MyChannelMatchers(devIp));
		GUISResponseHandleContext.channels.writeAndFlush(bs);
		logger.info("GUIS 发送位机设置主机流水号命令成功 message={}",ByteUtil.byteArrToHexString(datas));
		 df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		RedirectOutputStream.put(df.format(new Date())+"上位机 发送位机设置主机流水号命令成功 message={}   "+ByteUtil.byteArrToHexString(datas));
	}

	class MyChannelMatchers implements ChannelMatcher{
		private String currentIP;
		public MyChannelMatchers(String ip)
		{
			this.currentIP=ip;
		}
		
		@Override
		public boolean matches(Channel channel) {
			InetSocketAddress insocket = (InetSocketAddress)channel.remoteAddress();
			if(insocket.getAddress().getHostAddress().equals(this.currentIP)){
				return true;
			}else{
				return false;
			}
		}
	}
	
	
}
