package com.strongbj.iot.devices.ru.response.service;

import java.util.Collection;
import java.util.Map;

import com.strongbj.iot.devices.ru.response.entity.UInfoEntity;

public interface ICabinetService {
	/**
	 * 设置机柜门状态
	 * @param devAddr 机柜编号
	 * @param state	  机柜门状态，0为关门，1为开门
	 */
	void setDoorState(String devAddr,byte state);
	/**
	 * 根据机柜编号查找机柜信息
	 * @param devAddr
	 * @return
	 */
	UInfoEntity findCabinetInfo(String devAddr);
	
	/**
	 * 查询所有机柜信息
	 * @return
	 */
	Collection<UInfoEntity> findAllCabinetInfoList();
	/**
	 * 移除指定机柜信息
	 * @param devAddr
	 */
	void removeCabinetInfo(String devAddr);
	/**
	 * 更新U位信息
	 * @param UInfoEntity
	 */
	void updateScanUInfo(UInfoEntity entity);
	/**
	 * 检查机柜信息是否可以上传
	 * @param devAddr
	 * @param uMap
	 * @return
	 */
	 boolean checkCabinetInfoIsPush(String devAddr, Map<Byte, String> uMap);
	 /**
	  * 添加检测的历史机柜信息
	  * @param devAddr
	  * @param uMap
	  */
	 void addCheckCabinetInfo(String devAddr, Map<Byte, String> uMap);
}
