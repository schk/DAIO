package com.strongbj.iot.devices.reader.response.handle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.strongbj.core.message.MessageHandleContext;
import com.strongbj.iot.devices.reader.message.ReaderMessage;
import com.strongbj.iot.devices.reader.server.ReaderServer;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
/**
 * Reader基站应答处理的上下文
 * @author yuzhantao
 *
 */
public class ReaderResponseHandleContext extends SimpleChannelInboundHandler<ReaderMessage> {
	private MessageHandleContext<ReaderMessage,Object> messageHandleContent;
	private static Logger logger = LogManager.getLogger(ReaderResponseHandleContext.class.getName());
	
	public ReaderResponseHandleContext(){
		super();
		this.messageHandleContent = new MessageHandleContext<>();
		this.messageHandleContent.addHandleClass(new ActivelyUploadedLabelInfoResponseHandle());
		this.messageHandleContent.addHandleClass(new DataCenterCallLabelResponseHandle());
	}
	
	@Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        ReaderServer.channels.add(ctx.channel());
        logger.info("Reader设备已连接"+ctx.channel().remoteAddress().toString());
		super.channelActive(ctx);
    }

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		logger.info("Reader设备已断开"+ctx.channel().remoteAddress().toString());
		super.channelInactive(ctx);
	}
	
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, ReaderMessage msg)
			throws Exception {
		if(msg==null) return;
		// logger.info("收到Reader数据信息，数据类型："+msg.getType()+"	设备编号："+msg.getAddress()+"		数据位："+ByteUtil.byteArrToHexString(msg.getBody()));
		this.messageHandleContent.handle(ctx,msg);
	}
	
}
