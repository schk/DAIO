package com.strongbj.iot.devices.hr.message;

import com.strongbj.core.message.IMessageHandleFactory;
import com.strongbj.iot.devices.hr.response.coder.HRDecoder;
import com.strongbj.iot.devices.hr.response.handle.HRResponseHandleContext;

import io.netty.channel.ChannelHandler;

/**
 * HR消息处理工厂，用来创建拆包、消息处理类
 * @author yuzhantao
 *
 */
public class HRMessageHandleFactory implements IMessageHandleFactory {
	private String doorDirection;
	
	// 读取标签位数, 0是取后8位，1是全量；默认是0
	private Integer labelCount;
	
	public String getDoorDirection() {
		return doorDirection;
	}

	public void setDoorDirection(String doorDirection) {
		this.doorDirection = doorDirection;
	}
	
	public Integer getLabelCount() {
		return labelCount;
	}

	public void setLabelCount(Integer labelCount) {
		this.labelCount = labelCount;
	}

	@Override
	public ChannelHandler createMessageDecoder() {
		return new HRDecoder(Integer.MAX_VALUE, 6, 2, 2, 0, true);
	}

	@Override
	public ChannelHandler createMessageHandle() {
		return new HRResponseHandleContext(this.getDoorDirection(), this.getLabelCount());
	}

}
