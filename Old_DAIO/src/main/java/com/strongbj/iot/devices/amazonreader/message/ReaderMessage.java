package com.strongbj.iot.devices.amazonreader.message;

public class ReaderMessage {

	// 地址
	private byte[] address;

	// 类型
	private short type;

	// 数据
	private byte[] body;

	public ReaderMessage(byte[] address,short type,byte[] body) { 
		this.address = address;
		this.type = type;
		this.body = body;
	}

	public byte[] getAddress() {
		return address;
	}

	public void setAddress(byte[] address) {
		this.address = address;
	}

	public short getType() {
		return type;
	}

	public void setType(short type) {
		this.type = type;
	}

	public byte[] getBody() {
		return body;
	}

	public void setBody(byte[] body) {
		this.body = body;
	}
}
