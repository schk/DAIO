package com.strongbj.iot;

import java.util.Iterator;
import java.util.Map.Entry;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Booter {

	private static AbstractApplicationContext ctx;

	public static void main(String[] args)  {
//		Date date = new Date();
//		date.setTime(1527830678503L);
//		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		String dateString = formatter.format(date);
//		 System.out.println("==============="+dateString);
		String osName = System.getProperty("os.name");
		String osBit = System.getProperty("os.arch");
		String osVersion = System.getProperty("os.version");
		System.out.println("================采集软件版本:2.2.1================");
		System.out.println("添加小屏功能");
		System.out.println("================操作系统:"+osName+"================");
		System.out.println("Version:"+osVersion+"    Bit:"+osBit);
		System.out.println("------------------------------------------------");
		
		

		   
		Iterator<Entry<Object, Object>> it = System.getProperties().entrySet().iterator();  
        while (it.hasNext()) {  
            Entry<Object, Object> entry = it.next();  
            Object key = entry.getKey();  
            Object value = entry.getValue(); 
            System.out.println(key+"="+value); 
        } 
        
//		String javaVersion = System.getProperty("java.version");
//		String javaHome = System.getProperty("java.home");
//		String javaLibrayPath = System.getProperty("java.library.path");
//		String userDir = System.getProperty("user.dir");
//		StringTokenizer parser = new StringTokenizer(javaLibrayPath, ";");
//		
//		System.out.println("java.version:"+javaVersion);
//		System.out.println("java.home:"+javaHome);
//		System.out.println("user.dir:"+userDir);
//		while (parser.hasMoreTokens()) {
//		    System.out.println("java.library.path:"+parser.nextToken());
//		}
		System.out.println("================================================");
//		System.out.println(System.getProperty("user.dir")+"/src/main/resources/project/innotel/properties");
//		LicenseVertify vlicense=new LicenseVertify(
//				"/project/innotel/properties/license.properties",
//				"adio_license"); // 项目唯一识别码，对应生成配置文件的subject  
//
//        vlicense.install(System.getProperty("user.dir"));  
//        
//        vlicense.vertify();
        
		String[] configFile = { "application-context.xml" };
		ctx = new ClassPathXmlApplicationContext(configFile);
		ctx.registerShutdownHook();
	}
}
