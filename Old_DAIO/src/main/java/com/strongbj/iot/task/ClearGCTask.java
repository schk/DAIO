package com.strongbj.iot.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
@Service
@Lazy(false)
public class ClearGCTask {
	private static Logger logger = LogManager.getLogger(ClearGCTask.class.getName());
	
	@Scheduled(cron = "0 0/10 * * * ?")
	public void clearGCTask() {
		System.gc();
		logger.info("=================每10分钟清理一次GC");
	}
}
