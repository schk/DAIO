package com.strongbj.iot.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.reflect.TypeToken;
import com.strongbj.core.message.MessageHandleContext;
import com.strongbj.iot.devices.guis.message.MQMessageOfGUIS;
import com.strongbj.iot.devices.guis.request.handle.ControlOpenColorLedTwinkleHandle2;
import com.strongbj.iot.devices.guis.request.handle.GetHostNetworkInformationHandle;
import com.strongbj.iot.devices.guis.request.handle.GetTheHostBasicInformationHandle;
import com.strongbj.iot.devices.guis.request.handle.SetHostHardwareVersionHandle;
import com.strongbj.iot.devices.guis.request.handle.SetHostNetworkInformationHandle;
import com.strongbj.iot.devices.guis.request.handle.SetHostSoftwareVersionHandle;
import com.strongbj.iot.devices.guis.request.handle.SetTheFunctionOptionsHandle;
import com.strongbj.iot.devices.guis.request.handle.SetTheHostMACAddressHandle;
import com.strongbj.iot.devices.guis.request.handle.SetTheMainframeTheIncomeHandle;
import com.strongbj.iot.devices.guis.request.handle.StartTheReadCardHandle;
import com.strongbj.iot.devices.guis.request.handle.TransmissionHandle;
import com.strongbj.iot.devices.guis.request.listener.MQMessageOfGUISListener;
import com.strongbj.iot.devices.guis.respnose.common.SendLightAnswerListener;
import com.strongbj.iot.devices.hr.message.MQMessageOfHR;
import com.strongbj.iot.devices.hr.request.handle.BuzzerHandle;
import com.strongbj.iot.devices.hr.request.handle.HRControlBuzzerSoundHandle;
import com.strongbj.iot.devices.hr.request.handle.HRResetHandle;

@Component
public class KafkaConsumerMessageListener implements MessageListener<Integer, String> {

	private static Logger logger = LogManager.getLogger(MQMessageOfGUISListener.class.getName());
	// 通过策略模式处理不同的信息
	private MessageHandleContext<MQMessageOfGUIS, Object> messageHandleContent;
	private MessageHandleContext<MQMessageOfHR, Object> hrMessageHandleContent;

	public KafkaConsumerMessageListener() {
		this.messageHandleContent = new MessageHandleContext<>();
		
		this.hrMessageHandleContent = new MessageHandleContext<>();
		// 添加消息处理类
		// 蜂鸣器命令的处理
		this.hrMessageHandleContent.addHandleClass(new BuzzerHandle());
		// 控制蜂鸣器发声处理类
		this.hrMessageHandleContent.addHandleClass(new HRControlBuzzerSoundHandle());
		// 门禁重启处理类
		this.hrMessageHandleContent.addHandleClass(new HRResetHandle());

		// 添加消息处理类 ----（上位机控制打开LED灯闪烁）
		// this.messageHandleContent.addHandleClass(new ControlOpenLedTwinkleHandle());
		// 添加彩灯接口处理类(老唐新加的),启动该行代码时，须注释上一段
		this.messageHandleContent.addHandleClass(new ControlOpenColorLedTwinkleHandle2());
//		this.messageHandleContent.addHandleClass(new ControlOpenColorLedTwinkleHandle());
		// 下面是测试的解析类，暂时注释上面的，正式上线时，放开上面的，注释下面的
//		this.messageHandleContent.addHandleClass(new TestControlOpenLedTwinkleHandle());
		// 添加消息处理类 ----（ 上位机设置主机流水号（主机唯一编号））
		this.messageHandleContent.addHandleClass(new SetTheMainframeTheIncomeHandle());
		// 添加消息处理类 ----（上位机设置主机硬件版本）
		this.messageHandleContent.addHandleClass(new SetHostHardwareVersionHandle());
		// 添加消息处理类 ---- (上位机设置主机软件版本)
		this.messageHandleContent.addHandleClass(new SetHostSoftwareVersionHandle());
		// 添加消息处理类 ---- (上位机设置主机MAC地址)
		this.messageHandleContent.addHandleClass(new SetTheHostMACAddressHandle());
		// 添加消息处理类 ---- (上位机设置主机网络信息)
		this.messageHandleContent.addHandleClass(new SetHostNetworkInformationHandle());
		// 添加消息处理类 ----（上位机设置主机扫描标签后定时向上位机发送标签信息的功能选项）
		this.messageHandleContent.addHandleClass(new SetTheFunctionOptionsHandle());
		// 添加消息处理类---- （上位机获取主机基本信息）
		this.messageHandleContent.addHandleClass(new GetTheHostBasicInformationHandle());
		// 添加消息处理类 ---- (上位机获取主机网络配置信息)
		this.messageHandleContent.addHandleClass(new GetHostNetworkInformationHandle());
		// 添加消息处理类 ----(上位机启动读卡)
		this.messageHandleContent.addHandleClass(new StartTheReadCardHandle());
		// 添加消息处理类 ----(大彩屏幕数据偷传)
		this.messageHandleContent.addHandleClass(new TransmissionHandle());
		// 开启循环遍历存储时间戳的数组，如果时间差值大于4秒向上返回点亮失败 线程
		SendLightAnswerListener sendListener = new SendLightAnswerListener();
		Thread thread = new Thread(sendListener);
		thread.start();
	}

	@Override
	public void onMessage(ConsumerRecord<Integer, String> data) {
		try {
			if ("DAIO".equals(data.topic())) {
				String json = data.value();
				MQMessageOfGUIS rm = JSON.parseObject(json, new TypeToken<MQMessageOfGUIS>() {
				}.getType());
				this.messageHandleContent.handle(null, rm);
			} else if ("HR".equals(data.topic())) {
				String json = data.value();
				MQMessageOfHR rm = JSON.parseObject(json, new TypeToken<MQMessageOfHR>() {
				}.getType());
				this.hrMessageHandleContent.handle(null, rm);
			}
		} catch (Exception e) {
			logger.info("无法解析kafka对象消息:" + JSONObject.toJSONString(data));
		}
	}
}