package com.strongbj.core.tag;
/**
 * 判断标签的有效性
 * @author yuzhantao
 *
 */
public interface ITagRule {
	/**
	 * 判断标签格式是否有效
	 * @return
	 */
	boolean isValid(String code);
}
