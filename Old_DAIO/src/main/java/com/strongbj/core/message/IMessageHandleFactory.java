package com.strongbj.core.message;

import io.netty.channel.ChannelHandler;
/**
 * 消息工厂类
 * @author yuzhantao
 *
 */
public interface IMessageHandleFactory {
	/**
	 * 创建消息解码类
	 * @return
	 */
	ChannelHandler createMessageDecoder();
	
	/**
	 * 创建消息处理类
	 * @return
	 */
	ChannelHandler createMessageHandle();
}
