package com.strongbj.core;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

import com.strongbj.core.message.IMessageHandleFactory;
import com.strongbj.core.server.TimeoutHandler;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.concurrent.DefaultEventExecutorGroup;
import io.netty.util.concurrent.EventExecutorGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

// 采集服务器
public class CollectionServer  implements ApplicationListener<ApplicationEvent> {
	public static final ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
	private static Logger logger = LogManager.getLogger(CollectionServer.class.getName());
	/**
	 * 服务器名称
	 */
	private String serverName;
	/**
	 * 服务器端口号
	 */
	private int serverPort;
	/**
	 * 服务器socket读超时的秒数
	 */
	private long readTimeout=600;
	/**
	 * 服务器socket写超时的秒数
	 */
	private long writeTimeout=Long.MAX_VALUE;
	/**
	 * 服务器处理工厂
	 */
	private IMessageHandleFactory messageHandleFactory;
	
	@Override
	public void onApplicationEvent(ApplicationEvent event) {

		
//		try {
//			cpu();
//		} catch (SigarException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		new Thread(new Runnable(){

			@Override
			public void run() {
				try {
					CollectionServer.this.bind(
							CollectionServer.this.getMessageHandleFactory(),
							CollectionServer.this.getServerName(),
							CollectionServer.this.getServerPort(),
							CollectionServer.this.getWriteTimeout(),
							CollectionServer.this.getReadTimeout()
							);
				} catch (Exception e) {
					e.printStackTrace();
					logger.error(CollectionServer.this.getServerName()+"异常:"+e.getMessage());
				}
			}
		}).start();
	}
	
	private void bind(IMessageHandleFactory factory,String serverName,int port,long writeTimetou,long readTime) throws Exception {
		EventLoopGroup bossGroup = new NioEventLoopGroup();
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		try {
			ServerBootstrap serverBootstrap = new ServerBootstrap();
			serverBootstrap.group(bossGroup, workerGroup);
			serverBootstrap.channel(NioServerSocketChannel.class);
			serverBootstrap.option(ChannelOption.SO_BACKLOG, 1024);
			serverBootstrap.handler(new LoggingHandler());
			serverBootstrap.childHandler(new NettyChannelHandler(factory,writeTimetou,readTime));
			serverBootstrap.childOption(ChannelOption.SO_KEEPALIVE, true);
			ChannelFuture channelFuture = serverBootstrap.bind(port).sync();
			logger.info("======="+serverName+"[port:"+port+"] 已启动=======");
			channelFuture.channel().closeFuture().sync();
		} finally {
			bossGroup.shutdownGracefully();
			workerGroup.shutdownGracefully();
		}
	}

	// 消息处理类
	private class NettyChannelHandler extends ChannelInitializer<SocketChannel> {
		private IMessageHandleFactory factory;
		private long writeTimeout,readTimeout;
//		private final EventExecutorGroup coderExecutorGroup = new DefaultEventExecutorGroup(5);		// 解码用线程池
		private final EventExecutorGroup handleExecutorGroup = new DefaultEventExecutorGroup(3);		// 业务处理线程池
		
		public NettyChannelHandler(IMessageHandleFactory factory,long writeTimeout,long readTimeout){
			super();
			this.factory=factory;
			this.readTimeout=readTimeout;
			this.writeTimeout=writeTimeout;
		}
		@Override
		protected void initChannel(SocketChannel socketChannel) throws Exception {
			// 设置超时
			socketChannel.pipeline().addLast(
					new IdleStateHandler(
							this.readTimeout,
							this.writeTimeout, 
							Math.max(this.readTimeout, this.writeTimeout), 
							TimeUnit.SECONDS)
					); 
			// 超时处理
			socketChannel.pipeline().addLast(new TimeoutHandler());
			// 解码处理
			socketChannel.pipeline().addLast(factory.createMessageDecoder());
			// 业务处理
			socketChannel.pipeline().addLast(handleExecutorGroup,factory.createMessageHandle());
		}
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public int getServerPort() {
		return serverPort;
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	public IMessageHandleFactory getMessageHandleFactory() {
		return messageHandleFactory;
	}

	public void setMessageHandleFactory(IMessageHandleFactory messageHandleFactory) {
		this.messageHandleFactory = messageHandleFactory;
	}

	public long getReadTimeout() {
		return readTimeout;
	}

	public void setReadTimeout(long readTimeout) {
		this.readTimeout = readTimeout;
	}

	public long getWriteTimeout() {
		return writeTimeout;
	}

	public void setWriteTimeout(long writeTimeout) {
		this.writeTimeout = writeTimeout;
	} 
}
